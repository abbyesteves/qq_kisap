//
//  CouchService.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 18/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class CouchService {
    
    func savePreference(tags : [String]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("preference")
        let document = database.document(withID: "preference")!
        var properties = document.properties
        
        if properties != nil {
            properties!["tags"] = tags
            do {
                try document.putProperties(properties!)
            } catch {
                print(" update status failed! ", error)
            }
        } else {
            let object = [
                "subscription_details": [
                    "type": "free",
                    "start": "Fri, 09 Nov 2018 06:29:22 GMT",
                    "end": "Fri, 09 Nov 2019 00:00:00 GMT"
                ],
                "tags" : tags
            ] as [String : Any]
            
            do {
                try document.putProperties(object)
            } catch {
                print(" save status failed! ", error)
            }
        }
    }
    
    func town() -> Towns {
        var town = Service().Town()
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("town")
        let document = database.document(withID: "selected")!
        let properties = document.properties
        if properties != nil {
            let documentTown = database.document(withID: "\((properties!["town_id"])!)")!
            let propertiesTown = documentTown.properties
            
            if propertiesTown != nil {
                let officials = propertiesTown!["officials"] as! [String: Any]
                let mayor = (officials["mayor"])! as! [String: Any]
                let vice_mayor = (officials["vice_mayor"])! as! [String: Any]
                let address = (propertiesTown!["address"])! as! [String: Any]
                let configurations = (propertiesTown!["config"])! as! [String: Any]
                let galleries = (configurations["gallery"])! as! [String]
                let teams = mayor["teams"]! as! [[String:Any]]
                
                var addresses = Addresses()
                addresses.lat = Double("\((address["lat"])!)")
                addresses.long = Double("\((address["long"])!)")
                addresses.municipal_address = "\((address["municipal_address"])!)"
                addresses.province = "\((address["province"])!)"
                addresses.region = "\((address["region"])!)"
                
                var config = Configurations()
                config.logo = "\((configurations["logo"])!)"
                config.theme_color = "\((configurations["theme_color"])!)"
                config.gallery = galleries
                
                town.town_id = "\((propertiesTown!["town_id"])!)"
                town.doc_type = "\((propertiesTown!["doc_type"])!)"
                town.name = "\((propertiesTown!["name"])!)"
                town.initials = "\((propertiesTown!["initials"])!)"
                town.short_desc = "\((propertiesTown!["short_desc"])!)"
                town.tagline = "\((propertiesTown!["tagline"])!)"
                town.address = addresses
                town.rating = (propertiesTown!["rating"])! as? Double
                town.config = config
                town.last_updated = "\((propertiesTown!["last_updated"])!)"
                town.town_version = "\((propertiesTown!["town_version"])!)"
                town.officials = Officials(mayor: Mayors(
                    name : "\((mayor["name"])!)",
                    short_desc : "\((mayor["short_desc"])!)",
                    summary : "\((mayor["summary"])!)",
                    teams : teamModel(teams : teams),//mayor["teams"]! as? [Teams],
                    gallery : (mayor["social_media"]!) as? [String],
                    social_media : mayor["social_media"]! as? [String],
                    mayors_voice : mayor["mayors_voice"]! as? [String],
                    news : mayor["news"]! as? [String],
                    videos : mayor["videos"]! as? [String],
                    rating : 0
                    ), vice_mayor: Mayors(
                        name : "\((vice_mayor["name"])!)",
                        short_desc : "\((vice_mayor["short_desc"])!)",
                        summary : "\((vice_mayor["summary"])!)",
                        teams : vice_mayor["teams"]! as? [Teams],
                        gallery : vice_mayor["social_media"]! as? [String],
                        social_media : vice_mayor["social_media"]! as? [String],
                        mayors_voice : mayor["mayors_voice"]! as? [String],
                        news : mayor["news"]! as? [String],
                        videos : mayor["videos"]! as? [String],
                        rating : 0
                ), counselors: [""], term: "")
                town.businesses = ["\((propertiesTown!["businesses"])!)"]
                town.baranggay = propertiesTown!["baranggay"] as? [Baranggays]
                town.forms = propertiesTown!["forms"] as? [Forms]
                town.gov_services = (propertiesTown!["gov_services"])! as? Services
                town.news = [Bulletins()]
                town.events = [Bulletins()]
                town.didSelect = true
            }
        }
        return town
    }
    
    func townSelected(town : Towns) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("town")
        let documentSelect = database.document(withID: "selected")!
        let document = database.document(withID: (town.town_id)!)!
        let properties = document.properties
        var propertiesSelect = documentSelect.properties
        
        if propertiesSelect != nil {
            propertiesSelect!["town_id"] = town.town_id
            do {
                try documentSelect.putProperties(propertiesSelect!)
            } catch {
                print(" update status failed! ", error)
            }
            
        } else {
            let object = [
                "town_id": (town.town_id)!
                ] as [String: Any]
            do {
                try documentSelect.putProperties(object)
            } catch {
                print(" save status failed! ", error)
            }
        }
        
        if properties == nil {
            let object = [
                "town_id": (town.town_id)!,
                "doc_type": (town.doc_type)!,
                "name": (town.name)!,
                "initials": (town.initials)!,
                "short_desc": (town.short_desc)!,
                "tagline": (town.tagline)!,
                "address":[
                    "lat": (town.address!.lat)!,
                    "long": (town.address!.lat)!,
                    "province": (town.address!.province)!,
                    "municipal_address": (town.address!.municipal_address)!,
                    "region": (town.address!.region)!
                ],
                "rating": (town.rating)!,
                "config": [
                    "logo": (town.config!.logo)!,
                    "theme_color": (town.config!.theme_color)!,
                    "gallery": (town.config!.gallery)!
                ],
                "last_updated": (town.last_updated)!,
                "town_version": (town.town_version)!,
                "officials": [
                    "mayor":[
                        "name" : (town.officials!.mayor.name)!,
                        "short_desc" : (town.officials!.mayor.short_desc)!,
                        "summary" : (town.officials!.mayor.summary)!,
                        "gallery" : (town.officials!.mayor.gallery)!,
                        "teams" : teams(teams : town.officials!.mayor.teams),
                        "social_media": (town.officials!.mayor.social_media)!,
                        "mayors_voice": (town.officials!.mayor.mayors_voice)!,
                        "news": (town.officials!.mayor.news)!,
                        "videos": (town.officials!.mayor.videos)!,
                        "rating": (town.officials!.mayor.rating)!
                    ],
                    "vice_mayor":[
                        "name" : (town.officials!.vice_mayor.name)!,
                        "short_desc" : (town.officials!.vice_mayor.short_desc)!,
                        "summary" : (town.officials!.vice_mayor.summary)!,
                        "gallery" : (town.officials!.vice_mayor.gallery)!,
                        "teams" : teams(teams : town.officials!.vice_mayor.teams),
                        "social_media": (town.officials!.vice_mayor.social_media)!,
                        "mayors_voice": (town.officials!.vice_mayor.mayors_voice)!,
                        "news": (town.officials!.vice_mayor.news)!,
                        "videos": (town.officials!.vice_mayor.videos)!,
                        "rating": (town.officials!.vice_mayor.rating)!
                    ],
                    "counselors" : (town.officials!.counselors)!,
                    "term" : (town.officials!.term)!
                ],
                "businesses": (town.businesses)!,
                "baranggay" : businesses(baranggays : town.baranggay),
                "forms" : forms(forms : town.forms),
                "gov_services": [
                    "categories": (town.gov_services?.categories)!,
                    "police" : serviceObjects(serviceObjects : town.gov_services.police),
                    "fire" : serviceObjects(serviceObjects : town.gov_services.fire),
                    "hospital" : serviceObjects(serviceObjects : town.gov_services.hospital),
                    "risk_management" : serviceObjects(serviceObjects : town.gov_services.risk_management)
                ]
            ] as [String:Any]
            
            do {
                try document.putProperties(object)
//            properties!["name"] = town.name
//            properties!["initials"] = town.initials
//            properties!["short_desc"] = town.short_desc
//            properties!["municipal_address"] = town.municipal_address
//            properties!["rating"] = town.rating
//            properties!["config"] = town.config
//            properties!["population"] = town.population
//            properties!["officials"] = town.officials
//            properties!["businesses"] = town.businesses
//            properties!["baranggay"] = town.baranggay
//            properties!["forms"] = town.forms
//            properties!["gov_services"] = town.gov_services
//            properties!["news"] = town.news
//            properties!["events"] = town.events
//            properties!["didSelect"] = town.didSelect
//            properties!["theme"] = town.theme
//
//            do {
//                try document.putProperties(properties!)
//            } catch {
//                print(" update status failed! ", error)
//            }
                    
            } catch {
                print(" save status failed! ", error)
            }
        }
    }
    func bulletins(bulletins : [Bulletins]) ->[[String:Any]] {
//        var array = [[String:Any]]()
//        for bulletin in bulletins {
//        }
        return [[String:Any]]()
    }
    
    func serviceObjects(serviceObjects : [ServiceObjects]) ->[[String:Any]] {
        var array = [[String:Any]]()
        for serviceObject in serviceObjects {
            array.append([
                "name": serviceObject.name,
                "contact" : serviceObject.contact,
                "lat" : serviceObject.lat,
                "long" : serviceObject.long
            ])
        }
        return array
    }
    
    func forms(forms : [Forms]) ->[[String:Any]] {
        var array = [[String:Any]]()
        for form in forms {
            array.append([
                "name": form.name,
                "data": form.data
            ])
        }
        return array
    }
    func businesses(baranggays : [Baranggays]) ->[[String:Any]] {
        var array = [[String:Any]]()
        for baranggay in baranggays {
            array.append([
                "name": baranggay.name,
                "captain": baranggay.captain,
                "lat" : baranggay.lat,
                "long" : baranggay.long
            ])
        }
        return array
    }
    
    func teams(teams : [Teams]) ->[[String:Any]] {
        var array = [[String:Any]]()
        for team in teams {
            if team.name != nil {
                array.append([
                    "name": "\((team.name)!)",
                    "position": "\((team.position)!)",
                    "img" : "\((team.img)!)",
                ])
            }
        }
        return array
    }
    
    func teamModel(teams : [[String:Any]]) ->[Teams] {
        var teamsArr = [Teams]()
        for team in teams {
            if team["name"] != nil {
                var teamModel = Teams()
                teamModel.name = "\((team["name"])!)"
                teamModel.img = "\((team["img"])!)"
                teamModel.position = "\((team["position"])!)"
                teamsArr.append(teamModel)
            }
        }
        return teamsArr
    }
}
