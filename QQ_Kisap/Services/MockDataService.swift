//
//  MockDataService.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 16/11/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class MockDataService {
    
    static var towns : [Towns] = {
        var address1 = Addresses()
        address1.lat = 14.8056671
        address1.long = 120.9357876
        address1.province = "Bulacan"
        address1.municipal_address = ""
        address1.region = ""
        var town1 = Towns(
            town_id: "town_0001",
            doc_type: "town",
            name: "Bocaue",
            initials: "BOC",
            short_desc: "",
            tagline: "",
            address: address1,
            rating: 4.6,
            config: Configurations(logo: "", theme_color: "2799fa", gallery: [""]),
            last_updated: "",
            town_version: "",
            officials: Officials(
                mayor: Mayors(
                    name: "Joni Villanueva",
                    short_desc: "Mayor Joni Villanueva is currently the Municipal Mayor of Bocaue, Bulacan. Mayor Joni graduated cum laude with a bachelor's degree in Broadcast Communication from the University of the Philippines. Not long after, Joni Villanueva won the mayoral race in Bocaue, Bulacan via coin toss became an unusual event in the history of Philippine elections.",
                    summary: "Joni's admiration for her parents is obvious. \"My dad and mom have always been my faithful supporters. They are God's greatest blessing to me.\" she shares adding that it was their godly rearing and instructions that has made her choose the life she njoys now. \"The never dictated anything. Instead the led me to God and taught me always to turn to Him and the good book for wisdom and direction.\" Her life's guiding principle is Proverbs 3:5-6: \"In everything you do, put God first and He will direct you and crown your efforts with succcess.\"",
                    teams : [Teams(name: "Imelda G. De Leon", position: "Vice Mayor", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXdqHXxr6en18yHY9DPRjvEpxTV30Jiyohm0sYoTfzRvsvNkvO"),Teams(name: "Lorie S. Vinta", position: "Coun.", img: "https://3.bp.blogspot.com/-NWEXFN35RdA/V3oR0l6a7GI/AAAAAAAAos8/G4H3ynm6RvsroatF1OzlkKnTc8wwyerIwCLcB/s1600/2015PhotoCASAS.jpg"),Teams(name: "Reginald V. Javier", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/sipat_delacruz.jpg"),Teams(name: "Raul V. Gatuz", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/tabang_gatuz.jpg"),Teams(name: "Maria Isabel V. Liwanag", position: "Coun.", img: "http://livedoor.blogimg.jp/wildday/imgs/d/9/d96d8d57.jpg"),Teams(name: "Gerlad S. Constantino", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/banga1_dedios.jpg"),Teams(name: "Rodelio DC. Camitan", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/HAGONOY/stacruz_delacruz.jpg"),Teams(name: "Rhandy G. Marcelo", position: "Coun.", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1Gw7C6HWG2sCSbQ6kAcBzeZAq3ym35AofGzRATf2gMu2eihCp"),Teams(name: "Virgilio DC. Alejo", position: "Coun.", img: "http://oscamanila.x10host.com/img/dist2coun6.jpg")],
                    gallery: ["https://scontent-sea1-1.cdninstagram.com/vp/f3285797fd0e70e9aedddf2b0bcf5862/5C20688B/t51.2885-15/e35/20686747_1488221224554297_1731266454768058368_n.jpg?se=7&ig_cache_key=MTU3NjAwNjg5MzgxNzM1OTI1NQ%3D%3D.2", "https://files.pia.gov.ph/source/2018/08/03/03_5b646ca74daa96_15623120.jpg","https://i0.wp.com/bulatlat.com/main/wp-content/uploads/2017/03/occupy-bulacan-zeng-05.jpg"],
                    social_media: ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJ4TQIaDyFEBG9NNTSdV5us3EDgcm3IYvNv9seTnpWKUQT1pRC","https://ytimg.googleusercontent.com/vi/Y1czXZ-Lr4o/mqdefault.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVLChqVLeTRQuemWyyPfT6_j9Lt31axOLiqjlg5qiyFr15lBmE","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEE-xVrgrzG_uL9IoTFljXOSiI2nKxOqRo147Fm6SDBlpFc1k9"],
                    mayors_voice: [""],
                    news: ["http://centralluzon.politics.com.ph/mayor-villanueva-shows-off-new-bocaue-projects/"],
                    videos: ["https://www.youtube.com/watch?v=wtDxK7qGK5c", "https://www.youtube.com/watch?v=l_C2M5SkCCI&feature=youtu.be", "https://www.youtube.com/watch?v=Ta5XMVI8q8g"], rating: 0),
                vice_mayor: Mayors(
                    name: "",
                    short_desc: "",
                    summary: "",
                    teams : [Teams()],
                    gallery: [""],
                    social_media :[""],
                    mayors_voice : [""],
                    news : [""],
                    videos :[""],
                    rating : 0
                ),
                counselors: [""],
                term: ""),
            businesses: [""],
            baranggay: [Baranggays()],
            forms: [Forms(name: "", data: "")],
            gov_services: Services(categories: [""], police: [ServiceObjects()], fire: [ServiceObjects()], hospital: [ServiceObjects()], risk_management: [ServiceObjects()]),
            news: [Bulletins()],
            events: [Bulletins()],
            didSelect: false)
        
        var address2 = Addresses()
        address2.lat = 14.8527393
        address2.long = 120.8160376
        address2.province = "Bulacan"
        address2.municipal_address = ""
        address2.region = ""
        var town2 = Towns(
            town_id: "town_0002",
            doc_type: "town",
            name: "Malolos",
            initials: "MAL",
            short_desc: "",
            tagline: "",
            address: address2,
            rating: 4.5,
            config: Configurations(logo: "", theme_color: "614898", gallery: [""]),
            last_updated: "",
            town_version: "",
            officials: Officials(
                mayor: Mayors(
                    name: "Christian D. Natividad",
                    short_desc: "Christian D. Natividad, our nominee for youth leadership is a young achiever, aggressive, action oriented, independent, strong willed and passionate. He is the son of former Assemblyman and NAPOLCOM Chief Teodulo Natividad and Matilde Dionisio, a teacher. Son of an illustrious national figure, people expect him to be rich, and spoiled but on the contrary, Christian being an illegitimate child faced extreme hardships at an age when he should have been enjoying himself.",
                    summary: "At age 19, with the untimely death of his father, he saw his mother cry because she only has P 65 in her wallet. The incident turned the boy into a man. With no one to depend on, he vowed to himself, his mother will not cry again; that no matter what, their lives will improve for the better. To provide for himself, his brother and mother he waited on tables, played in bands, taught part time in College and sold encyclopedias.. In high school, he was an active officer of the Student Council. Later, at 18, he was appointed as a Sangguniang Kabataan Chairman and implemented activities on sports, health and environment that received recognitions and earned for him respect from the community. What was amazing was that with sheer determination and courage he was able to turn adversity into opportunity all by himself. As one of the prime movers of KAPANALIG ( Kabataang Panlalawigan na Nagkakaisa Laban sa Ipinagbabawal na Gamot ), his firm stand against illegal drugs was recognized by the PNP on 1998. With his accomplishments, he slowly ceased to be in the shadows of his Dad. Natividad or No Natividad, Maloleños, especially the young people started to trust and believe in him. His 16 years in school, community, city and provincial government as a young leader has amply prepared him for his job as Mayor.Today, he is one of the province’s most dynamic and progressive young leaders.",
                    teams : [Teams(name: "Imelda G. De Leon", position: "Vice Mayor", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXdqHXxr6en18yHY9DPRjvEpxTV30Jiyohm0sYoTfzRvsvNkvO"),Teams(name: "Lorie S. Vinta", position: "Coun.", img: "https://3.bp.blogspot.com/-NWEXFN35RdA/V3oR0l6a7GI/AAAAAAAAos8/G4H3ynm6RvsroatF1OzlkKnTc8wwyerIwCLcB/s1600/2015PhotoCASAS.jpg"),Teams(name: "Reginald V. Javier", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/sipat_delacruz.jpg"),Teams(name: "Raul V. Gatuz", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/tabang_gatuz.jpg"),Teams(name: "Maria Isabel V. Liwanag", position: "Coun.", img: "http://livedoor.blogimg.jp/wildday/imgs/d/9/d96d8d57.jpg"),Teams(name: "Gerlad S. Constantino", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/banga1_dedios.jpg"),Teams(name: "Rodelio DC. Camitan", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/HAGONOY/stacruz_delacruz.jpg"),Teams(name: "Rhandy G. Marcelo", position: "Coun.", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1Gw7C6HWG2sCSbQ6kAcBzeZAq3ym35AofGzRATf2gMu2eihCp"),Teams(name: "Virgilio DC. Alejo", position: "Coun.", img: "http://oscamanila.x10host.com/img/dist2coun6.jpg")],
                    gallery: ["http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31727855_441456696291478_523343938271051776_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKQ_4geIeWz1eYfmJUamoNnDdd3fbregeTqdX_jY48dXgidq7LGw", "http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31706525_441458269624654_5645390646920347648_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSi7vkeqYC7f_BniOVN5r5K9Z6lQoAbQzqnshxj8thp77WBmUkbKQ"],
                    social_media: ["https://pbs.twimg.com/media/Dm-Q1mFV4AYcBdU.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREBXTKwtfAt9R_JcGwx94V81WQQQoOUxwJoeGwdW98OPioSiPi","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-QrFZ4t05w3jcvjr-pXbRBRsTL1IIvEwyXct5KypzmPZgzKii"],
                    mayors_voice: [""],
                    news: ["http://manilastandard.net/mobile/article/271669"],
                    videos: ["https://www.youtube.com/watch?v=44MyIOwBYus", "https://www.youtube.com/watch?v=xiYVtUY3HKM", "https://www.youtube.com/watch?v=s5yO-2T2wyc"], rating: 0),
                vice_mayor: Mayors(
                    name: "",
                    short_desc: "",
                    summary: "",
                    teams : [Teams()],
                    gallery: [""],
                    social_media :[""],
                    mayors_voice : [""],
                    news : [""],
                    videos :[""],
                    rating : 0
                ),
                counselors: [""],
                term: ""),
            businesses: [""],
            baranggay: [Baranggays()],
            forms: [Forms()],
            gov_services: Services(categories: [""], police: [ServiceObjects()], fire: [ServiceObjects()], hospital: [ServiceObjects()], risk_management: [ServiceObjects()]),
            news: [Bulletins()],
            events: [Bulletins()],
            didSelect: false)
        
        var address3 = Addresses()
        address3.lat = 14.9672031
        address3.long = 120.8843434
        address3.province = "Bulacan"
        address3.municipal_address = ""
        address3.region = ""
        var town3 = Towns(
            town_id: "town_0003",
            doc_type: "town",
            name: "Baliuag",
            initials: "BAL",
            short_desc: "",
            tagline: "",
            address: address3,
            rating: 5.0,
            config: Configurations(logo: "https://www.businesslist.ph/img/ph/m/1496992006-53-municipality-of-baliuag-bulacan.png", theme_color: "f89838", gallery: [""]),
            last_updated: "",
            town_version: "",
            officials: Officials(
                mayor: Mayors(
                    name: "Joel Villanueva",
                    short_desc: "Emmanuel Joel Villanueva is a Filipino politician, who serves as a Senator of the Philippines. He took his senatorial seat following the 2016 Philippine national elections, where he ranked second highest in the polls with 18,459,222 votes. He was a former Director General of the Philippine TESDA from 2010-2015 and a three-term congressman from 2001 through 2010. Villanueva was the youngest member of the House of Representatives when he assumed office in February 2002.",
                    summary: "Villanueva was elected to the Philippine House of Representatives in 2001 as a party-list representative of the Citizens' Battle Against Corruption (CIBAC). However, his oath-taking was stalled for seven months following issues that CIBAC was an extension of the Jesus Is Lord Church Worldwide, a Christian church founded by his father, Eddie Villanueva.",
                    teams : [Teams(name: "Imelda G. De Leon", position: "Vice Mayor", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXdqHXxr6en18yHY9DPRjvEpxTV30Jiyohm0sYoTfzRvsvNkvO"),Teams(name: "Lorie S. Vinta", position: "Coun.", img: "https://3.bp.blogspot.com/-NWEXFN35RdA/V3oR0l6a7GI/AAAAAAAAos8/G4H3ynm6RvsroatF1OzlkKnTc8wwyerIwCLcB/s1600/2015PhotoCASAS.jpg"),Teams(name: "Reginald V. Javier", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/sipat_delacruz.jpg"),Teams(name: "Raul V. Gatuz", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/tabang_gatuz.jpg"),Teams(name: "Maria Isabel V. Liwanag", position: "Coun.", img: "http://livedoor.blogimg.jp/wildday/imgs/d/9/d96d8d57.jpg"),Teams(name: "Gerlad S. Constantino", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/banga1_dedios.jpg"),Teams(name: "Rodelio DC. Camitan", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/HAGONOY/stacruz_delacruz.jpg"),Teams(name: "Rhandy G. Marcelo", position: "Coun.", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1Gw7C6HWG2sCSbQ6kAcBzeZAq3ym35AofGzRATf2gMu2eihCp"),Teams(name: "Virgilio DC. Alejo", position: "Coun.", img: "http://oscamanila.x10host.com/img/dist2coun6.jpg")],
                    gallery: ["http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31727855_441456696291478_523343938271051776_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKQ_4geIeWz1eYfmJUamoNnDdd3fbregeTqdX_jY48dXgidq7LGw", "http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31706525_441458269624654_5645390646920347648_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSi7vkeqYC7f_BniOVN5r5K9Z6lQoAbQzqnshxj8thp77WBmUkbKQ","https://antacloban.files.wordpress.com/2012/06/joel-villanueva.jpg"],
                    social_media: ["https://www.pabaon.com/wp-content/uploads/2016/03/tesdaman-joel.jpg","https://2.bp.blogspot.com/-MkicdOJCi6M/WJScDOdNviI/AAAAAAAADus/ZlEqohxIvSYKLopipHPwVGh_mHtuyiDJQCLcB/s1600/000000000000.png","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2Hppy9xj4ZvjUTmsIBGgpM8MQnbT4XuVmGeBBb0pkSpQpKyCz","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQX75lW0Jbl0mYn9Rx8F_45qE1Trl_WeQ-3ea7tfQg8L3bZtRIl","https://2.bp.blogspot.com/-NZ_GIytShKA/V3PMhJSy5tI/AAAAAAAAGs4/Xr9gMOE16JsZVBltruyKqV1-Iaum-PppwCLcB/s1600/inagurasyon2016bulacan.png"],
                    mayors_voice: [""],
                    news: ["https://m.inquirer.net/business/94827"],
                    videos: ["https://www.youtube.com/watch?v=XmzdcSI_HDs", "https://www.youtube.com/watch?v=Hd3OK8ALY9s&t=2s", "https://www.youtube.com/watch?v=-IJpPDlfSRA"], rating: 0),
                vice_mayor: Mayors(
                    name: "",
                    short_desc: "",
                    summary: "",
                    teams : [Teams()],
                    gallery: [""],
                    social_media :[""],
                    mayors_voice : [""],
                    news : [""],
                    videos :[""],
                    rating : 0
                ),
                counselors: [""],
                term: ""),
            businesses: [""],
            baranggay: [Baranggays()],
            forms: [Forms()],
            gov_services: Services(categories: [""], police: [ServiceObjects()], fire: [ServiceObjects()], hospital: [ServiceObjects()], risk_management: [ServiceObjects()]),
            news: [Bulletins()],
            events: [Bulletins()],
            didSelect: false)
        
        var address4 = Addresses()
        address4.lat = 14.8814829
        address4.long = 120.8671201
        address4.province = "Bulacan"
        address4.municipal_address = ""
        address4.region = ""
        var town4 = Towns(
            town_id: "town_0004",
            doc_type: "town",
            name: "Plaridel",
            initials: "PLA",
            short_desc: "",
            tagline: "",
            address: address4,
            rating: 4.1,
            config: Configurations(logo: "", theme_color: "c752a7", gallery: [""]),
            last_updated: "",
            town_version: "",
            officials: Officials(
                mayor: Mayors(
                    name: "Jocell Vistan Casaje",
                    short_desc: "Like many towns in Bulacan, Plaridel has its niche in Philippine history as the site of the Battle of Quingua during the Philippine-American War as part of the defense of the First Philippine Republic against the Northern Campaign of the American Army. The battle, manned by Pablo Tecson—Lt. Colonel Pablo Ocampo Tecson of San Miguel, Bulacan—under Gregorio del Pilar on the side of the First Philippine Republic, led to the death of Col. John Stotsenberg of the American Army on April 23, 1899. A marker now stands at the site of the battle in Barangay Agnaya.",
                    summary: "Plaridel's history can be traced through records back to 1581 in the early years of the Spanish colonization. The Augustinian friars from Malolos Convent discovered a vast forest in 1581 then named as Binto; this would later be known as Quingua. As per the history of the Parish of St. James the Apostle, 2001 issue, Quingua was established by the Augustinian Friars of Malolos who initially named it \"Encomienda Binto\" (Brgy. Bintog got its name from this settlement). They built a visita (chapel of ease) and placed it under the jurisdiction of Fray Mateo Mendoza, the prior of Malolos.",
                    teams : [Teams(name: "Imelda G. De Leon", position: "Vice Mayor", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXdqHXxr6en18yHY9DPRjvEpxTV30Jiyohm0sYoTfzRvsvNkvO"),Teams(name: "Lorie S. Vinta", position: "Coun.", img: "https://3.bp.blogspot.com/-NWEXFN35RdA/V3oR0l6a7GI/AAAAAAAAos8/G4H3ynm6RvsroatF1OzlkKnTc8wwyerIwCLcB/s1600/2015PhotoCASAS.jpg"),Teams(name: "Reginald V. Javier", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/sipat_delacruz.jpg"),Teams(name: "Raul V. Gatuz", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/tabang_gatuz.jpg"),Teams(name: "Maria Isabel V. Liwanag", position: "Coun.", img: "http://livedoor.blogimg.jp/wildday/imgs/d/9/d96d8d57.jpg"),Teams(name: "Gerlad S. Constantino", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/PLARIDEL/banga1_dedios.jpg"),Teams(name: "Rodelio DC. Camitan", position: "Coun.", img: "https://www.bulacan.gov.ph/lnb/images/HAGONOY/stacruz_delacruz.jpg"),Teams(name: "Rhandy G. Marcelo", position: "Coun.", img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1Gw7C6HWG2sCSbQ6kAcBzeZAq3ym35AofGzRATf2gMu2eihCp"),Teams(name: "Virgilio DC. Alejo", position: "Coun.", img: "http://oscamanila.x10host.com/img/dist2coun6.jpg")],
                    gallery: ["http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31727855_441456696291478_523343938271051776_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKQ_4geIeWz1eYfmJUamoNnDdd3fbregeTqdX_jY48dXgidq7LGw", "http://www.capastarlac.gov.ph/wp-content/uploads/photo-gallery/2018/Senator_Joel_Villanueva_Launches_Project_in_Capas_Tarlac/31706525_441458269624654_5645390646920347648_o.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSi7vkeqYC7f_BniOVN5r5K9Z6lQoAbQzqnshxj8thp77WBmUkbKQ"],
                    social_media: ["https://i.ytimg.com/vi/2y9DzcYi5sA/maxresdefault.jpg","https://www.bulacan.gov.ph/government/images/btneducation.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZajTLCZ16euoUhBP7iAsAuzwGx_qqSrjZDbsoQkYMttmxb2Bk","https://www.bulacan.gov.ph/images/news/2410.jpg","https://www.spcpasig.edu.ph/wp-content/uploads/2018/05/mayor-Vistan-Batch-1994.jpg"],
                    mayors_voice: [""],
                    news: ["http://centralluzon.politics.com.ph/tag/plaridel-mayor-jocell-vistan-casaje/"],
                    videos: ["https://www.youtube.com/watch?v=2y9DzcYi5sA","https://www.youtube.com/watch?v=iebi0EYnjcE","https://www.youtube.com/watch?v=aQfPFD3EKcI"], rating: 0),
                vice_mayor: Mayors(
                    name: "",
                    short_desc: "",
                    summary: "",
                    teams : [Teams()],
                    gallery: [""],
                    social_media :[""],
                    mayors_voice : [""],
                    news : [""],
                    videos :[""],
                    rating : 0
                ),
                counselors: [""],
                term: ""),
            businesses: [""],
            baranggay: [Baranggays()],
            forms: [Forms()],
            gov_services: Services(categories: [""], police: [ServiceObjects()], fire: [ServiceObjects()], hospital: [ServiceObjects()], risk_management: [ServiceObjects()]),
            news: [Bulletins()],
            events: [Bulletins()],
            didSelect: false)
        
        return [town1, town2, town3, town4]
    }()
    
    static var businesses : [Businesses] = {
        var feedBack1 = Feedbacks()
        feedBack1.name = "Juan Dela Cruz"
        feedBack1.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoqG0A-tDagkz8gp0H8a-mOoFBybt4j0ZuDaI6Rt36_HgKmrT0"
        feedBack1.review = "Best burgers!!! Hands down! try niyo guys yung Bacon Cheesy Melt.. Sulit!! babalik kami dito lagi hehe. They have so many varieties of burger to choose from. All burger cravings ma satify mo dito.🤩👍🏼"
        feedBack1.rating = 4.2
        feedBack1.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShuwVNPkqsMgEQi0IJ5cKhMfOxyqb5fGkcdlm0FsEfj5QcxsuyTg", "https://www.seriouseats.com/recipes/images/2017/06/20170617-bulgogi-burger-matt-clifton-1-1500x1125.jpg", "https://1ajvmf3xekdd2r35dh3pklbl-wpengine.netdna-ssl.com/wp-content/uploads/2016/06/Vegan-Mushroom-Black-Bean-Burgers-ilovevegancom2.jpg"]
        feedBack1.time_stamp = ""
        
        var feedBack2 = Feedbacks()
        feedBack2.name = "Ben De Corito"
        feedBack2.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqNrwaQroVK_jp4aXV0tUxqcjsCuLFf3mhIZOu7GuFPU4NqOh7"
        feedBack2.review = " great for hanging out with friends. lalo na yung service. ayos dito!😁. Here some of the photos ganda ng place nila. Even the drink are great! Especially the milkshakes."
        feedBack2.rating = 4.7
        feedBack2.photos_uploads = ["https://media-cdn.tripadvisor.com/media/photo-s/03/01/9c/31/cafe-bleibtreu.jpg", "https://im1.dineout.co.in/images/uploads/restaurant/sharpen/2/b/i/p20369-1447846518564c6276cf425.jpg?w=1200", "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/shutterstock-626261780mod-1515166546.jpg", "https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2016/05/milkshake.jpg?itok=n_3au7e1", "https://imagesvc.timeincapp.com/v3/mm/image?url=https%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Fbk-lucky-charms-milkshake-hero-builder.jpg%3Fitok%3DRL3XHIVY&w=1000&q=70"]
        feedBack2.time_stamp = ""
        
        var business1 = Businesses(
            _id : "business_0001",
            name : "Kenyo Burgers",
            branches : 2,
            img_url : "https://static.adweek.com/adweek.com-prod/wp-content/uploads/files/blogs/qpc.jpg",
            doc_type : "business",
            type : "",
            category : "",
            tags : ["Kenyo Burgers", "burger", "fries"],
            address : Addresses(),
            rating : 3,
            short_desc : "burgers and fries",
            description : "",
            owner : ["String"],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://static.adweek.com/adweek.com-prod/wp-content/uploads/files/blogs/qpc.jpg", "https://cdn.trendhunterstatic.com/thumbs/fortune-deli-ads.jpeg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtCpBV7RhPAb_Th_01IjE33XMlM_Q0Ot_f5hCq2Hvo02w6LJekMw", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKtEDelht20aq-eJtmr90YYjOCsM_0ysGFWCrTylnzdfP0PObe", "https://images1.dallasobserver.com/imager/u/original/7019965/mcds_doublequarterpounder_real.jpg"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack1, feedBack2],
            catalog : [Catalog(name: "Beefy Double", img_url: "https://media-cdn.tripadvisor.com/media/photo-s/07/e2/62/01/fast-food-bon-appetit.jpg"),Catalog(name: "Regular with Fries", img_url: "https://d9hyo6bif16lx.cloudfront.net/live/img/production/detail/menu/lunch-dinner_burgers_all-american-burger.jpg"),Catalog(name: "Pickled Pounder", img_url: "https://static1.squarespace.com/static/597534afc534a5ea5ddb5d8f/5975361fe4fcb57d7164ec57/5975361f440243f8d1986790/1505257256108/The+Burger+Place+Online+Ordering+Food+Image.JPG?format=1500w"),Catalog(name: "Black Seed Baconator", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5JhtEDG-3zUR3oT6tGbI6qoinRfxMlBmEQGODzhPOxg0KOtg-"),Catalog(name: "Crispy Potato Strings Patty", img_url: "https://media-cdn.tripadvisor.com/media/photo-s/0b/f4/7d/d3/the-big-anatolian-burger.jpg"),Catalog(name: "Overload", img_url: "https://media.timeout.com/images/102607655/image.jpg"),Catalog(name: "Black Seed Pounder", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKhaSfTNSaIhlc23driofV7r2e_gD7JNjbH0d3JVi8aUDc-YEB")]
        )
        
        var feedBack3 = Feedbacks()
        feedBack3.name = "Sarah Kalvin"
        feedBack3.user_id = "https://images.pexels.com/photos/39013/self-portrait-eyes-face-39013.jpeg?cs=srgb&dl=beautiful-black-and-white-eyes-39013.jpg&fm=jpg"
        feedBack3.review = "I was craving for sushi for a very long time.. luckily may nag open na along zapote road. They have great food and services. You can watch how the sushi and Sashimis are made. Good Ambiance and great wifi i can work in peace with my cravings satisfied.😻"
        feedBack3.rating = 4.5
        feedBack3.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYD5mTaR3eKC-sozWMU5EYlFpWelxZjpk6QFBLpuocISxkqbAX", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSW9AhQ9hguVZZwL3-JSW_F7_ErYXpSGvq5GRaB7yQsvAh8w43c", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQydat4RNuOQYZMNYZMSgpsJNWVrlh83CXH_-wc7dNpKbnn8u0i", "https://igx.4sqi.net/img/general/200x200/684769_LFL0kwhj7Te65PHWNlO4RGcSNDIhF_zPZeHXeIld9uA.jpg"]
        feedBack1.time_stamp = ""
        
        var feedBack4 = Feedbacks()
        feedBack4.name = "Heris Gatmaitan"
        feedBack4.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD3X4N2xaK3IwE8d67SsvkuYlKfun9faPdNbm9hF6nfUlrDY5Y"
        feedBack4.review = "The service is really cool.. They're showcase how the sushis are made was entertaining and very affordable too. We'll definitely come back."
        feedBack4.rating = 3.2
        feedBack4.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6n21Jue5u0AkbviJawCMZrqxJlYrnqAswSpvI_6DEyieK1tm8", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzN-cPOUSArUAOLZJyfhK6MdmRZLkQtcvnQaduyfpcyq24cBAr", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYF8YYzm8Vklgd4t8FDIz3YQM1VA269P1NCM57viPZDu9sN8Rd"]
        feedBack2.time_stamp = ""
        
        var business2 = Businesses(
            _id : "business_0002",
            name : "Sashimi and Sushi",
            branches : 5,
            img_url : "https://files1.coloribus.com/files/adsarchive/part_894/8947805/file/mizkan-group-sushi-vinegar-small-36540.jpg",
            doc_type : "business",
            type : "",
            category : "",
            tags : ["Sashimi and Sushi", "sushi", "sashimi", "japanese"],
            address : Addresses(),
            rating : 5,
            short_desc : "japanese food",
            description : "",
            owner : ["String"],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["http://www.vkeong.com/wp-content/uploads/2010/08/fresh-salmon-sashimi.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiv9mIKUpl52q-AA6X8m0dMhtjhkpnYXderlhD0-cDdBtIQqWD", "https://files1.coloribus.com/files/adsarchive/part_894/8947805/file/mizkan-group-sushi-vinegar-small-36540.jpg"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack3, feedBack4],
            catalog : [Catalog(name: "Sashimi Salmon", img_url: "https://static.diffen.com/uploadz/1/11/sashimi-2.jpg"),Catalog(name: "Combo Sashimi", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUH3yX-kQl2p5UBdAAMmBHMa26Wx_id6OJ4VwgcLvTU-3ip9Hr"),Catalog(name: "Salmon and Tuna", img_url: "https://i.redd.it/3yc9wx2ny14z.jpg"),Catalog(name: "Combo Sashimi and Sushi", img_url: "https://pixfeeds.com/images/japan/food/1280-148656574-sushi-meal-served-with-chopsticks.jpg"),Catalog(name: "Combo Boat", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4YbM4ie1hMM6G5pbKq3Uqx5XdPt_lZG2h4HOAGY8RBWkPxD0p"),Catalog(name: "Combo Sushi", img_url: "https://img.grouponcdn.com/deal/2CtR2S65oxsqAcBUNaoozYpvjnnW/2C-2048x1229/v1/c700x420.jpg"),Catalog(name: "Squid Stuffed Sushi", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXlI5VeCSbHm0byRMxLJx70COby7iMIEhCrZ1UXMxv6ZGtAyEIzA")]
        )
        
        var feedBack5 = Feedbacks()
        feedBack5.name = "Sophia Polintan"
        feedBack5.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTB35OKWH3HGkEQBrD691tWO29m1YtVJSXzvU7lFRpgjy6RZvT"
        feedBack5.review = "Tea was perfect and so was the milk tea.💚"
        feedBack5.rating = 4.5
        feedBack5.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnXO833a3gHPtFmw0n4QnMqWH72e1c9VHSvJvQECnNY6ZP35wv", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmB3ewQx1YQDUA8eLJ-F6dKne96nvKUi410OyqE-33bFXs-VYt"]
        feedBack5.time_stamp = ""
        
        var feedBack6 = Feedbacks()
        feedBack6.name = "Yanna Lopez"
        feedBack6.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToj7_9ChVX2Ow8vKgJSppEHSTKfnWBIufEveRJQ0rS-uqaNk3g"
        feedBack6.review = "It was too sweet for my liking. but the food was good."
        feedBack6.rating = 2.0
        feedBack6.photos_uploads = []
        feedBack6.time_stamp = ""
        
        var business3 = Businesses(
            _id : "business_0003",
            name : "Milk and Teas",
            branches : 1,
            img_url : "https://d22ir9aoo7cbf6.cloudfront.net/wp-content/uploads/sites/6/2018/05/bubble-tea-in-hong-kong-gong-cha.png",
            doc_type : "business",
            type : "food",
            category : "tea",
            tags : ["Milk and Teas", "tea", "milktea", "sweet", "milk"],
            address : Addresses(),
            rating : 1.7,
            short_desc : "milkteas and green teas",
            description : "",
            owner : ["String"],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://i.ytimg.com/vi/k2EIQiAoDA4/maxresdefault.jpg", "https://i.pinimg.com/236x/80/b4/77/80b477224a34e78b8a80bedf582866a2.jpg", "https://i.ytimg.com/vi/KHdlzwkHT4c/maxresdefault.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQg5bf2AEflGu5CTUywUrsxeMy1ESXc7Yu09xTIwSfr4sxz2jtM"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack5, feedBack6],
            catalog : [Catalog(name: "Bubble Tea Trio", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjuRoD90G4GJPNZD-cCLeIju3dcJb7JJIb4de9zyQg85YHn7sI"),Catalog(name: "Strawberry Bubble", img_url: "https://i.pinimg.com/originals/f4/be/47/f4be47faff60d210be1c3025e7c23e3e.jpg"),Catalog(name: "Taro Bubble", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBe5ibCgEwvRwPVconKz9w5n7NBDqFWdLOPRgol7p1Ago1bfPY"),Catalog(name: "Vanilla Bubble", img_url: "https://i.ytimg.com/vi/EoJwHUZoQbY/maxresdefault.jpg"),Catalog(name: "Chocolate Bubble", img_url: "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/719f1aa1501e443bb1856bce121e74d6/BFV41761_DeliciousAsianDrinks_FBFINAL_v5.jpg"),Catalog(name: "Pumpkin Special Bubble", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeoQ7YNKR7K3jMH4Io-4ADvgwHxyL9I36TGUNVR1F2Cfef3TN1"),Catalog(name: "Mango Float Bubble", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRsLwqdHN1lJ6jZe7kdi6YK3eGEwgR65pMhGSNHcyZe58NOXRi7")]
        )
        
        var feedBack7 = Feedbacks()
        feedBack7.name = "Glen Race"
        feedBack7.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTviMUJsM0vbp-pjZ7rVbzzMMef30yEQ-p1YcENeorBqTTdQuBLew"
        feedBack7.review = "I can't explain how good the pizzas was.. If i could marry food i'll marry the pizza i swear😫. Iz jus too good!!!"
        feedBack7.rating = 3.3
        feedBack7.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-Xy_6SeXcFfpPpsTm1mxCqlffWTX7xR2x2K6wMfd94PdBphNs", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQl2_RrV7ZYRiyOwVLxqJiieKlqorEBiJ4GxcE2APEaoOie8l2WeQ","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvBDswBsaqYoAFb5Tf0G9RsMUzsURRacedNQB-eCDYAs8Iwtr9"]
        feedBack7.time_stamp = ""
        
        var feedBack8 = Feedbacks()
        feedBack8.name = "Claire Alcantara"
        feedBack8.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYZk8aw9YqqDAtu4dwT_hLvCpSGG7n_unq25iob2toVrC6x7wN"
        feedBack8.review = "I was finding the pizza place for the perfect pizza and yes i have found it.. Not to greasy not too fatty it was defintely the best i had so far. I was having a hard time deciding and was creating a line but the service people are soo polite and gave me recommendations for my liking.🍕❤️"
        feedBack8.rating = 4.8
        feedBack8.photos_uploads = ["https://localtvwghp.files.wordpress.com/2018/09/921720280-170667a.jpg?quality=85&strip=all&w=400&h=225&crop=1", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuPkFudympGMv25Ffg0GmAVGq0Ie14Y-2aG0UKjpLSMlmSSLgXvQ", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSS59rvqOBPKyIGj2aauavDZYQKqn7_2Tji-mhGY7ZRCzflFao"]
        feedBack8.time_stamp = ""
        
        var business4 = Businesses(
            _id : "business_0004",
            name : "Chinese Palace",
            branches : 10,
            img_url : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSO-Fqi8q8i-p2-ya6oD-spHqDCLrgG1IF5NYG0gb5wuWn9SLZT",
            doc_type : "business",
            type : "food",
            category : "pizza",
            tags : ["chinese Palace", "pizza", "cheesy", "delicious", "pie", "pasta"],
            address : Addresses(),
            rating : 3.5,
            short_desc : "pizzas and pasta",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuTNFpD2sc1-nh59X_C1l9DoP5pn6Sf8jl4ixINOx-4eb-8svf", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuRxI_rXtujwgyt_C_Dvn-ofHzPvR1eur2ja1z5rp1UiTU8O86", "http://s3.amazonaws.com/ogden_images/www.7springs.com/images/2015/06/23201814/pizza-place-2.jpg", "http://clipgoo.com/daut/as/f/b/best-italian-restaurants-in-nyc-for-pasta-pizza-risotto-gelato-the-20-america_cool-italian-cook-tops_home-decor_cheap-home-decor-online-decoration-ideas-affordable-decorating-stores-yosemite-country-o.jpg"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack7, feedBack8],
            catalog : [Catalog(name: "Pepperoni", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZv8YYx7ukVJ9pGeWd-o-II89-5mgMdkV7UmOxPADKvj878d3PTw"), Catalog(name: "Vegi Combo", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSl7uDVN0AfjgwstY6TbDhCzUdNd-aZ26YVyYQVwcoQR4LQ4GKhrQ"),Catalog(name: "Meaty Pie", img_url: "https://www.oceansidechamber.com/uploads/4/4/5/3/44535401/shakeys_orig.jpg"),Catalog(name: "2 Flavors in 1", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKkGTFthACJgSJNQyTtK2xnUkvFet3xlcCHVlmPrHE9m7vAePe"),Catalog(name: "Cheesy Basil", img_url: "https://www.accessatlanta.com/rf/image_inline/Pub/p9/AJC/2018/01/12/Images/pizza1.jpg"),Catalog(name: "Tomato Galore", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4Uce9pwC2GGoMz9m7MatUYoANvnIO1V-4Qw6fXA0ibkbjbUAaWg"),Catalog(name: "Oil Base Pasta", img_url: "http://clipgoo.com/daut/as/f/b/best-italian-restaurants-in-nyc-for-pasta-pizza-risotto-gelato-the-20-america_cool-italian-cook-tops_home-decor_cheap-home-decor-online-decoration-ideas-affordable-decorating-stores-yosemite-country-o.jpg")]
        )
        
        var feedBack9 = Feedbacks()
        feedBack9.name = "Emily Rose"
        feedBack9.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTviMUJsM0vbp-pjZ7rVbzzMMef30yEQ-p1YcENeorBqTTdQuBLew"
        feedBack9.review = "Coffee is great in all branches as usually. Plus the happy service really made my day. Not to mention great internet connectivity i can get things work done. Definetly my go to coffee place.❤️"
        feedBack9.rating = 3.3
        feedBack9.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaVnFgdHFS_p-HggJ1RuDY_h7ypSL7GSnVuKwVS8ZIQhASs2rZCg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSk2H61Rf4I84jZxxaD3zYWcXEidjxDZZ4ztOfn3c_GbEGSM_S1", "https://s3-media4.fl.yelpcdn.com/bphoto/HLQ4upqUconVbkMBQNt0-g/348s.jpg"]
        feedBack9.time_stamp = ""
        
        var feedBack10 = Feedbacks()
        feedBack10.name = "Jill Calvez"
        feedBack10.user_id = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYZk8aw9YqqDAtu4dwT_hLvCpSGG7n_unq25iob2toVrC6x7wN"
        feedBack10.review = "Ice blended are the best for me. I'm not really a coffee girl but this place is definitely had been one of my cravings lately. Once you passed the door you're greeted by smiling faces. This is the win for me.🤩"
        feedBack10.rating = 4.8
        feedBack10.photos_uploads = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBb-czzjyQMMIKS2tx0jIrB1ByquZy5r6Q-pXtQfYkAl-ED-a0", "https://www.afr.com/content/dam/images/g/w/z/1/s/c/image.related.afrArticleLead.620x350.gx8oag.png/1499767087682.jpg", "http://www.chewoutloud.com/wp-content/uploads/2018/02/thai-iced-coffee-1-683x1024.jpg"]
        feedBack10.time_stamp = ""
        
        var business5 = Businesses(
            _id : "business_0005",
            name : "Starbucks Coffee",
            branches : 120,
            img_url : "https://www.america-retail.com/static//2018/04/Equidad-laboral-en-Starbucks-1100x500.jpg",
            doc_type : "business",
            type : "food",
            category : "coffee",
            tags : ["Starbucks Coffee", "coffee", "pastries", "cakes", "coffee bean", "cafe"],
            address : Addresses(),
            rating : 3.8,
            short_desc : "greatest coffee house",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://www.america-retail.com/static//2018/04/Equidad-laboral-en-Starbucks-1100x500.jpg", "https://assets.change.org/photos/7/ou/zi/OlOuziNRVcXqzpX-800x450-noPad.jpg?1531499872", "https://media2.s-nbcnews.com/j/newscms/2018_32/1358955/stabucks-pumpkin-spice-latte-today-main-180809_4f9d281175c73db37f48e7e35f6c5536.fit-760w.jpg","https://cdn-image.foodandwine.com/sites/default/files/styles/medium_2x/public/bando-starbucks-merch-ft-blog0517.jpg?itok=osIP9BNn"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack9, feedBack10],
            catalog : [Catalog(name: "menu", img_url: "https://b.zmtcdn.com/data/menus/344/43344/06e580bd529e7fcf13d5f966418c3cfb.jpg"), Catalog(name: "menu", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-CYi7R-277fiF8AGpa7vnZtrW3P3ufHLU1Qrr6rbOvKX8qIZE"),Catalog(name: "menu", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJGzEZuQtEwqoVbSAKE_RQ3I0eY3o9V0Y_h0nhtu_MVBUh20M2")]
        )
        
        var business6 = Businesses(
            _id : "business_0005",
            name : "Serenitea",
            branches : 73,
            img_url : "https://4.bp.blogspot.com/-wS-yDxHcjZQ/WONh2WNFB5I/AAAAAAAAMQU/sHLm2e3tUH4DP76OhY9vaGtLMf3P8IVUACLcB/s1600/BUY1TAKE1.jpg",
            doc_type : "business",
            type : "drink",
            category : "milk tea",
            tags : ["Serenitea", "tea", "milk tea", "drink", "sweet", "cafe"],
            address : Addresses(),
            rating : 4.6,
            short_desc : "greatest tea house",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSNbiGGdMiWWJ5YIlBx-P5jYXtkDOJ_YUjMKdTTbm446Kn92YM5", "https://1.bp.blogspot.com/-9fr-nAmfEuE/WOQ3HFlVzKI/AAAAAAABBEg/AwgiaxOzMLMbkLKy0maoebktqI64T_MlACLcB/s1600/serenitea%2Bmacha%2Bbuy1%2Bget1%2Bat%2B50%2525%2Boff%2Bpromo%2Bapr%2B2017.jpg", "https://www.davaobase.com/wp-content/uploads/tv-ad-600x338.jpg", "https://4.bp.blogspot.com/-wS-yDxHcjZQ/WONh2WNFB5I/AAAAAAAAMQU/sHLm2e3tUH4DP76OhY9vaGtLMf3P8IVUACLcB/s1600/BUY1TAKE1.jpg"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack5, feedBack6],
            catalog : [Catalog(name: "menu", img_url: "https://b.zmtcdn.com/data/menus/107/6302107/2a7f4acb03fb13b2bb1d56a38814ddcd.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"), Catalog(name: "menu", img_url: "https://b.zmtcdn.com/data/menus/956/6301956/9ec5dd0b9847a0f0ac4f3104d11c3aed.jpg")]
        )
        
        var business7 = Businesses(
            _id : "business_0005",
            name : "Coffee Bean and Tea Leaf",
            branches : 43,
            img_url : "https://cdn1.clickthecity.com/images/establishment/common/raw/2807.jpg",
            doc_type : "business",
            type : "drink",
            category : "coffee",
            tags : ["Coffee Bean and Tea Leaf", "tea", "coffee", "pastries", "cakes", "coffee bean", "cafe"],
            address : Addresses(),
            rating : 4.9,
            short_desc : "greatest coffee house",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://c1.staticflickr.com/9/8660/27859346354_93facfe1cf_b.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOL63D_dmPjhF1beUiqkXb-0eL-7_orllquL5q7aL19x_25nLB", "https://japantoday-asset.scdn3.secure.raxcdn.com/img/store/5d/f4/4483e156860886a2c1d5ced1270e9409935f/coffeebean.png"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack9, feedBack10],
            catalog : [Catalog(name: "menu", img_url: "https://media-cdn.tripadvisor.com/media/photo-s/0e/32/5e/1f/drinks-menu.jpg"), Catalog(name: "menu", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHS__DumIMc8FSrq-DGpazkjyHcqBgFGh4ydmi2INbZZMZEfLR"),Catalog(name: "menu", img_url: "https://media-cdn.tripadvisor.com/media/photo-s/0e/32/5e/17/breakfast-pasta-wraps.jpg"),Catalog(name: "menu", img_url: "https://media-cdn.tripadvisor.com/media/photo-s/0c/e7/22/81/food-drinks-menu.jpg")]
        )
        
        var business8 = Businesses(
            _id : "business_0005",
            name : "Sisig Place",
            branches : 43,
            img_url : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUyvij99kgDWP9sHpLRBlYnoAQptfexA3pxzvgRolRwQ3eadOB",
            doc_type : "business",
            type : "food",
            category : "food",
            tags : ["Sisig Place", "sisig", "food", "rice", "meal", "sizzling"],
            address : Addresses(),
            rating : 0.9,
            short_desc : "best sisig place",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: ["https://images.ctfassets.net/uexfe9h31g3m/288ATWgcEgyCqYCoAMeoq0/370627e6c8e82dc143d4b6e005199b1f/quorn-sisig.jpg?fm=jpg&fit=thumb&w=1200&h=630","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvrI3Uky966rtAzBsdCrK5Ozek2ag_8P5aQWzaUgV5BrCvJmXN2w"]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [feedBack1, feedBack2],
            catalog : [Catalog(name: "Chicken Sisig", img_url: "http://www.panlasangpinoymeatrecipes.com/wp-content/uploads/chicken_sisig.jpg"), Catalog(name: "Pork Sisig", img_url: "https://balay.ph/wp-content/uploads/2016/10/Pork-Sisig-Recipe.jpg"),Catalog(name: "Fish Sisig", img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSt3AM5rMBX-JeC6k9zDHzWqXzObtqTtXVGsTDsW9ip6gKckjNx")]
        )
        
        return [business1, business2, business3, business4, business5, business6, business7, business8]
    }()
    
    static var ads : [Ads] = {
        var ad1 = Ads()
        ad1.name = "@CoffeeBean"
        ad1.icon = "https://www.coffeebean.com.sg/media/CBTL_NitroBrewWebbanner.png"
        
        var ad2 = Ads()
        ad2.name = "@Shakeys"
        ad2.icon = "https://spavi-noo-prod.s3.ap-southeast-1.amazonaws.com/s3fs-public/slideshow/2018-07/B1T1%20WEB%20BANNER.jpg"
        
        var ad3 = Ads()
        ad3.name = "@BurgerKing"
        ad3.icon = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRi6-kQBuBWL1gu46Yin4mcQM-IASe0cqVYAJiTCK8nqNnjyHeb7w"
        
        return [ad1, ad2, ad3]
    }()
    
    static var categories : [ThumbnailCells] = {
        var category1 = ThumbnailCells()
        category1.name = "american"
        category1.img_url = "https://api-content.prod.pizzahutaustralia.com.au//umbraco/api/Image/Get2?path=assets/products/menu/Meat-Super-Supreme-Pizza-3250-menu.jpg"
        category1.icon = ""
        
        var category2 = ThumbnailCells()
        category2.name = "chinese"
        category2.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYMHMAn3b6n0lPRy-RA9_h2aaaeIFFBE09ugIKTEqO54FMupSx"
        category2.icon = ""
        
        var category3 = ThumbnailCells()
        category3.name = "french"
        category3.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYyZo_-95yxd_3jDdOwI7mnWrTBrPX3GL5fK0aeItDvtlIrtDS"
        category3.icon = ""
        
        var category4 = ThumbnailCells()
        category4.name = "mexican"
        category4.img_url = "https://olive.qa/wp-content/uploads/2018/08/mexican-food.jpg"
        category4.icon = ""
        
        var category5 = ThumbnailCells()
        category5.name = "coffee"
        category5.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIqHzgLxmQG_jkpGD6kDKdH1eMtiwKBTXhgRLHGuZn6Af5GuWU"
        category5.icon = ""
        
        var category6 = ThumbnailCells()
        category6.name = "teas"
        category6.img_url = "https://culinaryginger.com/wp-content/uploads/2014/11/afternoon-tea-2.jpg"
        category6.icon = ""
        
        var category7 = ThumbnailCells()
        category7.name = "japanese"
        category7.img_url = "https://st2.depositphotos.com/2287637/9820/i/950/depositphotos_98208624-stock-photo-sushi-set-nigiri-and-sashimi.jpg"
        category7.icon = ""
        
        var category8 = ThumbnailCells()
        category8.name = "pasta"
        category8.img_url = "https://cdn.vox-cdn.com/thumbor/92Psv_m7KJHKoAoaagT3-QE1WPg=/0x0:4500x3000/1200x800/filters:focal(1890x1140:2610x1860)/cdn.vox-cdn.com/uploads/chorus_image/image/57391659/pasta_flyer.0.jpg"
        category8.icon = ""
        
        var category9 = ThumbnailCells()
        category9.name = "cakes"
        category9.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAprYqehKhhepm-mtubaZJX7G-pjMwrONc0K50W9RuuCpl6_XRzA"
        category9.icon = ""
        
        var category10 = ThumbnailCells()
        category10.name = "indian"
        category10.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEwtg8O5e39YYPfahRf3qvRR9oVRAqpltgtXF0ATmJFIgOZ-Zp"
        category10.icon = ""
        
        var category11 = ThumbnailCells()
        category11.name = "thailand"
        category11.img_url = "https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/32073/SITours/evening-thai-dinner-and-huangpu-river-cruise-in-shanghai-in-shanghai-473979.jpg"
        category11.icon = ""
        
        var category12 = ThumbnailCells()
        category12.name = "korean"
        category12.img_url = "https://cdn.vox-cdn.com/thumbor/FRygTTZd5VpkgkX5XfzJ_LJQxsk=/0x0:1000x1000/1200x800/filters:focal(420x420:580x580)/cdn.vox-cdn.com/uploads/chorus_image/image/57396595/Gen_Korean_BBQ_House.0.jpg"
        category12.icon = ""
        
        var category13 = ThumbnailCells()
        category13.name = "bulalo"
        category13.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhODGsEc7Ak6F_d1EdtzniDGR-Qh70Y6TpxstjtLVy9pU8UMP_"
        category13.icon = ""
        
        var category14 = ThumbnailCells()
        category14.name = "sisig"
        category14.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR64uo5TMnjWuEx9W3bLnTlReaUmVr2rYs8IkQL3tF6vC8mp0E3"
        category14.icon = ""
        
        var category15 = ThumbnailCells()
        category15.name = "laksa"
        category15.img_url = "https://d2mkh7ukbp9xav.cloudfront.net/recipeimage/83l5armx-c93cc-098721-cfcd2-rgtgtix4/dfc4d4e4-be2e-4490-8569-a6ba98f46fb8/main/chicken-laksa.jpg"
        category15.icon = ""
        
        var category16 = ThumbnailCells()
        category16.name = "curry"
        category16.img_url = "https://www.sbs.com.au/food/sites/sbs.com.au.food/files/styles/full/public/gettyimages-673858790.png?itok=XbCJ4Tb2"
        category16.icon = ""
        
        var category17 = ThumbnailCells()
        category17.name = "sushi"
        category17.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6nH2XKXH0U5XDmQaSL61SeA5q_RTbSjkF60foS3TrE6IYP02m4Q"
        category17.icon = ""
        
        var category18 = ThumbnailCells()
        category18.name = "ramen"
        category18.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLk-3fG3isgTb3Eirgml0l-FroVbIvb8jvmaVm64VG34_pF-HC"
        category18.icon = ""
        
        var category19 = ThumbnailCells()
        category19.name = "cookies"
        category19.img_url = "https://chefsavvy.com/wp-content/uploads/coconut-oil-cookies-1.jpg"
        category19.icon = ""
        
        var category20 = ThumbnailCells()
        category20.name = "doughnuts"
        category20.img_url = "https://images.food52.com/40TQaaZXuxhETH3rTyCPdcZMBWs=/7488420c-5da7-4139-adad-16deb87ba99d--16114269763_81ee2c9fae_b.jpg"
        category20.icon = ""
        
        var category21 = ThumbnailCells()
        category21.name = "nachos"
        category21.img_url = "https://www.foodinaminute.co.nz/var/fiam/storage/images/recipes/quick-and-easy-nachos/1289674-22-eng-US/Quick-and-Easy-Nachos.jpg"
        category21.icon = ""
        
        var category22 = ThumbnailCells()
        category22.name = "burger"
        category22.img_url = "https://www.eatthis.com/wp-content/uploads/media/images/ext/524030441/edit-BK-Whopper-500x389.jpg"
        category22.icon = ""
        
        var category23 = ThumbnailCells()
        category23.name = "salad"
        category23.img_url = "https://cdn.cdkitchen.com/images/cats/969/cat-969-720-1.jpg"
        category23.icon = ""
        
        var category24 = ThumbnailCells()
        category24.name = "juice bar"
        category24.img_url = "https://cdn1.medicalnewstoday.com/content/images/articles/320/320834/bottles-of-fruit-juice.jpg"
        category24.icon = ""
        
        return [category1, category2, category3, category4, category5, category6, category7, category8, category9, category10, category11, category12, category13, category14, category15, category16, category17, category18, category19, category20, category21, category22, category23, category24]
    }()
    
    
    //dashboard data
    static var dashCategories : [ThumbnailCells] = {
        var category1 = ThumbnailCells()
        category1.name = "pizza"
        category1.img_url = ""
        category1.icon = "pizza"
        
        var category2 = ThumbnailCells()
        category2.name = "chinese"
        category2.img_url = ""
        category2.icon = "noodles"
        
        var category3 = ThumbnailCells()
        category3.name = "burger"
        category3.img_url = ""
        category3.icon = "burger"
        
        var category4 = ThumbnailCells()
        category4.name = "taco"
        category4.img_url = ""
        category4.icon = "taco"
        
        var category5 = ThumbnailCells()
        category5.name = "coffee"
        category5.img_url = ""
        category5.icon = "coffee_bean"
        
        var category6 = ThumbnailCells()
        category6.name = "teas"
        category6.img_url = ""
        category6.icon = "tea_bag"
        
        var category7 = ThumbnailCells()
        category7.name = "japanese"
        category7.img_url = ""
        category7.icon = "maki"
        
        return [category1, category2, category3, category4, category5, category6, category7]
    }()
    
    func services(hasIcon : Bool) -> [ThumbnailCells] {
        return {
            var service1 = ThumbnailCells()
            service1.name = "call service questions"
            service1.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6tCYnF5ZTrVme3o7WCkowdkAxCGT03_8N8fya1kKSsXCzdb9L"
            if hasIcon {
                service1.icon = "telephone"
            } else {
                service1.icon = ""
            }
            
            var service2 = ThumbnailCells()
            service2.name = "cars automotive workshop oil change"
            service2.img_url = "https://spglobalinc.com/wp-content/uploads/AutoRepairShop-1024x534.jpeg"
            if hasIcon {
                service2.icon = "wheel"
            } else {
                service2.icon = ""
            }
            
            var service3 = ThumbnailCells()
            service3.name = "plumber water pipe"
            service3.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdCQMG5XwXXSxci-Q5L5Lm0bkYlEUrhN3rTmfv0igLx8G7BIfvnw"
            if hasIcon {
                service3.icon = "drop"
            } else {
                service3.icon = ""
            }
            
            var service4 = ThumbnailCells()
            service4.name = "electrician electrical"
            service4.img_url = "https://us.123rf.com/450wm/kadmy/kadmy1308/kadmy130800086/21716818-young-adult-electrician-builder-engineer-inspecting-electric-counter-equipment-in-distribution-fuse-.jpg?ver=6"
            if hasIcon {
                service4.icon = "plug"
            } else {
                service4.icon = ""
            }
            
            var service5 = ThumbnailCells()
            service5.name = "doctor"
            service5.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVGaEAIvPXjCXpuO6pskZ1W26-V-xMipVNsAgVwF4jsgX3PvOy"
            if hasIcon {
                service5.icon = "stethoscope"
            } else {
                service5.icon = ""
            }
            
            var service6 = ThumbnailCells()
            service6.name = "architect house construction"
            service6.img_url = "https://c1.dq1.me/uploads/article_block/27738/article_featured_image/55009/thumb_architect-construction-plans.jpg"
            if hasIcon {
                service6.icon = "ruler_pencil"
            } else {
                service6.icon = ""
            }
            
            var service7 = ThumbnailCells()
            service7.name = "gas station oil change"
            service7.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMzOrfQ89WfA-MFLZ_4Ya-Jg3GPEj9DJuxW1tMueVsIxkgkB_o"
            if hasIcon {
                service7.icon = "gas"
            } else {
                service7.icon = ""
            }
            
            var service8 = ThumbnailCells()
            service8.name = "store grocery buy food"
            service8.img_url = "https://adultswithallergies.files.wordpress.com/2017/12/istock-672450320.jpg?w=672&h=372&crop=1"
            if hasIcon {
                service8.icon = "store"
            } else {
                service8.icon = ""
            }
            
            var service9 = ThumbnailCells()
            service9.name = "dentist teeth cavity"
            service9.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxPRUwW9VrQYqme4ya4UxeClxsmmqgX-bOVvuq4zLDoUDVpji4"
            if hasIcon {
                service9.icon = "dentist"
            } else {
                service9.icon = ""
            }
            
            var service10 = ThumbnailCells()
            service10.name = "court justice"
            service10.img_url = "https://sa.kapamilya.com/absnews/abscbnnews/media/2017/news/01/26/20140923_supreme-court.jpg"
            if hasIcon {
                service10.icon = "court"
            } else {
                service10.icon = ""
            }
            
            return [service1, service2, service3, service4, service5, service6, service7, service8, service9, service10]
        }()
    }
    
    static var dashTries : [ThumbnailCells] = {
        var try1 = ThumbnailCells()
        try1.name = "chicken wings"
        try1.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRML-Casm4PngzTApqJxrE6VWm6E28SevnrpYKxn3xTqJLUyhYp"
        try1.icon = ""
        
        var try2 = ThumbnailCells()
        try2.name = "noodles"
        try2.img_url = "https://static1.squarespace.com/static/54678365e4b011326d571829/t/5aafbe7870a6adbf54ed60db/1521467005272/"
        try2.icon = ""
        
        var try3 = ThumbnailCells()
        try3.name = "lasagna"
        try3.img_url = "https://reciperunner.com/wp-content/uploads/2016/09/Lasagna-Sloppy-Joes3.jpg"
        try3.icon = ""
        
        var try4 = ThumbnailCells()
        try4.name = "yogurt"
        try4.img_url = "https://fooddonelight.com/wp-content/uploads/2016/01/chocolate-covered-strawberry-yogurt-3782.jpg"
        try4.icon = ""
        
        var try5 = ThumbnailCells()
        try5.name = "bread"
        try5.img_url = "https://s3.amazonaws.com/content.barefootwine.com/productionV2/s3fs-public/2018-04/moscatowinecake-compressor.jpg?n_hFGLcr1NO8_ZH9IKCutQ82H0JvUYgj"
        try5.icon = ""
        
        var try6 = ThumbnailCells()
        try6.name = "breakfast"
        try6.img_url = "https://cdn-image.realsimple.com/sites/default/files/styles/portrait_435x518/public/egg-muffins.jpg?itok=MMcyF4dH"
        try6.icon = ""
        
        var try7 = ThumbnailCells()
        try7.name = "sandwich"
        try7.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8VLNkxigQ49k2wzH4Yq2VUsS538YQVwqW7QOjPQyU1qQpatXi"
        try7.icon = ""
        
        return [try1, try2, try3, try4, try5, try6, try7]
    }()
}
