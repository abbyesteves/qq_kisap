//
//  Service.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 18/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class Service {
    
    func Town() -> Towns {
        return Towns(town_id: "", doc_type: "", name: "", initials: "", short_desc: "", tagline: "", address: Addresses(), rating: 0.0, config: Configurations(logo: "", theme_color: "", gallery: [""]), last_updated: "", town_version: "", officials: Officials(mayor: Mayors(), vice_mayor: Mayors(), counselors: [""], term: ""), businesses: [""], baranggay: [Baranggays(name: "", captain: "", lat: 0.0, long: 0.0)], forms: [Forms(name: "", data: "")], gov_services: Services(categories: [""], police: [ServiceObjects()], fire: [ServiceObjects()], hospital: [ServiceObjects()], risk_management: [ServiceObjects()]), news: [Bulletins()], events: [Bulletins()], didSelect: false)
    }
    
    func tags() -> [String] {
        var tags = [String]()
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("preference")
        let document = database.document(withID: "preference")!
        let properties = document.properties
        
        if properties != nil {
            tags = (properties!["tags"])! as! [String]
        }
        return tags
    }
    
    func savedId()-> [String] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("town")
        let query = database.createAllDocumentsQuery()
        var townsId = [String]()
        
        do {
            let result = try query.run()
            while let row = result.nextRow(){
                let document = row.document?.properties
                //                    var town = Towns()
                if document != nil {
                    if !(townsId.contains("\((document!["town_id"])!)")) {
                        townsId.append("\((document!["town_id"])!)")
                    }
                }
            }
        } catch {
            print(" error getting announcements :", error)
        }
        return townsId
    }
    
    func getTowns() {
        guard let path = Bundle.main.path(forResource: "towns", ofType: "json") else { return }
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            guard let stores = json as? [Any] else { return }
            
            for eachstore in stores {
                guard let store = eachstore as? [String: Any] else { return }
                print(" path via json as Any ", store["name"])
            }
        } catch {
            print("Error!! Unable to parse  \(error)")
        }
    }
    
    func loadJson(fileName: String) -> [String: AnyObject]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = object as? [String: AnyObject] {
                    return dictionary
                }
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        return nil
    }
    
    func hexUIColor(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func dateFormat(date : String, format : String) -> String {
        let date = Date(timeIntervalSince1970: Double(date)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "\(format)"
        return dateFormatter.string(from: date)
    }
    
    func dateSince(date: String) -> String {
        let datePrev = Date(timeIntervalSince1970: Double(date)!)
        let dateSent = Date(timeIntervalSince1970: Double(Date().timeIntervalSince1970))
        let elapsed = dateSent.timeIntervalSince(datePrev)
        let minute = Int(elapsed/60)
        let hour = Int(minute/60)
        let day = Int(hour/24)
        let week = Int(day/7)
        var recent = String()
        
        if minute <= 5 {
            recent = "now"
        } else if minute >= 6 && minute <= 59 {
            recent = "\(minute)m ago"
        } else if minute >= 60 && hour <= 1 {
            recent = "1h ago"
        } else if hour >= 2 && hour <= 23 {
            recent = "\(hour)hs ago"
        } else if hour >= 24 && day <= 1 {
            recent = "1d ago"
        } else if day >= 2 && day <= 6 {
            recent = "\(day)ds ago"
        } else if day >= 7 && day <= 8 {
            recent = "\(week)w ago"
        } else if day >= 9 && week <= 4 {
            recent = "\(week)ws ago"
        } else if week >= 5 {
            recent = dateFormat(date: "\(date)", format: "MM dd,yy")
        }
        
        return recent
    }
    
    func setColor(rating: Double) -> UIColor {
        if rating <= 1 {
            return UIColor.Low(alpha: 1.0)
        } else if rating <= 2 {
            return UIColor.LowMid(alpha: 1.0)
        } else if rating <= 3 {
            return UIColor.Mid(alpha: 1.0)
        } else if rating <= 4 {
            return UIColor.MidHigh(alpha: 1.0)
        }
        return UIColor.High(alpha: 1.0)
    }
    
    func resetCell(cell: UICollectionViewCell, indexPath : IndexPath) {
        for views in cell.subviews {
            views.removeFromSuperview()
        }
    }
}
