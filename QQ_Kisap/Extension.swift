//
//  Extension.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func Background(alpha: CGFloat) -> UIColor {
        return UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: alpha)
    }
    
    static func Low(alpha: CGFloat) -> UIColor {
        //red
        return UIColor(red: 196/255, green: 60/255, blue: 60/255, alpha: alpha)
    }
    
    static func LowMid(alpha: CGFloat) -> UIColor {
        //yellow
        return UIColor(red: 203/255, green: 205/255, blue: 44/255, alpha: alpha)
    }
    
    static func Mid(alpha: CGFloat) -> UIColor {
        //orange
        return UIColor(red: 219/255, green: 154/255, blue: 51/255, alpha: alpha)
    }
    
    static func MidHigh(alpha: CGFloat) -> UIColor {
        //yellow green
        return UIColor(red: 92/255, green: 163/255, blue: 52/255, alpha: alpha)
    }
    
    static func High(alpha: CGFloat) -> UIColor {
        // green
        return UIColor(red: 15/255, green: 113/255, blue: 27/255, alpha: alpha)
    }
    
    static func Base(alpha : CGFloat) -> UIColor {
        let town = CouchService().town()
        let color = Service().hexUIColor(hex: "#\((town.config.theme_color)!)")
        if town.config.theme_color != nil {
            if (town.config.theme_color)! != "" {
                return UIColor(hue: color.hueComponent, saturation: 0.39, brightness: 0.95, alpha: alpha)
            }
        }
        return UIColor(red: 142/255, green: 200/255, blue: 248/255, alpha: alpha)
    }
    
    static func Main(alpha : CGFloat) -> UIColor {
        let town = CouchService().town()
        if town.config.theme_color != nil {
            if (town.config.theme_color)! != "" {
                return Service().hexUIColor(hex: "#\((town.config.theme_color)!)")
            }
        }
        return UIColor(red: 46/255, green: 157/255, blue: 244/255, alpha: alpha)
    }
    
    static func Bg(alpha : CGFloat) -> UIColor {
        let town = CouchService().town()
        let color = Service().hexUIColor(hex: "#\((town.config.theme_color)!)")
        if town.config.theme_color != nil {
            if (town.config.theme_color)! != "" {
                return UIColor(hue: color.hueComponent, saturation: 0.23, brightness: 0.99, alpha: alpha)
            }
        }
        return UIColor(red: 142/255, green: 200/255, blue: 248/255, alpha: alpha)
    }
    
    static func BgLight(alpha : CGFloat) -> UIColor {
        let town = CouchService().town()
        let color = Service().hexUIColor(hex: "#\((town.config.theme_color)!)")
        if town.config.theme_color != nil {
            if (town.config.theme_color)! != "" {
                return UIColor(hue: color.hueComponent, saturation: 0.04, brightness: 1.0, alpha: alpha)
            }
        }
        return UIColor(red: 240/255, green: 248/255, blue: 255/255, alpha: alpha)
    }
    
    static func Boarder(alpha: CGFloat) -> UIColor {
        return UIColor(red: 230/255, green: 236/255, blue: 240/255, alpha: alpha)
    }
    
    static func rgba(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    public func adjust(hueBy hue: CGFloat = 0, saturationBy saturation: CGFloat = 0, brightnessBy brightness: CGFloat = 0) -> UIColor {
        var currentHue: CGFloat = 0.0
        var currentSaturation: CGFloat = 0.0
        var currentBrigthness: CGFloat = 0.0
        var currentAlpha: CGFloat = 0.0
        
        if getHue(&currentHue, saturation: &currentSaturation, brightness: &currentBrigthness, alpha: &currentAlpha) {
            return UIColor(hue: currentHue + hue,
                           saturation: currentSaturation + saturation,
                           brightness: currentBrigthness + brightness,
                           alpha: currentAlpha)
        } else {
            return self
        }
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
    var hueComponent: CGFloat {
        var hue: CGFloat = 0
        getHue(&hue, saturation: nil, brightness: nil, alpha: nil)
        return hue
    }
}

extension UIView {
    static func navigationBar(color : UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 3
        return view
    }
    
    static func statusBar(color : UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
    
    static func border(color : UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
}

extension UILabel {
    
    static func titleLabel(text : String, alignment : NSTextAlignment, font : UIFont, color : UIColor) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = font//UIFont(name: "SFProDisplay-Medium", size: 15)!//.systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 1))
        label.textColor = color//UIColor.Main(alpha: 1.0)
        label.textAlignment = alignment
        label.text = text
        return label
    }
    
    static func subTitleLabel(text : String, alignment : NSTextAlignment) -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: 13)
        label.textAlignment = alignment
        label.text = text
        return label
    }
    
}

extension UIButton {
    func pulse() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.4
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = false//true
        pulse.repeatCount = 0
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
    }
    
    static func allButton(fontColor : UIColor, text: String) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(name: "SFProDisplay-Heavy", size: 13)!
        button.setTitleColor(fontColor, for: .normal)
        button.setTitle(text.capitalized, for: .normal)
        return button
    }
}

extension UITextField {
    
    static func searchBar(placeholder : String) -> UITextField {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.Main(alpha: 1.0)
        text.font = .systemFont(ofSize: 13)
        var placeholderMutable = NSMutableAttributedString()
        placeholderMutable = NSMutableAttributedString(attributedString: NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.Base(alpha: 1.0)]))
        text.leftPadding(space: 10)
        text.attributedPlaceholder = placeholderMutable
        return text
    }
    
    func leftPadding(space: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: space, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}

extension UITextView {
    static func subTextView(text : String, textColor : UIColor) -> UITextView {
        let textView = UITextView()
        textView.textColor = textColor
        textView.font = UIFont(name: "SFProDisplay-Regular", size: 15)!
        textView.backgroundColor = .clear
        textView.text = text
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.textAlignment = .justified
        return textView
    }
}

extension UIImage {
    
    func overlayImage(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        
        color.setFill()
        
        context!.translateBy(x: 0, y: self.size.height)
        context!.scaleBy(x: 1.0, y: -1.0)
        
        context!.setBlendMode(CGBlendMode.colorBurn)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context!.draw(self.cgImage!, in: rect)
        
        context!.setBlendMode(CGBlendMode.sourceIn)
        context!.addRect(rect)
        context!.drawPath(using: CGPathDrawingMode.fill)
        
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return coloredImage!
    }
    
    func scaleImage(newSize: CGSize) -> UIImage? {
        var newImage: UIImage?
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext(), let cgImage = self.cgImage {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(cgImage, in: newRect)
            if let img = context.makeImage() {
                newImage = UIImage(cgImage: img)
            }
            UIGraphicsEndImageContext()
        }
        return newImage
    }
}
