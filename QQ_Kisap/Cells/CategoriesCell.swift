//
//  DashCategoriesCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//
import UIKit
class CategoriesCell: BaseCell {
    var category: ThumbnailCells? {
        didSet {
            nameLabel.text = category?.name.capitalized
            iconImage.image = UIImage(named: "\((category?.icon)!)".lowercased())?.withRenderingMode(.alwaysTemplate)
            iconImage.tintColor = UIColor.Main(alpha: 1.0)
            adImage.kf.setImage(with: URL(string: "\((category?.img_url)!)"))
        }
    }
    
    let iconView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        return view
    }()
    
    let iconImage : UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: "SFProDisplay-Regular", size: 13)!//.systemFont(ofSize: 13, weight: UIFont.Weight(rawValue: 5))
        label.textAlignment = .center
        label.text = "Title"
        return label
    }()
    
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private func setupView() {
        iconView.layer.cornerRadius = (frame.width-(20+10))/2
        adImage.layer.cornerRadius = iconView.layer.cornerRadius
        addSubview(iconView)
        iconView.addSubview(adImage)
        iconView.addSubview(iconImage)
        addSubview(nameLabel)
    }
    
    private func setupConstraints() {
        let size = frame.width-(20+10)
        iconView.frame = CGRect(x: (frame.width/2)-(size/2), y: 0, width: size, height: size)
        adImage.frame = CGRect(x: 0, y: 0, width: size, height: size)
        iconImage.frame = CGRect(x: 15, y: 15, width: iconView.frame.width-30, height: iconView.frame.height-30)
        nameLabel.frame = CGRect(x: 0, y: iconView.frame.maxY+5, width: frame.width, height: 20)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
