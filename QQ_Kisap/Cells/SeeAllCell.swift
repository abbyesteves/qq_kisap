//
//  SeeAllCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/11/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SeeAllCell: BaseCell {
    var business: Businesses? {
         didSet {
            adImage.kf.setImage(with: URL(string: "\((business?.img_url)!)"))
            nameLabel.text = business?.name.capitalized
            descText.text = business?.short_desc.capitalized
            ratingButton.backgroundColor = Service().setColor(rating: Double("\((business?.rating)!)")!)
            ratingButton.setTitle("\((business?.rating)!)⭑".capitalized, for: .normal)
        }
    }
    
    let indexLabel : UILabel = {
        let label = UILabel()
        return label
    }()
    
    let iconImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_marker")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Mid(alpha: 1.0)
        return imageView
    }()
    
    let nameLabel = UILabel.titleLabel(text : "Rate this store!", alignment : .left, font : UIFont(name: "SFProDisplay-Heavy", size: 17)!, color : UIColor.darkGray)
    let descText = UILabel.titleLabel(text : "", alignment : .left, font : UIFont(name: "SFProDisplay-Regular", size: 15)!, color : UIColor.darkGray)
    let distanceText = UILabel.titleLabel(text : "650 m -", alignment : .left, font : UIFont(name: "SFProDisplay-Regular", size: 13)!, color : UIColor.lightGray)
    let addressText = UILabel.titleLabel(text : "SM Southmall, Las Piñas City", alignment : .left, font : UIFont(name: "SFProDisplay-Regular", size: 13)!, color : UIColor.lightGray)
    let ifOpenText = UILabel.titleLabel(text : "open now".uppercased(), alignment : .left, font : UIFont(name: "SFProDisplay-Regular", size: 12)!, color : UIColor.Main(alpha: 1.0))
    let ratingButton : UIButton = {
        let button = UIButton()
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.5))
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 25/4
        return button
    }()
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private func setupView() {
        addSubview(indexLabel)
        addSubview(adImage)
        addSubview(ifOpenText)
        addSubview(nameLabel)
        addSubview(descText)
        addSubview(iconImage)
        addSubview(distanceText)
        addSubview(addressText)
        addSubview(ratingButton)
    }
    
    private func setupConstraints() {
        adImage.frame = CGRect(x: 10, y: 10, width: frame.width/4, height: frame.height-20)
        ifOpenText.frame = CGRect(x: adImage.frame.maxX+10, y: 10, width: frame.width-(adImage.frame.width+20+5+50), height: 20)
        nameLabel.frame = CGRect(x: adImage.frame.maxX+10, y: ifOpenText.frame.maxY, width: ifOpenText.frame.width, height: 20)
        descText.frame = CGRect(x: adImage.frame.maxX+10, y: nameLabel.frame.maxY, width: nameLabel.frame.width, height: (frame.height-(nameLabel.frame.height+20+20))/2)
        iconImage.frame = CGRect(x: adImage.frame.maxX+10, y: descText.frame.maxY+3.25, width: 13, height: 13)
        distanceText.frame = CGRect(x: iconImage.frame.maxX+5, y: descText.frame.maxY, width: 50, height: descText.frame.height)
        addressText.frame = CGRect(x: distanceText.frame.maxX, y: descText.frame.maxY, width: descText.frame.width-(distanceText.frame.width+iconImage.frame.width), height: descText.frame.height)
        ratingButton.frame = CGRect(x: frame.width-70, y: 10, width: 50, height: 25)
        
    }
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
