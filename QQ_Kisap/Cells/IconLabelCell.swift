//
//  DashMustTriesCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Kingfisher

class IconLabelCell: BaseCell {
    var value: ThumbnailCells? {
        didSet {
            adImage.kf.setImage(with: URL(string: "\((value?.img_url)!)"))
            if "\((value?.icon)!)" != "" {
                iconImage.image = UIImage(named: "\((value?.icon)!)".lowercased())?.withRenderingMode(.alwaysTemplate)
                iconImage.tintColor = UIColor.white
                 blackView.backgroundColor = UIColor(white: 0, alpha: 0.3)
            } else {
                iconImage.image = UIImage(named: "")
                 blackView.backgroundColor = UIColor(white: 0, alpha: 0.0)
            }
            
        }
    }
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.3)
        return view
    }()
    
    let iconImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.shadowColor = UIColor.darkGray.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private func setupView() {
        addSubview(adImage)
        addSubview(blackView)
        addSubview(iconImage)
    }
    
    private func setupConstraints() {
        adImage.frame = self.bounds
        blackView.frame = adImage.frame
        iconImage.frame = CGRect(x: frame.width/2-25, y: frame.height/2-25, width: 50, height: 50)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

