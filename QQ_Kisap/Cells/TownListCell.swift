//
//  TownListCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 18/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class TownListCell: BaseCell {
    let saved = Service().savedId()
    var town: Towns? {
        didSet {
            nameLabel.text = "\((town?.name)!), \((town?.address.province)!)"
            
            if saved.contains((town?.town_id)!) {
                arrowImageView.image = UIImage(named: "ic_arrow_right")?.withRenderingMode(.alwaysTemplate)
//                arrowImageView.transform = CGAffineTransform(rotationAngle: 0)
            } else {
                arrowImageView.image = UIImage(named: "ic_download")?.withRenderingMode(.alwaysTemplate)
            }
            arrowImageView.tintColor = UIColor.Main(alpha: 1.0)
        }
    }
    
    let indexLabel : UILabel = {
        let label = UILabel()
        return label
    }()
    
    let ratingLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = UIColor.Main(alpha: 1.0)
        label.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 2))
        label.text = "⭑⭑⭑⭑⭒".capitalized
        return label
    }()
    
    let arrowView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let arrowImageView : UIImageView = {
        let imageView = UIImageView()
//        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        return imageView
    }()
    
    let nameLabel = UILabel.titleLabel(text : "Town name", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.darkGray)
    let rangeLabel = UILabel.subTitleLabel(text: "0.5 km Range From Current Location", alignment: .left)
    
    private func setupView() {
        addSubview(nameLabel)
        addSubview(rangeLabel)
        addSubview(ratingLabel)
        addSubview(arrowView)
        arrowView.addSubview(indexLabel)
        arrowView.addSubview(arrowImageView)
    }
    
    private func setupConstraints() {
        nameLabel.frame = CGRect(x: 20, y: 12.5, width: frame.width-(40+50), height: 25)
        rangeLabel.frame = CGRect(x: 20, y: nameLabel.frame.maxY, width: frame.width-(40+50), height: 25)
        ratingLabel.frame = CGRect(x: 20, y: rangeLabel .frame.maxY, width: frame.width-(40+50), height: 25)
        arrowView.frame = CGRect(x: nameLabel.frame.maxX, y: frame.height/2-25, width: 50, height: 50)
        arrowImageView.frame = CGRect(x: 0, y: 0, width: arrowView.frame.width, height: arrowView.frame.height)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
