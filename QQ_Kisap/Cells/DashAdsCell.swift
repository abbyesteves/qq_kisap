//
//  DashAdsCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 16/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Kingfisher

class DashAdsCell: BaseCell {
    var ad: Ads? {
        didSet {
            handlerLabel.text = ad?.name
            adImage.kf.setImage(with: URL(string: "\((ad?.icon)!)"))
        }
    }
    
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.Main(alpha: 1.0)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let handlerLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = UIColor.white
        label.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 5))
        label.textAlignment = .center
        return label
    }()
    
    private func setupView() {
        addSubview(handlerLabel)
        addSubview(adImage)
    }
    
    private func setupConstraints() {
        handlerLabel.frame = self.bounds
        adImage.frame = self.bounds
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
