//
//  DashStoresCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Kingfisher

class DashBusinessCell: BaseCell {
    var business: Businesses? {
        didSet {
            nameLabel.text = business?.name.capitalized
            descLabel.text = business?.short_desc
            branchButton.setTitle("\((business?.branches)!) stores".uppercased(), for: .normal)
            adImage.kf.setImage(with: URL(string: "\((business?.img_url)!)"))
        }
    }
    
    let gradientLayer = CAGradientLayer()
    
    let indexLabel : UILabel = {
        let label = UILabel()
        return label
    }()
    
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 10))
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.layer.shadowColor = UIColor.darkGray.cgColor
        label.layer.shadowOpacity = 0.5
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowRadius = 3
        return label
    }()
    
    let descLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = .systemFont(ofSize: 18)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.layer.shadowColor = UIColor.darkGray.cgColor
        label.layer.shadowOpacity = 0.5
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowRadius = 3
        return label
    }()
    
    let branchButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: UIFont.Weight(rawValue: 5))
        button.setTitleColor(UIColor.Main(alpha: 1.0), for: .normal)
        button.layer.cornerRadius = 10
        return button
    }()
    
    private func setupView() {
        gradientLayer.colors = [UIColor.clear, UIColor(white: 0, alpha: 0.5).cgColor]

        addSubview(adImage)
        layer.addSublayer(gradientLayer)
        addSubview(indexLabel)
        addSubview(nameLabel)
        addSubview(descLabel)
        addSubview(branchButton)
    }
    
    private func setupConstraints() {
        adImage.frame = self.bounds
        gradientLayer.frame = CGRect(x: 0, y: frame.height-(frame.height/3), width: frame.width, height: frame.height/3)
        nameLabel.frame = CGRect(x: 20, y: 20, width: frame.width-40, height: 30)
        descLabel.frame = CGRect(x: 20, y: nameLabel.frame.maxY, width: frame.width-40, height: 30)
        branchButton.frame = CGRect(x: (frame.width/2)-50, y: descLabel.frame.maxY+10, width: 100, height: 30)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
