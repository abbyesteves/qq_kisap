//
//  DashTopServicesCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Kingfisher
class TopServicesCell: BaseCell {
    var service: ThumbnailCells? {
        didSet {
            adImage.kf.setImage(with: URL(string: "\((service?.img_url)!)"))
        }
    }
    
    let adImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private func setupView() {
        addSubview(adImage)
    }
    
    private func setupConstraints() {
        adImage.frame = self.bounds
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

