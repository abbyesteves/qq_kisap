//
//  BusinessReviewCell.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 17/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class BusinessReviewCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let reviewId = "reviewId"
    var review: Feedbacks? {
        didSet {
            profileImage.kf.setImage(with: URL(string: "\((review?.user_id)!)"))
            nameLabel.text = review?.name
            reviewText.text = review?.review
            ratingButton.backgroundColor = Service().setColor(rating: Double("\((review?.rating)!)")!)
            ratingButton.setTitle("\((review?.rating)!)⭑".capitalized, for: .normal)
            uploadTotal.text = "\((review?.photos_uploads.count)!)"
        }
    }
    
    let iconImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "camera")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    let ReviewsView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let nameLabel = UILabel.titleLabel(text : "Rate this store!", alignment : .left, font : UIFont(name: "SFProDisplay-Heavy", size: 17)!, color : UIColor.darkGray)
    let reviewText = UITextView.subTextView(text: "", textColor: UIColor.darkGray)
    let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
    let profileImage : UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.Main(alpha: 1.0)
        imageView.layer.cornerRadius = 25
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let ratingButton : UIButton = {
        let button = UIButton()
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.5))
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 25/4
        return button
    }()
    
    let uploadTotal : UILabel = {
        let button = UILabel()
        button.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.5))
        button.textColor = UIColor.lightGray
        return button
    }()
    
    private func setupView() {
        ReviewsView.backgroundColor = UIColor.clear
        ReviewsView.delegate = self
        ReviewsView.dataSource = self
        ReviewsView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reviewId)
        
        addSubview(profileImage)
        addSubview(nameLabel)
        addSubview(ratingButton)
        addSubview(iconImage)
        addSubview(uploadTotal)
        addSubview(border)
        addSubview(reviewText)
        addSubview(ReviewsView)
    }
    
    private func setupConstraints() {
        profileImage.frame = CGRect(x: 10, y: 10, width: 50, height: 50)
        nameLabel.frame = CGRect(x: profileImage.frame.maxX+10, y: 10, width: frame.width-(profileImage.frame.width+20+20), height: 25)
        ratingButton.frame = CGRect(x: nameLabel.frame.minX, y: nameLabel.frame.maxY, width: 40, height: 25)
        iconImage.frame = CGRect(x: ratingButton.frame.maxX+10, y: nameLabel.frame.maxY+3.5, width: 20, height: 18)
        uploadTotal.frame = CGRect(x: iconImage.frame.maxX+5, y: nameLabel.frame.maxY, width: 40, height: 25)
        border.frame = CGRect(x: nameLabel.frame.minX, y: ratingButton.frame.maxY+10, width: nameLabel.frame.width, height: 0.5)
        reviewText.frame = CGRect(x: nameLabel.frame.minX, y: border.frame.maxY, width: nameLabel.frame.width, height: frame.height-(nameLabel.frame.height+ratingButton.frame.height+border.frame.height+20+20+50))
        ReviewsView.frame = CGRect(x: nameLabel.frame.minX, y: reviewText.frame.maxY, width: nameLabel.frame.width, height: 50)
    }
    private func reset(cell : UICollectionViewCell) {
        for views in cell.subviews {
            views.removeFromSuperview()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (review?.photos_uploads.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reviewId, for: indexPath)
        let upload = review?.photos_uploads[indexPath.item]
        cell.backgroundColor = UIColor.Bg(alpha: 1.0)
        self.reset(cell : cell)
        let imageUploaded : UIImageView = {
            let imageView = UIImageView()
            imageView.kf.setImage(with: URL(string: "\((upload)!)"))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
        }()
        
        cell.addSubview(imageUploaded)
        imageUploaded.frame = cell.bounds
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ReviewsView.frame.height, height: ReviewsView.frame.height)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
