//
//  Introduction.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 22/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class Introduction: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    let introId = "introId"
    let town = CouchService().town()
    var abouts = [Abouts]()
    
    let Bocaue : [Abouts] = {
        var about1 = Abouts()
        about1.desc = "Bocaue, officially the Municipality of Bocaue, (Tagalog: Bayan ng Bocaue), is a 1st class municipality in the province of Bulacan, Philippines. According to the 2015 census, it has a population of 119,675 people."
        about1.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSV098gURL5Tba2F6tcKYCNKiB8SnyDkYds4Rc5YSjFGlgbX6vB"
        
        var about2 = Abouts()
        about2.desc = "With the continuous expansion of Metro Manila, the town is now part of the metropolis' built-up area, which reaches San Ildefonso municipality at Bulacan's northernmost part and continues on into Nueva Ecija province. The Bocaue River runs through most of the town."
        about2.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/BocaueHall%2CBulacanjf9865_17.JPG/500px-BocaueHall%2CBulacanjf9865_17.JPG"
        
        var about3 = Abouts()
        about3.desc = "Among its tourist attractions are a town museum located near the municipality's center and the town's river festival celebrated on the first Sunday of every July. The river festival is in commemoration of the Holy Cross of Wawa, believed to be miraculous by the town's predominating Roman Catholic populace."
        about3.img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpHXhGA673vlrJTidnms58rcFi1FLiAW3e_rdpLo9IQdsDaQxM"
        
        var about4 = Abouts()
        about4.desc = "Bocaue was first established by Franciscan missionaries as a barrio and visita of Meycauayan in 1582 and as a town in April 11, 1606 under the advocacy of San Martin de Tours. It was the first town to be granted independence from the old Meycauayan, which was then a very large town comprising the present territories of Meycauayan City, Marilao, Santa Maria, San Jose del Monte City, Obando, and Valenzuela City."
        about4.img_url = "https://media-cdn.tripadvisor.com/media/photo-s/12/4e/9d/0d/img-20180225-081723-largejpg.jpg"
        return [about1, about2, about3, about4]
    }()
    
    let Malolos : [Abouts] = {
        var about1 = Abouts()
        about1.desc = "Malolos, officially the City of Malolos, (Tagalog: Lungsod ng Malolos), or simply known as Malolos City, is a 3rd class city in the province of Bulacan, Philippines. According to the 2015 census, it has a population of 252,074 people."
        about1.img_url = "https://pbs.twimg.com/profile_images/1240493693/Ph_seal_bulacan_malolos_400x400.png"
        
        var about2 = Abouts()
        about2.desc = "It is the capital city of the province of Bulacan as the seat of the provincial government.\nThe city is 45 kilometres (28 mi) north of Manila, the capital city of the Philippines. It is one of the major suburbs conurbated to Metro Manila, situated in the southwestern part of Bulacan, in the Central Luzon Region (Region 3) in the island of Luzon and part of the Metro Luzon Urban Beltway Super Region."
        about2.img_url = "https://upload.wikimedia.org/wikipedia/commons/5/51/RobinsonsPlaceMalolos.jpg"
        
        var about3 = Abouts()
        about3.desc = "Until today, the Roman Catholic faith in Malolos remained intensive. It is evident through the existence of the three stone churches. (Malolos Cathedral, Barasoain Church, and the Santa Isabel Church) Being predominantly Catholic, Malolos, together with the whole province of Bulacan is constituted as Vicaria dela Immaculada Concepcion in which the (Cura de Malolos is the Vicar Forane)."
        about3.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Barasoain_Church_in_Malolos_City.JPG/500px-Barasoain_Church_in_Malolos_City.JPG"
        
        var about4 = Abouts()
        about4.desc = "Malolos is the historical site of the constitutional convention of 1898 that led to the establishment of the First Philippine Republic, the first republic in Asia, led by Emilio Aguinaldo. Malolos served as the capital of the short-lived republic from 1898 to 1899. In 1899, after the Malolos Constitution was ratified, the Universidad Scientifico Literaria de Filipinas was established in Malolos, Bulacan."
        about4.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Malolos_congress.jpg/500px-Malolos_congress.jpg"
        return [about1, about2, about3, about4]
    }()
    
    let Baliuag : [Abouts] = {
        var about1 = Abouts()
        about1.desc = "Baliuag, officially the Municipality of Baliuag, (Tagalog: Bayan ng Baliuag), is a 1st class municipality in the province of Bulacan, Philippines. According to the 2015 census, it has a population of 149,954 people."
        about1.img_url = "https://www.businesslist.ph/img/ph/m/1496992006-53-municipality-of-baliuag-bulacan.png"
        
        var about2 = Abouts()
        about2.desc = "Baliuag was founded in 1732 by Augustinian friars and was incorporated by the Spanish Governor-General on May 26, 1733. The town was a part of Quingua (now Plaridel) before.\nThrough the years of Spanish domination, Baliuag was predominantly agricultural. People had to depend on rice farming for main source of livelihood. Orchards and tumanas yielded fruits and vegetables, which were sold in the public market. Commerce and industry also played important contributions to the economy of the people."
        about2.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/07029jfBaliuag%2C_Bulacan_Town_Hall_Municipal_Office_Buildingsfvf_40.jpg/500px-07029jfBaliuag%2C_Bulacan_Town_Hall_Municipal_Office_Buildingsfvf_40.jpg"
        
        var about3 = Abouts()
        about3.desc = "Mariano Ponce Memorial (Birthplace and Ancestral house, Mariano Ponce street, Subic, Baliuag, Bulacan) Legionares del Trabajo in Barangays Poblacion and Subic, Baliuag, Bulacan, Bulacan (accessed from Maharlika Highway (Cagayan Valley Road, Baliuag-Pulilan-Guiguinto, Bulacan) Pan-Philippine Highway, also known as the Maharlika \"Nobility/freeman\" Highway or Asian Highway 26, Cagayan Valley Road)."
        about3.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Mariano_Ponce_historical_marker.jpg/1920px-Mariano_Ponce_historical_marker.jpg"
        
        return [about1, about2, about3]
    }()
    
    let Plaridel : [Abouts] = {
        var about1 = Abouts()
        about1.desc = "Plaridel, officially the Municipality of Plaridel, (Tagalog: Bayan ng Plaridel), is a 1st class municipality in the province of Bulacan, Philippines. According to the 2015 census, it has a population of 107,805 people."
        about1.img_url = "https://www.zamboanga.com/z/images/6/6d/Plaridel_Bulacan_seal_logo.png"
        
        var about2 = Abouts()
        about2.desc = "Like many towns in Bulacan, Plaridel has its niche in Philippine history as the site of the Battle of Quingua during the Philippine-American War as part of the defense of the First Philippine Republic against the Northern Campaign of the American Army. The battle, manned by Pablo Tecson—Lt. Colonel Pablo Ocampo Tecson of San Miguel, Bulacan—under Gregorio del Pilar on the side of the First Philippine Republic, led to the death of Col. John Stotsenberg of the American Army on April 23, 1899. A marker now stands at the site of the battle in Barangay Agnaya."
        about2.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/St.James_Parish_Church%2CPlaridel%2CBulacan.jpg/440px-St.James_Parish_Church%2CPlaridel%2CBulacan.jpg"
        
        var about3 = Abouts()
        about3.desc = "Plaridel's history can be traced through records back to 1581 in the early years of the Spanish colonization. The Augustinian friars from Malolos Convent discovered a vast forest in 1581 then named as Binto; this would later be known as Quingua. As per the history of the Parish of St. James the Apostle, 2001 issue, Quingua was established by the Augustinian Friars of Malolos who initially named it \"Encomienda Binto\" (Brgy. Bintog got its name from this settlement). They built a visita (chapel of ease) and placed it under the jurisdiction of Fray Mateo Mendoza, the prior of Malolos."
        about3.img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Parkmarianojf.JPG/1920px-Parkmarianojf.JPG"
        
        return [about1, about2, about3]
    }()
    
    let pageIndicator : UIPageControl = {
        let control = UIPageControl()
        control.currentPage = 0
        control.currentPageIndicatorTintColor = UIColor.Main(alpha: 1.0)
        control.pageIndicatorTintColor = UIColor.Bg(alpha: 1.0)
        return control
    }()
    
    let IntroductionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let bgView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.BgLight(alpha: 1.0)
        return view
    }()
    
    let startButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.Main(alpha: 1.0)
        button.titleLabel?.font = UIFont(name: "SFProDisplay-Medium", size: 13)!
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Let's start".uppercased(), for: .normal)
        button.layer.cornerRadius = 5
        return button
    }()
    
    //@objc funcs
    
    @objc func goToHome() {
        let dashboard = Dashboard()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(dashboard, animated: true)
    }
    
    //public private funcs
    private func setupNavigationbar(){
    }
    
    private func setupView(){
        //
        if "\((town.name)!)" == "Bocaue"{
            abouts = Bocaue
        } else if "\((town.name)!)" == "Malolos"{
            abouts = Malolos
        } else if "\((town.name)!)" == "Baliuag"{
            abouts = Baliuag
        } else {
            abouts = Plaridel
        }
        //
        view.backgroundColor = .white
        pageIndicator.numberOfPages = abouts.count
        pageIndicator.currentPage = 0
        
        IntroductionView.backgroundColor = UIColor.clear
        IntroductionView.delegate = self
        IntroductionView.dataSource = self
        IntroductionView.isPagingEnabled = true
        IntroductionView.showsHorizontalScrollIndicator = false
        IntroductionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: introId)
        
        view.addSubview(bgView)
        view.addSubview(IntroductionView)
        view.addSubview(pageIndicator)
        view.addSubview(startButton)
    }
    
    private func setupConstraints(){
        bgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/2)
        IntroductionView.frame = self.view.bounds
        startButton.frame = CGRect(x: 20, y: view.frame.height-70, width: view.frame.width-40, height: 50)
        pageIndicator.frame = CGRect(x: 0, y: startButton.frame.minY-60, width: view.frame.width, height: 30)
    }
    
    private func setupGestures(){
        startButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToHome)))
    }
    
    private func watch(){}
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return abouts.count
    }
    
    private func resetCell(cell : UICollectionViewCell) {
        for views in cell.subviews{
            views.removeFromSuperview()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: introId, for: indexPath)
        let about = abouts[indexPath.item]
        cell.backgroundColor = UIColor.clear
        
        self.resetCell(cell : cell)
        print(" cellForItemAt ", about)
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "Discover \((town.name)!)"
            label.font = UIFont(name: "SFProDisplay-Medium", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(30)
            label.textAlignment = .center
            label.textColor = UIColor.Main(alpha: 1.0)
            return label
        }()
        
        let desc = about.desc != nil ? "\((about.desc)!)" : ""
        
        let subTitleLabel : UITextView = {
            let label = UITextView()
            label.text = desc
            label.font = UIFont(name: "SFProDisplay-Light", size: UIFont.systemFontSize)!
            label.font = .systemFont(ofSize: 15)
            label.backgroundColor = .clear
            label.textAlignment = .justified
            label.textColor = UIColor.darkGray
            label.isEditable = false
            label.isSelectable = false
            return label
        }()
        
        let image : UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.kf.setImage(with: URL(string: about.img_url != nil ? "\((about.img_url)!)" : ""))
            return imageView
        }()
        
        let sizeCaption = CGSize(width: view.frame.width-40, height: view.frame.height-25)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let captionSize = NSString(string: desc).boundingRect(with: sizeCaption, options: options, attributes: [NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Regular", size: 18.1)!], context: nil)
        
        cell.addSubview(titleLabel)
        cell.addSubview(subTitleLabel)
        cell.addSubview(image)
        
        titleLabel.frame = CGRect(x: 20, y: navbarStatusHeight+navBarHeight+20, width: view.frame.width-40, height: 30)
        subTitleLabel.frame = CGRect(x: 20, y: titleLabel.frame.maxY, width: view.frame.width-40, height: captionSize.height)
        image.frame = CGRect(x: 20, y: subTitleLabel.frame.maxY+5, width: view.frame.width-40, height: view.frame.height/2)
//        if indexPath.item == 1 {
//            cell.backgroundColor = .red
//        } else if indexPath.item == 2 {
//            cell.backgroundColor = .green
//        } else if indexPath.item == 3 {
//            cell.backgroundColor = .blue
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let index = Int(IntroductionView.contentOffset.x/view.frame.width)
        pageIndicator.currentPage = index
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        setupNavigationbar()
        setupView()
        setupConstraints()
        setupGestures()
        watch()
    }
}

