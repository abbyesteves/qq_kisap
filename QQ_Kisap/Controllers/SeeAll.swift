//
//  SeeAll.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 14/11/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SeeAll: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    let seeAllId = "seeAllId"
    static var placeholder = String()
    static var businesses = [Businesses]()
    
    var searchBar = UITextField.searchBar(placeholder : "")
    
    let ListView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let searchImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        return imageView
    }()
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == searchBar {
            if textField.text! != "" {
                SeeAll.businesses = MockDataService.businesses.filter({ category -> Bool in
                    guard let searched = textField.text else { return false }
                    return (category.tags)!.contains(searched.lowercased())
                })
                if SeeAll.businesses.count == 0 {
                    SeeAll.businesses = MockDataService.businesses
                }
            } else {
                SeeAll.businesses = MockDataService.businesses
            }
            SeeAll.businesses.shuffle()
            ListView.reloadData()
        }
    }
    
    @objc func goToBusinessDetail(sender : UITapGestureRecognizer) {
        let indexLabel = sender.view?.subviews[1] as! UILabel
        BusinessDetail.business = SeeAll.businesses[Int((indexLabel.text)!)!]
        let businessDetail = BusinessDetail()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(businessDetail, animated: true)
    }
    
    private func setupView() {
        SeeAll.businesses.shuffle()
        searchBar = UITextField.searchBar(placeholder : SeeAll.placeholder)
        searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.backgroundColor = UIColor.white
        
        ListView.backgroundColor = UIColor.Background(alpha: 1.0)
        ListView.delegate = self
        ListView.dataSource = self
        ListView.contentInset = UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0)
        ListView.register(SeeAllCell.self, forCellWithReuseIdentifier: seeAllId)
        
        view.addSubview(searchBar)
        searchBar.addSubview(searchImage)
        view.addSubview(ListView)
    }
    private func setupConstraints() {
        searchBar.frame = CGRect(x: 10, y: navBarHeight+bottomSpacing!+10, width: view.frame.width-20, height: navBarHeight+10)
        searchImage.frame = CGRect(x: searchBar.frame.width-(searchBar.frame.height-20), y: searchBar.frame.height/2-((searchBar.frame.height-30)/2), width: searchBar.frame.height-30, height: searchBar.frame.height-30)
        ListView.frame = CGRect(x: 0, y: searchBar.frame.maxY, width: view.frame.width, height: view.frame.height-(searchBar.frame.height+navBarHeight+bottomSpacing!+10))
    }
    private func setupGestures() {}
    
    private func setupNavigationbar(){
        self.navigationController?.navigationBar.tintColor = UIColor.Main(alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setupNavigationbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SeeAll.businesses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: seeAllId, for: indexPath) as! SeeAllCell
        cell.backgroundColor = .white
        cell.business = SeeAll.businesses[indexPath.item]
        cell.indexLabel.text = "\(indexPath.item)"
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToBusinessDetail)))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    override func viewDidLoad() {
        setupView()
        setupConstraints()
        setupGestures()
        
    }
}
