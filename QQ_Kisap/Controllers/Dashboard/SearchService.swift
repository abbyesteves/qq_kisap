//
//  SearchService.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 22/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SearchService: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let serviceId = "serviceId"
    
    var services = MockDataService().services(hasIcon : false)
    
    let ServiceView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == Dashboard.searchBar {
            if textField.text! != "" {
                services = MockDataService().services(hasIcon : false).filter({ category -> Bool in
                    guard let searched = textField.text else { return false }
                    return (category.name)!.contains(searched.lowercased())
                })
                if services.count == 0 {
                    services = MockDataService().services(hasIcon : false)
                }
            } else {
                services = MockDataService().services(hasIcon : false)
            }
            ServiceView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: serviceId, for: indexPath) as! IconLabelCell
        Service().resetCell(cell: cell, indexPath: indexPath)
        let service = services[indexPath.item]
        cell.backgroundColor = UIColor.Bg(alpha: 1.0)
        cell.value = service
        let adImage : UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.kf.setImage(with: URL(string: "\((service.img_url)!)"))
            return imageView
        }()
        cell.addSubview(adImage)
        adImage.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 3 {
            return CGSize(width: ((frame.width/3)-8)*2, height: ((frame.width/3)-8)*2)
        } else if indexPath.item == 4 {
            return CGSize(width: (frame.width/3)-10, height: ((frame.width/3)-8)*2)
        }  else if indexPath.item == 8 {
            return CGSize(width: ((frame.width/3)-8)*2, height: ((frame.width/3)-8))
        }
        return CGSize(width: (frame.width/3)-10, height: (frame.width/3)-10)
    }
    
    private func setupView() {
        Dashboard.searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        ServiceView.backgroundColor = UIColor.clear
        ServiceView.delegate = self
        ServiceView.dataSource = self
        ServiceView.showsHorizontalScrollIndicator = false
        ServiceView.register(IconLabelCell.self, forCellWithReuseIdentifier: serviceId)
        
        addSubview(ServiceView)
    }
    
    private func setupConstraints() {
        ServiceView.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: frame.height)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
