//
//  SearchFood.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 19/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SearchFood: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    let foodId = "foodId"
    
    let FoodView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    var categories = MockDataService.categories
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == Dashboard.searchBar {
            if textField.text! != "" {
                categories = MockDataService.categories.filter({ category -> Bool in
                    guard let searched = textField.text else { return false }
                    return (category.name)!.contains(searched.lowercased())
                })
                if categories.count == 0 {
                    categories = MockDataService.categories
                }
            } else {
                categories = MockDataService.categories
            }
            FoodView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: foodId, for: indexPath)  as! CategoriesCell
        cell.backgroundColor = UIColor.clear
        cell.category = categories[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((frame.width/4)-5), height: ((frame.width/4)-5))
    }
    
    private func setupView() {
        Dashboard.searchBar.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        FoodView.backgroundColor = UIColor.clear
        FoodView.delegate = self
        FoodView.dataSource = self
        FoodView.showsHorizontalScrollIndicator = false
        FoodView.register(CategoriesCell.self, forCellWithReuseIdentifier: foodId)
        
        addSubview(FoodView)
    }
    
    private func setupConstraints() {
        FoodView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
