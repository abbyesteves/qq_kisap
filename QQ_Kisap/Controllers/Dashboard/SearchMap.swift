//
//  SearchMap.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 22/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchMap: BaseCell, CLLocationManagerDelegate {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    let additionals = ["access", "trades", "day", "week", "month"]
    var locationManager = CLLocationManager()
    var mapView = GMSMapView()
    
    let town = CouchService().town()
    
    let iconImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_marker")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        return imageView
    }()
    
    let locationText : UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.darkGray
        textView.font = UIFont(name: "SFProDisplay-Medium", size: 18)!
        textView.backgroundColor = .clear
        textView.text = "0158 B.S Aquino Ave. Tangos, Baliuag, Bulacan"
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.textAlignment = .justified
        return textView
    }()
    
    let additionalLabel = UILabel.titleLabel(text : "Additional".uppercased(), alignment : .center, font : UIFont(name: "SFProDisplay-Bold", size: 17)!, color : UIColor.gray)
    
    let additionalView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private func mapStyle(color : UIColor) {
        let streets = UIColor(hue: color.hueComponent, saturation: 0.20, brightness: 0.99, alpha: 1.0).toHexString()
        let land = UIColor(hue: color.hueComponent, saturation: 0.30, brightness: 0.99, alpha: 1.0).toHexString()
        let water = UIColor(hue: color.hueComponent, saturation: 0.35, brightness: 0.92, alpha: 1.0).toHexString()
        let mapStyle = "[" +
            "  {" +
            "    \"elementType\": \"labels.text.fill\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#ffffff\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"elementType\": \"labels.text.stroke\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi\"," +
            "    \"elementType\": \"all\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi\"," +
            "    \"elementType\": \"labels.text.fill\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#ffffff\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi.park\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#e5e5e5\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"transit\"," +
            "    \"elementType\": \"labels.icon\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"\"," +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"road\"," +
            "    \"elementType\": \"labels.icon\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"\"," +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"transit\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\(streets)\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"road\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\(streets)\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"landscape\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\((land))\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"water\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\((water))\"" +
            "      }" +
            "    ]" +
            "  }" +
        "]"
        
        do {
            // Set the map style by passing a valid JSON string.
            self.mapView.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
    }
    
    private func setupAdditionals() {
        let width = frame.width/CGFloat(additionals.count)
        let size = additionalView.frame.height-(20+20)
        var x = CGFloat(0)
        for additional in additionals{
            
            let iconView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.Background(alpha: 1.0)
                view.layer.cornerRadius = size/2
                return view
            }()
            
            let iconImage : UIImageView = {
                let imageView = UIImageView()
                imageView.image = UIImage(named: "ic_\(additional)".lowercased())?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = UIColor.Main(alpha: 1.0)
                return imageView
            }()
            
            let additionalLabel = UILabel.titleLabel(text : additional.uppercased(), alignment : .center, font : UIFont(name: "SFProDisplay-Regular", size: 15)!, color : UIColor.gray)
            
            additionalView.addSubview(iconView)
            iconView.addSubview(iconImage)
            additionalView.addSubview(additionalLabel)
            
            iconView.frame = CGRect(x: x+10, y: 0, width: size, height: size)
            iconImage.frame = CGRect(x: 12.5, y: 12.5, width: size-25, height: size-25)
            additionalLabel.frame = CGRect(x: x, y: iconView.frame.maxY+10, width: width, height: 20)
            x = x + additionalLabel.frame.width
        }
    }
    
    private func setupView() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        let camera = GMSCameraPosition.camera(withLatitude: 14.8370235, longitude: 120.8855897, zoom: 11.5)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        //set map style
        let color = Service().hexUIColor(hex: "#\((town.config.theme_color)!)")
        self.mapStyle(color : color)
        
        addSubview(mapView)
        addSubview(iconImage)
        addSubview(locationText)
        addSubview(additionalLabel)
        addSubview(additionalView)
    }
    
    private func setupConstraints() {
        mapView.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: frame.height/2)
        iconImage.frame = CGRect(x: 10, y: mapView.frame.maxY+25, width: 20, height: 20)
        locationText.frame = CGRect(x: iconImage.frame.maxX+5, y: mapView.frame.maxY+10, width: frame.width-(iconImage.frame.width+5+20), height: 100)
        additionalLabel.frame = CGRect(x: 10, y: frame.height-(100+20+20+10+bottomSpacing!), width: frame.width-20, height: 20)
        additionalView.frame = CGRect(x: 0, y: additionalLabel.frame.maxY+10, width: frame.width, height: 100)
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let userLocation = locations.last
//        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
//
//        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
//                                              longitude: userLocation!.coordinate.longitude, zoom: 13.0)
//        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        mapView.isMyLocationEnabled = true
//
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
//        marker.map = mapView
//        
//        print(" didUpdateLocations ", userLocation)
////        locationManager.stopUpdatingLocation()
//    }
    
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        print(" didChangeAuthorization ", status)
//    }
//
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print(" didFailWithError ", error)
//    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
        setupAdditionals()
    }
}
