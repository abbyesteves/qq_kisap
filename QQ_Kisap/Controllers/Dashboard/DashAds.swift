//
//  DashAds.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class DashAds: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    let adId = "adId"
    var currentIndex = 0
    var autoTimer : Timer?
    
    let ads = MockDataService.ads
    
    let AdsView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    // @objc
    
    @objc func autoScroll() {
        if currentIndex == ads.count-1 {
            currentIndex = 0
            let indexPath = NSIndexPath(item: currentIndex, section: 0)
            AdsView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
        } else {
            currentIndex = currentIndex + 1
            let indexPath = NSIndexPath(item: currentIndex, section: 0)
            AdsView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
        }
    }
    
    
    // private func
    
    private func setupView() {
        self.autoTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
        
        AdsView.backgroundColor = UIColor.Background(alpha: 1.0)
        AdsView.delegate = self
        AdsView.dataSource = self
        AdsView.isPagingEnabled = true
        AdsView.showsHorizontalScrollIndicator = false
        AdsView.register(DashAdsCell.self, forCellWithReuseIdentifier: adId)
        
        addSubview(AdsView)
    }
    
    private func setupConstraints() {
        AdsView.frame = self.bounds
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ads.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: adId, for: indexPath) as! DashAdsCell
        cell.backgroundColor = UIColor.Base(alpha: 1.0)
        cell.ad = ads[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let index = Int(AdsView.contentOffset.x/frame.width)
        self.currentIndex = index
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
