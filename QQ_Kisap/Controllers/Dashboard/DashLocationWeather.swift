//
//  DashLocationWeather.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class DashLocationWeather: BaseCell {
    
    private func setupView() {
        
    }
    
    private func setupConstraints() {
        
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
