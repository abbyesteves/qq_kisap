//
//  DashTopServices.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class DashTopServices: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let topServicesId = "topServicesId"
    
    var services = MockDataService().services(hasIcon : true)
    
    let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
    
    let titleLabel = UILabel.titleLabel(text : "Top Service", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 20)!, color : UIColor.darkGray)
    
    let seeAllButton = UIButton.allButton(fontColor : UIColor.Main(alpha: 1.0), text: "See All")
    
    let arrowImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_arrow_rectangle".lowercased())?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        return imageView
    }()
    
    let ServicesView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    //@objcs func
    
    // private, public funcs
    private func setupView() {
        services.shuffle()
        ServicesView.backgroundColor = UIColor.clear
        ServicesView.delegate = self
        ServicesView.dataSource = self
        ServicesView.showsHorizontalScrollIndicator = false
        ServicesView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        ServicesView.register(IconLabelCell.self, forCellWithReuseIdentifier: topServicesId)
        
        addSubview(border)
        addSubview(titleLabel)
        addSubview(seeAllButton)
        seeAllButton.addSubview(arrowImage)
        addSubview(ServicesView)
    }
    
    private func setupConstraints() {
        border.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 0.5)
        titleLabel.frame = CGRect(x: 10, y: border.frame.maxY+(15-border.frame.height), width: frame.width-120, height: 25)
        seeAllButton.frame = CGRect(x: titleLabel.frame.maxX, y: 15, width: 100, height: titleLabel.frame.height)
        arrowImage.frame = CGRect(x: seeAllButton.frame.width-20, y: seeAllButton.frame.height/2-10, width: 20, height: 20)
        ServicesView.frame = CGRect(x: 0, y: seeAllButton.frame.maxY+10, width: frame.width, height: frame.height-(titleLabel.frame.height+40))
    }
    
    private func setupGestures() {}
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topServicesId, for: indexPath) as! IconLabelCell
        cell.backgroundColor = UIColor.Background(alpha: 1.0)
        cell.value = services[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item == 3 {
            return CGSize(width: ((frame.width/3)-8)*2, height: ((frame.width/3)-8)*2)
        } else if indexPath.item == 4 {
            return CGSize(width: (frame.width/3)-10, height: ((frame.width/3)-8)*2)
        }  else if indexPath.item == 8 {
            return CGSize(width: ((frame.width/3)-8)*2, height: ((frame.width/3)-8))
        }
        return CGSize(width: (frame.width/3)-10, height: (frame.width/3)-10)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
        setupGestures()
    }
}
