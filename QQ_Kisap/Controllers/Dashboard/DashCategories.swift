//
//  DashCategories.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class DashCategories: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let categoriesId = "categoriesId"
    
    var categories = MockDataService.dashCategories
    
//    let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
//    
//    let titleLabel = UILabel.titleLabel(text : "Categories", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.darkGray)
//    
//    let seeAllButton = UIButton.allButton(fontColor : UIColor.Main(alpha: 1.0), text: "See All")
    
    let CategoriesView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    private func setupView() {
        categories.shuffle()
        CategoriesView.backgroundColor = UIColor.clear
        CategoriesView.delegate = self
        CategoriesView.dataSource = self
        CategoriesView.showsHorizontalScrollIndicator = false
        CategoriesView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        CategoriesView.register(CategoriesCell.self, forCellWithReuseIdentifier: categoriesId)
//
//        addSubview(border)
//        addSubview(titleLabel)
//        addSubview(seeAllButton)
        addSubview(CategoriesView)
    }
    
    private func setupConstraints() {
        CategoriesView.frame = self.bounds
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesId, for: indexPath) as! CategoriesCell
        cell.backgroundColor = UIColor.clear//Background(alpha: 1.0)
        cell.category = categories[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CategoriesView.frame.height, height: CategoriesView.frame.height)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
