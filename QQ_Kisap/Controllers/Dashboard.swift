//
//  Dashboard.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class Dashboard: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    var currentIndex = 0
    var autoTimer : Timer?
    var businesses = MockDataService.businesses
    
    let town = CouchService().town()
    let searchTabs = ["ic_food", "ic_service", "ic_location"]
    let storesId = "storesId"
    let categoriesId = "categoriesId"
    let mustTryId = "mustTryId"
    let topServicesId = "topServicesId"
    let locationWeatherId = "locationWeatherId"
    let dashId = "dashId"
    let socialId = "socialId"
    let adId = "adId"
    //
    let searchFoodId = "searchFoodId"
    let searchServiceId = "searchServiceId"
    let searchMapId = "searchMapId"
    
    let mapView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let mapImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_location")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 5))
        label.textAlignment = .left
        return label
    }()
    
    let profileImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_cityhall")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        return imageView
    }()
    
    let profileTabView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let searchTabView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let navigationBar = UIView.navigationBar(color : UIColor.Main(alpha: 1.0))
    
    let searchImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        return imageView
    }()
    
    static var searchBar = UITextField.searchBar(placeholder : "Best desserts near me...")
    
    let DashView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let SearchView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let StoresView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let pageIndicator : UIPageControl = {
        let control = UIPageControl()
        control.currentPage = 0
        control.currentPageIndicatorTintColor = UIColor.Main(alpha: 1.0)
        control.pageIndicatorTintColor = UIColor.white
        return control
    }()
    
    //@objc funcs
    @objc func search() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = UIColor.BgLight(alpha: 1.0)
            Dashboard.searchBar.layer.borderWidth = 1
            Dashboard.searchBar.layer.borderColor = UIColor.Base(alpha: 1.0).cgColor
            self.navigationBar.alpha = 0
            self.DashView.alpha = 0
            self.SearchView.alpha = 1
            self.searchTabView.alpha = 1
            Dashboard.searchBar.frame = CGRect(x: 10, y: self.navigationBar.frame.maxY, width: self.view.frame.width-20, height: self.navBarHeight+10)
            self.searchImage.frame = CGRect(x: Dashboard.searchBar.frame.width-(Dashboard.searchBar.frame.height-20), y: Dashboard.searchBar.frame.height/2-((Dashboard.searchBar.frame.height-30)/2), width: Dashboard.searchBar.frame.height-30, height: Dashboard.searchBar.frame.height-30)
        })
    }
    
    @objc func searchClose() {
        self.exitKeyboard()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = .white
            Dashboard.searchBar.layer.borderWidth = 0
            Dashboard.searchBar.text = ""
            self.navigationBar.alpha = 1
            self.DashView.alpha = 1
            self.SearchView.alpha = 0
            self.searchTabView.alpha = 0
            Dashboard.searchBar.frame = CGRect(x: 0, y: self.navigationBar.frame.maxY, width: self.view.frame.width, height: self.navBarHeight+10)
            self.searchImage.frame = CGRect(x: Dashboard.searchBar.frame.width-(Dashboard.searchBar.frame.height-20), y: Dashboard.searchBar.frame.height/2-((Dashboard.searchBar.frame.height-30)/2), width: Dashboard.searchBar.frame.height-30, height: Dashboard.searchBar.frame.height-30)
        })
    }
    
    @objc func exitKeyboard() {
        if let window = UIApplication.shared.keyWindow {
            window.endEditing(true)
        }
    }
    
    @objc func goToProfile(sender : UITapGestureRecognizer) {
        let profileDetail = ProfileDetail()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(profileDetail, animated: true)
    }
    
    @objc func goToBusinessDetail(sender : UITapGestureRecognizer) {
        let indexLabel = sender.view?.subviews[2] as! UILabel
        BusinessDetail.business = businesses[Int((indexLabel.text)!)!]
        let businessDetail = BusinessDetail()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(businessDetail, animated: true)
    }
    
    @objc func goToMaps() {
        let downloadTown = DownloadTown()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(downloadTown, animated: true)
    }
    
    @objc func tabSelect(selector: UITapGestureRecognizer) {
        let width = ((searchTabView.frame.width+1)/CGFloat(searchTabs.count))
        let view = selector.view
        let indexText = view?.subviews[1] as! UILabel
        let indicator = view?.subviews[2] as! UIView
        let indexPath = IndexPath(item: Int((indexText.text)!)!, section: 0)
        SearchView.scrollToItem(at: indexPath, at: [], animated: false)
        self.exitKeyboard()
        self.resetTabSelect()
        UIView.animate(withDuration: 0.3, animations: {
            indicator.frame = CGRect(x: (width/2)-(self.searchTabView.frame.height/2), y: 0, width: self.searchTabView.frame.height, height: self.searchTabView.frame.height)
        })
    }
    
    @objc func autoScroll() {
        if currentIndex == 3 { //businesses.count-1 
            currentIndex = 0
            let indexPath = NSIndexPath(item: currentIndex, section: 0)
            StoresView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
        } else {
            currentIndex = currentIndex + 1
            let indexPath = NSIndexPath(item: currentIndex, section: 0)
            StoresView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
        }
    }
    
    // private, public funcs
    private func resetTabSelect(){
        let width = ((searchTabView.frame.width+1)/CGFloat(searchTabs.count))
        for views in searchTabView.subviews {
            let indicator = views.subviews[2] as! UIView
            UIView.animate(withDuration: 0.3, animations: {
                indicator.frame = CGRect(x: width/2, y: self.searchTabView.frame.height/2, width: 0, height: 0)
            })
        }
    }
    
    private func setupView() {
        self.autoTimer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
        businesses.shuffle()
        Dashboard.searchBar = UITextField.searchBar(placeholder : "Best desserts near me...")
        nameLabel.text = "\((town.initials)!)".uppercased()
        
        pageIndicator.numberOfPages = 4//businesses.count
        Dashboard.searchBar.delegate = self
        Dashboard.searchBar.returnKeyType = .done
        //navigation
        navigationController?.navigationBar.tintColor = UIColor.white
        view.backgroundColor = .white
        // collection views
        DashView.backgroundColor = UIColor.clear
        DashView.delegate = self
        DashView.dataSource = self
        DashView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: dashId)//DashStores
        DashView.register(DashCategories.self, forCellWithReuseIdentifier: categoriesId)
        DashView.register(DashMustTries.self, forCellWithReuseIdentifier: mustTryId)
        DashView.register(DashTopServices.self, forCellWithReuseIdentifier: topServicesId)
        DashView.register(DashLocationWeather.self, forCellWithReuseIdentifier: locationWeatherId)
        DashView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: socialId)
        DashView.register(DashAds.self, forCellWithReuseIdentifier: adId)
        
        StoresView.backgroundColor = UIColor.Background(alpha: 1.0)
        StoresView.delegate = self
        StoresView.dataSource = self
        StoresView.isPagingEnabled = true
        StoresView.showsHorizontalScrollIndicator = false
        StoresView.register(DashBusinessCell.self, forCellWithReuseIdentifier: storesId)
        
        SearchView.backgroundColor = UIColor.clear
        SearchView.delegate = self
        SearchView.dataSource = self
        SearchView.isScrollEnabled = false
        SearchView.register(SearchFood.self, forCellWithReuseIdentifier: searchFoodId)
        SearchView.register(SearchService.self, forCellWithReuseIdentifier: searchServiceId)
        SearchView.register(SearchMap.self, forCellWithReuseIdentifier: searchMapId)
        SearchView.alpha = 0
        searchTabView.alpha = 0
        
        profileTabView.layer.cornerRadius = (navBarHeight)/2
        
        view.addSubview(Dashboard.searchBar)
        Dashboard.searchBar.addSubview(searchImage)
        view.addSubview(navigationBar)
        navigationBar.addSubview(profileTabView)
        profileTabView.addSubview(profileImage)
        navigationBar.addSubview(mapView)
        mapView.addSubview(mapImage)
        navigationBar.addSubview(nameLabel)
        view.addSubview(DashView)
        view.addSubview(searchTabView)
        view.addSubview(SearchView)
    }
    
    private func setupConstraints() {
        navigationBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 20+navBarHeight+30)
        profileTabView.frame = CGRect(x: navigationBar.frame.width-((navBarHeight)+20), y: 20+20, width: navBarHeight, height: navBarHeight)
        profileImage.frame = CGRect(x: profileTabView.frame.width/2-((profileTabView.frame.width-15)/2), y: profileTabView.frame.height/2-((profileTabView.frame.width-15)/2), width: profileTabView.frame.width-15, height: profileTabView.frame.width-15)
        mapView.frame = CGRect(x: 0, y: profileTabView.frame.minY, width: navBarHeight, height: navBarHeight)
        mapImage.frame = CGRect(x: mapView.frame.width/2-12.5, y: mapView.frame.height/2-12.5, width: 25, height: 25)
        nameLabel.frame = CGRect(x: mapView.frame.maxX+5, y: profileTabView.frame.minY, width: navigationBar.frame.width-(mapView.frame.width+profileTabView.frame.width+5), height: navBarHeight)
        
        Dashboard.searchBar.frame = CGRect(x: 0, y: navigationBar.frame.maxY, width: view.frame.width, height: navBarHeight+10)
        searchImage.frame = CGRect(x: Dashboard.searchBar.frame.width-(Dashboard.searchBar.frame.height-20), y: Dashboard.searchBar.frame.height/2-((Dashboard.searchBar.frame.height-30)/2), width: Dashboard.searchBar.frame.height-30, height: Dashboard.searchBar.frame.height-30)
        DashView.frame = CGRect(x: 0, y: Dashboard.searchBar.frame.maxY, width: view.frame.width, height: view.frame.height-(navigationBar.frame.height+Dashboard.searchBar.frame.height))
        searchTabView.frame = CGRect(x: 0, y: Dashboard.searchBar.frame.maxY+10, width: view.frame.width, height: 50)
        SearchView.frame = CGRect(x: 0, y: searchTabView.frame.maxY+30, width: view.frame.width, height: view.frame.height-(navigationBar.frame.height+Dashboard.searchBar.frame.height+searchTabView.frame.height+50))
    }
    
    private func setupGestures() {
        navigationBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitKeyboard)))
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClose)))
        navigationBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClose)))
        profileTabView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToProfile)))
        mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMaps)))
    }
    
    private func watch() {
    }
    
    private func setupSearchTabs() {
        let width = ((searchTabView.frame.width+1)/CGFloat(searchTabs.count))
        var x = CGFloat(0)
        for (index, tab) in searchTabs.enumerated() {
            let indexLabel : UILabel = {
                let label = UILabel()
                label.text = "\(index)"
                return label
            }()
            let indicator : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.Bg(alpha: 1.0)
                view.layer.cornerRadius = searchTabView.frame.height/2
                return view
            }()
            
            let iconImageView : UIImageView = {
                let imageView = UIImageView()
                imageView.image = UIImage(named: tab)?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = UIColor.Main(alpha: 1.0)
                return imageView
            }()
            let tabButton : UIButton = {
                let button = UIButton()
                button.backgroundColor = .clear
                button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 1))
                button.setTitleColor(UIColor.Main(alpha: 1.0), for: .normal)
                button.setTitle("", for: .normal)
                return button
            }()
            
            tabButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabSelect)))
            searchTabView.addSubview(tabButton)
            tabButton.addSubview(indexLabel)
            tabButton.addSubview(indicator)
            tabButton.addSubview(iconImageView)
            
            tabButton.frame = CGRect(x: x, y: 0, width: width, height: searchTabView.frame.height)
            let iconSize = (tabButton.frame.height)-30
            iconImageView.frame = CGRect(x: tabButton.frame.width/2-(iconSize/2), y: tabButton.frame.height/2-(iconSize/2), width: iconSize, height: iconSize)
            
            if index == 0 {
                indicator.frame = CGRect(x: (width/2)-(self.searchTabView.frame.height/2), y: 0, width: searchTabView.frame.height, height: searchTabView.frame.height)
            } else {
                indicator.frame = CGRect(x: (width/2), y: searchTabView.frame.height/2, width: 0, height: 0)
            }
            
            x = x + tabButton.frame.width + 1
        }
    }
    
    private func LocationWeather(cell: UICollectionViewCell) {
        cell.backgroundColor = UIColor.Main(alpha : 1.0)
        let weatherLabel = UILabel.titleLabel(text : "31º/33º", alignment : .center, font : UIFont(name: "SFProDisplay-Regular", size: 17)!, color : UIColor.white)
        let weatherImage : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_cloudy")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.white
            return imageView
        }()
        let iconImage : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_marker")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.white
            return imageView
        }()
        let label = UILabel.titleLabel(text : "Baliuag, Bulacan", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 20)!, color : UIColor.white)
        let locationText : UITextView = {
            let textView = UITextView()
            textView.textColor = UIColor.white
            textView.font = UIFont(name: "SFProDisplay-Regular", size: 15)
            textView.backgroundColor = .clear
            textView.text = "Mostly, Cloudy\nChance of rain : 10%"
            textView.isEditable = false
            textView.isScrollEnabled = false
            return textView
        }()
        
        cell.addSubview(iconImage)
        cell.addSubview(label)
        cell.addSubview(locationText)
        cell.addSubview(weatherImage)
        cell.addSubview(weatherLabel)
        
        iconImage.frame = CGRect(x: 10, y: 20, width: 20, height: 20)
        label.frame = CGRect(x: iconImage.frame.maxX+5, y: iconImage.frame.minY, width: cell.frame.width/2, height: 20)
        locationText.frame = CGRect(x: iconImage.frame.maxX, y: label.frame.maxY+5, width: label.frame.width, height: cell.frame.height-(label.frame.height+45))
        weatherImage.frame = CGRect(x: label.frame.maxX+(cell.frame.width/4-((cell.frame.height-(25+40))/2)), y: 20, width: cell.frame.height-(25+40), height: cell.frame.height-(25+40))
        weatherLabel.frame = CGRect(x: label.frame.maxX, y: weatherImage.frame.maxY, width: cell.frame.width/2, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == SearchView {
            return 3
        }
        if collectionView == StoresView {
            return 4//businesses.count
        }
        return 7
    }
    
    @objc func goToSeeAllTries(sender : UITapGestureRecognizer) {
        //assigning businesses to all list
        SeeAll.businesses = businesses
        SeeAll.placeholder = "Any cravings?"
        let seeAll = SeeAll()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(seeAll, animated: true)
    }
    
    @objc func goToSeeAllService(sender : UITapGestureRecognizer) {
        //assigning businesses to all list
        SeeAll.businesses = businesses
        SeeAll.placeholder = "What do you wanna see?"
        let seeAll = SeeAll()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(seeAll, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //search collection
        if collectionView == SearchView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: searchMapId, for: indexPath) as! SearchMap
            cell.backgroundColor = .clear
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: searchFoodId, for: indexPath) as! SearchFood
                return cell
            } else if indexPath.item == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: searchServiceId, for: indexPath) as! SearchService
                return cell
            }
            return cell
        }
        // store collection
        if collectionView == StoresView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storesId, for: indexPath) as! DashBusinessCell
            cell.backgroundColor = UIColor.Background(alpha: 1.0)
            cell.business = businesses[indexPath.item]
            cell.indexLabel.text = "\(indexPath.item)"
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToBusinessDetail)))
            return cell
        }
        // main view
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dashId, for: indexPath) //as! DashStores
            cell.backgroundColor = UIColor.clear
            cell.addSubview(StoresView)
            cell.addSubview(pageIndicator)
            StoresView.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height)
            pageIndicator.frame = CGRect(x: 0, y: StoresView.frame.height-20, width: StoresView.frame.width, height: 20)
            return cell
            
        } else if indexPath.item == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesId, for: indexPath) as! DashCategories
            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if indexPath.item == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mustTryId, for: indexPath) as! DashMustTries
            
            cell.seeAllButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSeeAllTries)))
            cell.backgroundColor = UIColor.clear
            
            return cell
            
        } else if indexPath.item == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topServicesId, for: indexPath) as! DashTopServices
            cell.seeAllButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSeeAllService)))
            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if indexPath.item == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: locationWeatherId, for: indexPath) as! DashLocationWeather
            self.LocationWeather(cell: cell)
            return cell
            
        }  else if indexPath.item == 6 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: adId, for: indexPath) as! DashAds
            cell.backgroundColor = UIColor.clear
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dashId, for: indexPath)
        cell.backgroundColor = UIColor.white
        
        Service().resetCell(cell: cell, indexPath: indexPath)
        
        let handlerLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.Main(alpha: 1.0)
            label.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 5))
            label.textAlignment = .center
            label.text = "@\((town.name)!)\((town.address.province)!)_ph"
            return label
        }()
        
        cell.addSubview(handlerLabel)
        handlerLabel.frame = cell.bounds
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == SearchView {
            return CGSize(width: SearchView.frame.width, height: SearchView.frame.height)
        }
        if collectionView == StoresView {
            return CGSize(width: StoresView.frame.width, height: StoresView.frame.height)
        }
        if indexPath.item == 0 {
            return CGSize(width: view.frame.width, height: view.frame.height/5)
            
        } else if indexPath.item == 1 {
            return CGSize(width: view.frame.width, height: 100)
            
        } else if indexPath.item == 2 {
            return CGSize(width: view.frame.width, height: (view.frame.height/3))
            
        } else if indexPath.item == 3 {
            let percell = (view.frame.width/3)-10
            let services = CGFloat((10/3)+0.5)
            let titlelabel = 25
            let padding = 10+20+20+10+10
            return CGSize(width: view.frame.width, height: (percell*round(services))+CGFloat(titlelabel+padding+Int(percell)))
            
        } else if indexPath.item == 4 {
            return CGSize(width: view.frame.width, height: 120)
            
        } else if indexPath.item == 5 {
            return CGSize(width: view.frame.width, height: 30)
            
        }
        return CGSize(width: view.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == StoresView {
            let index = Int(StoresView.contentOffset.x/StoresView.frame.width)
            currentIndex = index
            pageIndicator.currentPage = index
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == Dashboard.searchBar {
            self.search()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == Dashboard.searchBar {
            Dashboard.searchBar.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        setupView()
        setupConstraints()
        setupSearchTabs()
        setupGestures()
        watch()
    }
}
