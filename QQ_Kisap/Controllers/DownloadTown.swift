//
//  DownloadTown.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 18/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class DownloadTown: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    let townsId = "townsId"
    var locationManager = CLLocationManager()
    var mapView = GMSMapView()
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    let saved = Service().savedId()
    let townSaved = CouchService().town()
    var towns = MockDataService.towns
    var currentTown = Service().Town()
    var currentY = CGFloat(0)
    var alpha = CGFloat(0)
    
    let LocationsView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let pinSearchImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_marker")?.withRenderingMode(.alwaysTemplate)
        return imageView
    }()
    
    let searchImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        return imageView
    }()
    
    let searchBar : UITextField = {
        let text = UITextField()
        text.layer.borderWidth = 1
        text.layer.borderColor = UIColor.Base(alpha: 1.0).cgColor
        text.backgroundColor = UIColor.white
        text.font = .systemFont(ofSize: 13)
        var placeholderMutable = NSMutableAttributedString()
        placeholderMutable = NSMutableAttributedString(attributedString: NSAttributedString(string: "Let's explore!", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.Base(alpha: 1.0)]))
        text.leftPadding(space: 50)
        text.attributedPlaceholder = placeholderMutable
        text.layer.shadowColor = UIColor.lightGray.cgColor
        text.layer.shadowOpacity = 0.5
        text.layer.shadowOffset = CGSize(width: 2, height: 2)
        text.layer.shadowRadius = 2
        return text
    }()
    
    let slidingHandle : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 3.5
        view.alpha = 0.5
        view.layer.shadowColor = UIColor.white.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: -2)
        view.layer.shadowRadius = 2
        return view
    }()
    
    let slidingView : UIView = {
        let view = UIView()
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: -2)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor.BgLight(alpha: 0.0)
        return view
    }()
    
    
    
    // @objc funcs
    
    @objc func minimize(recognizer: UITapGestureRecognizer) {
        let view = recognizer.view
        let color = Service().hexUIColor(hex: "#\((currentTown.config.theme_color)!)")
        let bgUIColor = UIColor(hue: color.hueComponent, saturation: 0.06, brightness: 1.0, alpha: self.alpha)
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.slidingView.backgroundColor = bgUIColor
            view?.frame = CGRect(x: (view?.frame.minX)!, y: (self.view.frame.height/2)+150, width: (view?.frame.width)!, height: (view?.frame.height)!)
        })
    }
    
    @objc func drag(recognizer: UIPanGestureRecognizer) {
        guard recognizer.view != nil else { return }
        let view = recognizer.view
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: self.view)
            let y = (view?.center.y)! + translation.y
            view?.center = CGPoint(x: (view?.center.x)!, y: y)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
            
            if (view?.frame.minY)! < (self.view.frame.height/2)-150 {//&& (view?.frame.minY)! >= searchBar.frame.maxY
                if self.currentY > (view?.frame.minY)! {
                    self.alpha = self.alpha + 0.02
                } else {
                    self.alpha = self.alpha - 0.02
                }
                
                UIView.animate(withDuration: 0.3, animations: {
                    let color = Service().hexUIColor(hex: "#\((self.currentTown.config.theme_color)!)")
                    let bgUIColor = UIColor(hue: color.hueComponent, saturation: 0.06, brightness: 1.0, alpha: self.alpha)
                    self.slidingView.backgroundColor = bgUIColor
                })
            }
            self.currentY = (view?.frame.minY)!
        } else {
            if (view?.frame.minY)! >= (self.view.frame.height/2)+(self.view.frame.height/4) {
                self.alpha = 0.0
                UIView.animate(withDuration: 0.3, animations: {
                    let color = Service().hexUIColor(hex: "#\((self.currentTown.config.theme_color)!)")
                    let bgUIColor = UIColor(hue: color.hueComponent, saturation: 0.06, brightness: 1.0, alpha: self.alpha)
                    self.slidingView.backgroundColor = bgUIColor
                    view?.frame = CGRect(x: (view?.frame.minX)!, y: (self.view.frame.height/2)+150, width: (view?.frame.width)!, height: (view?.frame.height)!)
                })
            } else if (view?.frame.minY)! < (self.view.frame.height/2) {
                self.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: {
                    let color = Service().hexUIColor(hex: "#\((self.currentTown.config.theme_color)!)")
                    let bgUIColor = UIColor(hue: color.hueComponent, saturation: 0.06, brightness: 1.0, alpha: self.alpha)
                    self.slidingView.backgroundColor = bgUIColor
                    view?.frame = CGRect(x: (view?.frame.minX)!, y: self.searchBar.frame.maxY, width: (view?.frame.width)!, height: (view?.frame.height)!)
                })
            }
        }
    }
    
    @objc func goToSetup(sender : UITapGestureRecognizer) {
        let indexLabel = sender.view?.subviews[0] as! UILabel
        let indexPath = IndexPath(item: Int((indexLabel.text)!)!, section: 0)
        self.select(indexPath : indexPath, saved : true)
        //goto
        let setup = Setup()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(setup, animated: true)
    }
    
    @objc func goToHome(sender : UITapGestureRecognizer) {
        let indexLabel = sender.view?.subviews[0] as! UILabel
        let indexPath = IndexPath(item: Int((indexLabel.text)!)!, section: 0)
        self.select(indexPath : indexPath, saved : true)
        //goto
        let dashboard = Dashboard()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(dashboard, animated: true)
    }
    
    //public, private funcs
    
    private func setTown() {
        for town in towns {
            if town.didSelect {
                currentTown = town
            }
        }
    }
    
    private func applyTheme() {
        let color = Service().hexUIColor(hex: "#\((currentTown.config.theme_color)!)")
        let bgUIColor = UIColor(hue: color.hueComponent, saturation: 0.06, brightness: 1.0, alpha: self.alpha)
        let baseUIColor = UIColor(hue: color.hueComponent, saturation: 0.23, brightness: 0.99, alpha: 1.0)
        
        pinSearchImage.tintColor = color
        searchImage.tintColor = color
        searchBar.textColor = color
        searchBar.layer.borderColor = baseUIColor.cgColor
        var placeholderMutable = NSMutableAttributedString()
        placeholderMutable = NSMutableAttributedString(attributedString: NSAttributedString(string: "Let's explore!", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: baseUIColor]))
        searchBar.attributedPlaceholder = placeholderMutable
        self.slidingView.backgroundColor = bgUIColor
        
        //add pins to map
        self.mapView.clear()
        self.addPinsMap()
        //set map style
        self.mapStyle(color : color)
    }
    
    private func addPinsMap() {
        for town in towns {
            let Marker = GMSMarker()
            let lat = (town.address.lat)!
            let long = (town.address.long)!
            
            let icMarker = UIImage(named: "ic_marker")!.withRenderingMode(.alwaysTemplate)
            let markerView = UIImageView(image: icMarker)
            markerView.tintColor = Service().hexUIColor(hex: "#\((currentTown.config.theme_color)!)")
            
            Marker.iconView = markerView
            Marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: lat)!, longitude: CLLocationDegrees(exactly: long)!)
            Marker.title = (town.name)!
            Marker.snippet = (town.address.province)!
            Marker.map = mapView
            
            if town.didSelect {
                let camera = GMSCameraPosition.camera(withLatitude: Marker.position.latitude, longitude: Marker.position.longitude, zoom: 11.5)
                self.mapView.animate(to: camera)
                self.mapView.selectedMarker = Marker
                markerView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            } else {
                markerView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            }
        }
    }
    
    private func mapStyle(color : UIColor) {
        let land = UIColor(hue: color.hueComponent, saturation: 0.08, brightness: 1.0, alpha: 1.0).toHexString()
        let water = UIColor(hue: color.hueComponent, saturation: 0.15, brightness: 0.98, alpha: 1.0).toHexString()
        let mapStyle = "[" +
            "  {" +
            "    \"elementType\": \"labels.text.fill\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#c0c0c0\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"elementType\": \"labels.text.stroke\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi\"," +
            "    \"elementType\": \"all\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi\"," +
            "    \"elementType\": \"labels.text.fill\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#757575\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"poi.park\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#e5e5e5\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"transit\"," +
            "    \"elementType\": \"labels.icon\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"\"," +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"road\"," +
            "    \"elementType\": \"labels.icon\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"\"," +
            "        \"visibility\": \"off\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"transit\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#ffffff\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"road\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#ffffff\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"landscape\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\((land))\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  {" +
            "    \"featureType\": \"water\"," +
            "    \"elementType\": \"geometry\"," +
            "    \"stylers\": [" +
            "      {" +
            "        \"color\": \"#\((water))\"" +
            "      }" +
            "    ]" +
            "  }" +
        "]"
        
        do {
            // Set the map style by passing a valid JSON string.
            self.mapView.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
    }
    
    private func setInitialTown(){
        if townSaved.town_id != nil && townSaved.town_id != "" {
            currentTown = townSaved
        } else {
            currentTown = towns[0]
        }
    }
    
    private func setupView() {
        //setting up color
        self.setTown()
        self.setInitialTown()
        let camera = GMSCameraPosition.camera(withLatitude: 14.8370235, longitude: 120.8855897, zoom: 11.5)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.applyTheme()
        
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        searchBar.layer.cornerRadius = navBarHeight/2
        
        LocationsView.backgroundColor = UIColor.clear
        LocationsView.delegate = self
        LocationsView.dataSource = self
        LocationsView.register(TownListCell.self, forCellWithReuseIdentifier: townsId)
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        view.addSubview(self.mapView)
        view.addSubview(searchBar)
        searchBar.addSubview(pinSearchImage)
        searchBar.addSubview(searchImage)
        view.addSubview(slidingView)
        slidingView.addSubview(slidingHandle)
        slidingView.addSubview(LocationsView)
    }
    
    
    private func setupConstraints() {
        mapView.frame = self.view.bounds
        searchBar.frame = CGRect(x: 10, y: navbarStatusHeight+10, width: view.frame.width-20, height: navBarHeight)
        pinSearchImage.frame = CGRect(x: 20, y: 10, width: searchBar.frame.height-20, height: searchBar.frame.height-20)
        searchImage.frame = CGRect(x: searchBar.frame.width-(searchBar.frame.height), y: 10, width: searchBar.frame.height-20, height: searchBar.frame.height-20)
        
        slidingView.frame = CGRect(x: 0, y: (view.frame.height/2)+150, width: view.frame.width, height: view.frame.height*2)
        slidingHandle.frame = CGRect(x: view.frame.width/2-(20), y: 10, width: 40, height: 7)
        LocationsView.frame = CGRect(x: 0, y: slidingHandle.frame.maxY+10, width: slidingView.frame.width, height: view.frame.height)
    }
    
    private func setupGestures() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(drag(recognizer: )))
        panGesture.delegate = self
        slidingView.addGestureRecognizer(panGesture)
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(minimize))
        doubleTap.numberOfTapsRequired = 2
        slidingView.addGestureRecognizer(doubleTap)
    }
    
    private func watch() {
    }
    
    private func select(indexPath : IndexPath, saved : Bool) {
        let town = towns[indexPath.item]
        if saved {
            CouchService().townSelected(town : town)
        }
        for (index, _) in towns.enumerated() {
            if indexPath.item != index {
                towns[index].didSelect = false
            }
        }
        
        if towns[indexPath.item].didSelect {
            towns[indexPath.item].didSelect = false
        } else {
            towns[indexPath.item].didSelect = true
        }
        LocationsView.reloadData()
        self.setTown()
        self.applyTheme()
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let userLocation = locations.last
//        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
//
//        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
//                                              longitude: userLocation!.coordinate.longitude, zoom: 13.0)
//        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        mapView.isMyLocationEnabled = true
//
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
//        marker.map = mapView
//
//        locationManager.stopUpdatingLocation()
//    }
    
    private func layout(cell: TownListCell, indexPath: IndexPath) {
        let town = towns[indexPath.item]
        let color = Service().hexUIColor(hex: "#\((currentTown.config.theme_color)!)")
        if town.didSelect {
            cell.nameLabel.textColor = .white
            cell.rangeLabel.textColor = .white
            cell.ratingLabel.textColor = .white
            cell.arrowImageView.tintColor = .white
            cell.backgroundColor = color
        } else {
            cell.nameLabel.textColor = color
            cell.rangeLabel.textColor = color
            cell.ratingLabel.textColor = color
            cell.arrowImageView.tintColor = color
            cell.backgroundColor = UIColor.white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return towns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: townsId, for: indexPath) as! TownListCell
        let town = towns[indexPath.item]
        cell.town = town
        cell.indexLabel.text = "\(indexPath.item)"
        if saved.contains("\((town.town_id)!)") {
            cell.arrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToHome)))
        } else {
            cell.arrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSetup)))
        }
        layout(cell: cell, indexPath : indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: LocationsView.frame.width-40, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.select(indexPath : indexPath, saved : false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        setupView()
        setupConstraints()
        setupGestures()
        watch()
    }
}
