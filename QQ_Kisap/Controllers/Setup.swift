//
//  Setup.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 22/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import BubblePicker

class Setup: UIViewController, BubblePickerDelegate {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    var tags = Service().tags()
    var selectedCount = Int(0)
    var progressCount = 0.0
    var autoTimer : Timer?
    
    var selectedItems = [String]()
    var items = ["Food", "Shopping", "Fashion", "Gadgets", "Art", "Travel", "Night Life", "Photography", "Life Style", "Nutrition"]
    let bubblePicker = BubblePicker()
    
    let progressView : UIProgressView = {
        let progress = UIProgressView()
        progress.progressViewStyle = .bar
        progress.setProgress(0.0, animated: true)
        progress.trackTintColor = UIColor.Background(alpha: 1.0)
        progress.tintColor = UIColor.Main(alpha: 1.0)
        return progress
    }()
    
    let stepsLabel : UILabel = {
        let label = UILabel()
        label.text = "We're downloading your town"
        label.font = UIFont(name: "SFProDisplay-Medium", size: UIFont.systemFontSize)!
        label.font = label.font.withSize(20)
        label.textAlignment = .center
        label.textColor = UIColor.Main(alpha: 1.0)
        return label
    }()
    
    let nextView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Bg(alpha: 1.0)
        view.layer.cornerRadius = 25
        return view
    }()
    
    let nextImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        imageView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi/2))
        return imageView
    }()
    
    let stepsSubLabel : UITextView = {
        let label = UITextView()
        label.text = "In The mean time, Choose atleast five categories that represent what you want to see"
        label.font = UIFont(name: "SFProDisplay-Light", size: UIFont.systemFontSize)!
        label.font = .systemFont(ofSize: 15)
        label.textAlignment = .center
        label.textColor = UIColor.Base(alpha: 1.0)
        label.isEditable = false
        label.isSelectable = false
        return label
    }()
    
    //@objc funcs
    @objc func goToStartUp() {
        CouchService().savePreference(tags : selectedItems)
        let introduction = Introduction()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        self.navigationController?.pushViewController(introduction, animated: true)
    }
    
    @objc func progressMock() {
        progressCount = progressCount + 0.5
        
        if progressCount <= 1.0 {
            progressView.setProgress(Float(progressCount), animated: true)
        }
        
    }
    
    //public, private funcs
    
    private func setupNavigationbar(){
        self.navigationController?.navigationBar.tintColor = UIColor.Main(alpha: 1.0)
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "tell us about you".uppercased()
            label.font = UIFont(name: "SFProDisplay-Heavy", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.textColor = UIColor.Main(alpha: 1.0)
            return label
        }()
        
        self.navigationItem.titleView = titleLabel
    }
    private func setupView(){
        self.autoTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.progressMock), userInfo: nil, repeats: true)
        view.backgroundColor = .white
        
        bubblePicker.delegate = self
//        bubblePicker.backgroundColor = .gray
        bubblePicker.reloadData()
        
        view.addSubview(progressView)
        view.addSubview(stepsLabel)
        view.addSubview(stepsSubLabel)
        view.addSubview(bubblePicker)
        view.addSubview(nextView)
        nextView.addSubview(nextImage)
    }
    private func setupConstraints(){
        progressView.frame = CGRect(x: 0, y: navBarHeight+bottomSpacing!+10, width: view.frame.width, height: 50)
        stepsLabel.frame = CGRect(x: 20, y: navBarHeight+bottomSpacing!+20, width: view.frame.width-40, height: 50)
        stepsSubLabel.frame = CGRect(x: 20, y: stepsLabel.frame.maxY, width: view.frame.width-40, height: 100)
        bubblePicker.frame = CGRect(x: 0, y: stepsSubLabel.frame.maxY, width: view.frame.width, height: view.frame.height-(stepsLabel.frame.height+stepsSubLabel.frame.height+navbarStatusHeight+navBarHeight+40))
        nextView.frame = CGRect(x: view.frame.width-45, y: view.frame.height-45, width: 0, height: 0)
        nextImage.frame = CGRect(x: nextView.frame.width/2, y: nextView.frame.height/2, width: 0, height: 0)
    }
    
    private func checkTags() {
        var selected = [Int]() //[0, 2, 3, 4]
        self.selectedItems = tags
        if tags.count > 0 {
            self.selectedCount = self.selectedItems.count
        }
        for tag in tags {
            if items.contains(tag) {
                let index = self.selectedItems.firstIndex(of: tag)
                if !selected.contains(index!) {
                    selected.append(index!)
                }
            }
        }
        
        if selectedCount >= 5 {
            stepsLabel.text = "Awesome! You're all set"
            stepsSubLabel.text = "You may update your preferences"
            UIView.animate(withDuration: 0.3, animations: {
                self.nextView.frame = CGRect(x: self.view.frame.width-70, y: self.view.frame.height-70, width: 50, height: 50)
                self.nextImage.frame = CGRect(x: self.nextView.frame.width/2-11, y: self.nextView.frame.height/2-11, width: 22, height: 22)
            })
        } else if selectedCount != 0 {
            stepsLabel.text = "Selected \(selectedCount)/5"
        }
        bubblePicker.setSelected(selected);
    }
    
    private func setupGestures(){
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToStartUp)))
    }
    
    private func watch(){}
    
    func numberOfItems(in bubblepicker: BubblePicker) -> Int {
        return items.count
    }
    
    func bubblePicker(_: BubblePicker, nodeFor indexPath: IndexPath) -> BubblePickerNode {
        let item = items[indexPath.item]
        let image = UIImage(named: item.replacingOccurrences(of: " ", with: "").lowercased())
        let node = BubblePickerNode(title: items[indexPath.item], color: UIColor.Bg(alpha: 1.0), image: image!)
        node.font = UIFont(name: "SFProDisplay-Regular", size: UIFont.systemFontSize)!
        return node
    }
    
    func bubblePicker(_: BubblePicker, didSelectNodeAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        selectedCount = selectedCount + 1
//        let food = ["Snack", "Restaurant", "Fastfood", "Café"]
//        if item.lowercased() == "food" {
//            items.insert(contentsOf: food, at: indexPath.item)
//            bubblePicker.reloadData()
//        }
        selectedItems.append(item)
        if selectedCount >= 5 {
            stepsLabel.text = "Awesome! You're all set"
            UIView.animate(withDuration: 0.3, animations: {
                self.nextView.frame = CGRect(x: self.view.frame.width-70, y: self.view.frame.height-70, width: 50, height: 50)
                self.nextImage.frame = CGRect(x: self.nextView.frame.width/2-11, y: self.nextView.frame.height/2-11, width: 22, height: 22)
            })
        } else if selectedCount != 0 {
            stepsLabel.text = "Selected \(selectedCount)/5"
        }
    }
    
    func bubblePicker(_: BubblePicker, didDeselectNodeAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        selectedCount = selectedCount - 1
        let index = selectedItems.firstIndex(of: item)
        if index != nil {
            selectedItems.remove(at: index!)
        }
        if selectedCount < 5 && selectedCount > 0 {
            stepsLabel.text = "Selected \(selectedCount)/5"
            UIView.animate(withDuration: 0.3, animations: {
                self.nextView.frame = CGRect(x: self.view.frame.width-45, y: self.view.frame.height-45, width: 0, height: 0)
                self.nextImage.frame = CGRect(x: self.nextView.frame.width/2, y: self.nextView.frame.height/2, width: 0, height: 0)
            })
        } else if selectedCount == 0 {
            stepsLabel.text = "We're downloading your town"
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        setupNavigationbar()
        setupView()
        setupConstraints()
        setupGestures()
        watch()
        checkTags()
    }
}
