//
//  Models.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
// business comp
struct Businesses : Decodable {
    var _id : String!
    var name : String!
    var branches : Int!
    var img_url : String!
    var doc_type: String!
    var type: String!
    var category: String!
    var tags: [String]!
    var address : Addresses!
    var rating : Double!
    var short_desc: String!
    var description: String!
    var owner: [String]!
    var config: Configurations!
    var subscription_details : Subscriptions!
    var operating_days : [Operations]!
    var feedback : [Feedbacks]!
    var catalog : [Catalog]!
}

struct Catalog: Decodable {
    var name: String!
    var img_url: String!
}

struct SubmittedPhotos: Decodable {
    var user_id: String!
    var data: String!
    var timestamp: String!
}

struct Feedbacks: Decodable {
    var name : String!
    var user_id : String!
    var review : String!
    var rating : Double!
    var photos_uploads : [String]!
    var time_stamp : String!
}

struct Operations : Decodable {
    var day: String!
    var is_open: String!
    var opening: String!
    var closing: String!
}
struct Subscriptions : Decodable {
    var type: String!
    var start: String!
    var end: String!
}

struct Configurations : Decodable {
    var logo: String!
    var theme_color: String!
    var gallery: [String]!
}

struct Addresses : Decodable {
    var lat : Double!
    var long : Double!
    var province : String!
    var municipal_address : String!
    var region : String!
}


struct Towns : Decodable {
    var town_id : String!
    var doc_type: String!
    var name: String!
    var initials: String!
    var short_desc: String!
    var tagline:String!
    var address: Addresses!
    var rating:Double!
    var config: Configurations!
    var last_updated: String!
    var town_version: String!
    var officials: Officials!
    var businesses : [String]!
    var baranggay : [Baranggays]!
    var forms: [Forms]!
    var gov_services: Services!
    var news : [Bulletins]!
    var events : [Bulletins]!
    var didSelect : Bool!
}

struct Abouts : Decodable {
    var desc : String!
    var img_url : String!
}

struct Bulletins : Decodable {
}


struct Services : Decodable {
    var categories : [String]!
    var police : [ServiceObjects]!
    var fire : [ServiceObjects]!
    var hospital : [ServiceObjects]!
    var risk_management : [ServiceObjects]!
}

struct ServiceObjects : Decodable{
    var name : String!
    var contact: Double!
    var lat : Double!
    var long : Double!
}

struct Forms: Decodable {
    var name : String!
    var data : String!
}

struct Baranggays: Decodable {
    var name : String!
    var captain : String!
    var lat : Double!
    var long : Double!
}

struct Officials: Decodable {
    var mayor : Mayors!
    var vice_mayor : Mayors!
    var counselors :[String]!
    var term : String!
}

struct Mayors: Decodable {
    var name : String!
    var short_desc : String!
    var summary : String!
    var teams : [Teams]!
    var gallery : [String]!
    var social_media : [String]!
    var mayors_voice : [String]!
    var news :[String]!//"boc_news_0001","boc_news_0002"
    var videos :[String]!
    var rating : Double!
}

struct Teams : Decodable {
    var name : String!
    var position : String!
    var img : String!
}

struct Categories : Decodable {
    var name : String!
    var icon : String!
}

struct ThumbnailCells : Decodable {
    var name : String!
    var icon : String!
    var img_url : String!
}

struct Ads : Decodable {
    var name : String!
    var icon : String!
}
