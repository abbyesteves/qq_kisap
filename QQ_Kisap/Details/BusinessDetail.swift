//
//  BusinessDetail.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 16/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Kingfisher

class BusinessDetail: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    var currentIndex = Int(0)
    static var business = Businesses(
        _id : "",
        name : "",
        branches : 0,
        img_url : "",
        doc_type : "business",
        type : "",
        category : "",
        tags : [""],
        address : Addresses(),
        rating : 0,
        short_desc : "",
        description : "",
        owner : [""],
        config : Configurations(
            logo: "",
            theme_color: "",
            gallery: [""]
        ),
        subscription_details : Subscriptions(),
        operating_days : [Operations()],
        feedback : [Feedbacks()],
        catalog : [Catalog()]
    )
    
    let tabs = ["overview", "menu", "32 reviews"]
    
    let galleryId = "galleryId"
    let moreId = "moreId"
    let mainId = "mainId"
    let overViewId = "overViewId"
    let menuId = "menuId"
    let reviewId = "reviewId"
    let alsoTryId = "alsoTryId"
    
    let indicator : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Main(alpha: 1.0)
        return view
    }()
    
    let nextView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.alpha = 0.5
        return view
    }()
    
    let rightImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi+(CGFloat.pi/2))
        imageView.layer.shadowColor = UIColor.darkGray.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    let previousView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.alpha = 0.5
        return view
    }()
    
    let leftImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        imageView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi+(CGFloat.pi/2)))
        imageView.layer.shadowColor = UIColor.darkGray.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    let headerMainView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let GalleryView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let MainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let MoreView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    private func setupView() {
        
        view.backgroundColor = UIColor.white
        
        MainView.backgroundColor = UIColor.clear
        MainView.delegate = self
        MainView.dataSource = self
        MainView.register(DashMustTries.self, forCellWithReuseIdentifier: alsoTryId)
        MainView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: mainId)
        
        GalleryView.backgroundColor = UIColor.clear
        GalleryView.delegate = self
        GalleryView.dataSource = self
        GalleryView.isPagingEnabled = true
        GalleryView.showsHorizontalScrollIndicator = false
        GalleryView.register(DashAdsCell.self, forCellWithReuseIdentifier: galleryId)
        
        MoreView.backgroundColor = UIColor.clear
        MoreView.delegate = self
        MoreView.dataSource = self
        MoreView.isPagingEnabled = true
        MoreView.isScrollEnabled = false
        MoreView.register(BusinessOverview.self, forCellWithReuseIdentifier: overViewId)
        MoreView.register(BusinessMenu.self, forCellWithReuseIdentifier: menuId)
        MoreView.register(BusinessReview.self, forCellWithReuseIdentifier: reviewId)
        MoreView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: moreId)
        
        view.addSubview(GalleryView)
        view.addSubview(MainView)
    }
    
    //@objc funcs
    @objc func tabSelect(selector : UITapGestureRecognizer) {
        let view = selector.view
        let indexText = view?.subviews[1] as! UILabel
        let indexPath = IndexPath(item: Int((indexText.text)!)!, section: 0)
        MoreView.scrollToItem(at: indexPath, at: [], animated: false)
        
//        if indexPath.item == 0 {
//            MoreView.frame = CGRect(x: 0, y: 0, width: MoreView.frame.width, height: 800)
//        } else if indexPath.item == 1 {
//            MoreView.frame = CGRect(x: 0, y: 0, width: MoreView.frame.width, height: 100)
//        } else if indexPath.item == 2 {
//            MoreView.frame = CGRect(x: 0, y: 0, width: MoreView.frame.width, height: 150)
//        }
//        MainView.reloadData()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.indicator.frame = CGRect(x: (view?.frame.minX)!, y: self.indicator.frame.minY, width: self.indicator.frame.width, height: self.indicator.frame.height)
        }, completion:  { (Bool) in })
    }
    
    @objc func like() {
        
    }
    
    @objc func share() {
        
    }
    
    @objc func previousSlide() {
        currentIndex = currentIndex - 1
        self.showNextPrev()
        
        let indexPath = NSIndexPath(item: currentIndex, section: 0)
        GalleryView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    @objc func nextSlide() {
        currentIndex = currentIndex + 1
        self.showNextPrev()
        
        let indexPath = NSIndexPath(item: currentIndex, section: 0)
        GalleryView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    //private, public funcs
    
    private func mainViewNavigation() {
        MainView.addSubview(nextView)
        nextView.addSubview(rightImage)
        MainView.addSubview(previousView)
        previousView.addSubview(leftImage)
        
        previousView.alpha = 0
        
        nextView.frame = CGRect(x: MainView.frame.width-60, y: 0, width: 50, height: 50)
        previousView.frame = CGRect(x: 10, y: nextView.frame.minY, width: 50, height: 50)
        rightImage.frame = CGRect(x: 25, y: 12.5, width: nextView.frame.height/2, height: nextView.frame.height/2)
        leftImage.frame = CGRect(x: 0, y: 12.5, width: previousView.frame.height/2, height: previousView.frame.height/2)
    }
    
    private func setupConstraints() {
        GalleryView.frame = CGRect(x: 0, y: -(navBarHeight+navbarStatusHeight), width: view.frame.width, height: view.frame.height/3)
        MainView.frame = CGRect(x: 0, y: GalleryView.frame.maxY-100, width: view.frame.width, height: (view.frame.height+navBarHeight+navbarStatusHeight+100)-(GalleryView.frame.height))
        MoreView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 802)
        indicator.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 10)
    }
    
    private func setupNavigationbar() {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let likeBarItem = UIBarButtonItem(image: UIImage(named: "ic_heart"), style: .plain, target: self, action: #selector(like))
        let shareBarItem = UIBarButtonItem(image: UIImage(named: "ic_upload"), style: .plain, target: self, action: #selector(share))
        
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.rightBarButtonItems = [shareBarItem, likeBarItem]
    }
    
    private func setupGestures() {
        previousView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(previousSlide)))
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextSlide)))
    }
    
    private func watch() {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.Main(alpha: 1.0)
        BusinessDetail.business = Businesses(
            _id : "",
            name : "",
            branches : 0,
            img_url : "",
            doc_type : "business",
            type : "",
            category : "",
            tags : [""],
            address : Addresses(),
            rating : 0,
            short_desc : "",
            description : "",
            owner : [""],
            config : Configurations(
                logo: "",
                theme_color: "",
                gallery: [""]
            ),
            subscription_details : Subscriptions(),
            operating_days : [Operations()],
            feedback : [Feedbacks()],
            catalog : [Catalog()]
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == GalleryView {
            return BusinessDetail.business.config.gallery.count
        } else if collectionView == MoreView {
            return tabs.count
        }
        return 5
    }
    
    private func subLabel(text : String) -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 3
        label.text = text
        return label
    }
    
    private func basicInfo(cell : UICollectionViewCell, indexPath: IndexPath) {
        let indicatorPane : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.rgba(red: 223, green: 223, blue: 223, alpha: 1.0)
            return view
        }()
        
        let nameLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.darkGray
            label.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 10))
            label.adjustsFontSizeToFitWidth = true
            label.text = BusinessDetail.business.name.capitalized
            return label
        }()
        
        let shortDescLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.gray
            label.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 1))
            label.adjustsFontSizeToFitWidth = true
            label.text = BusinessDetail.business.short_desc.capitalized
            return label
        }()
        
        let locationLabel = subLabel(text: "Bunlo, Bocaue, Bulacan".capitalized)
        
        let ratingLabel : UIButton = {
            let button = UIButton()
            button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.5))
            button.setTitleColor(UIColor.white, for: .normal)
            button.layer.cornerRadius = 25/4
            button.backgroundColor = Service().setColor(rating: Double("\((BusinessDetail.business.rating)!)")!)
            button.setTitle("\((BusinessDetail.business.rating)!)⭑".capitalized, for: .normal)
            return button
        }()
        
//        let ratingLabel : UILabel = {
//            let label = UILabel()
//            label.backgroundColor = .clear
//            label.textColor = Service().setColor(rating: Double("\((BusinessDetail.business.rating)!)")!)
//            label.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 2))
//            label.text = "\((BusinessDetail.business.rating)!)".capitalized
//            return label
//        }()
        
        let ratingTotalLabel = subLabel(text: "243 Ratings")
        
        let isOpenLabel : UILabel = {
            let label = UILabel()
            label.backgroundColor = UIColor.clear
            label.textColor = UIColor.Main(alpha: 1.0)
            label.font = UIFont(name: "SFProDisplay-Heavy", size: 15)!
            label.text = "open now".uppercased()
            label.layer.cornerRadius = 25/4
            label.textAlignment = .center
            label.layer.masksToBounds = true
            return label
        }()
        
        let scheduleLabel = subLabel(text: "10am - 9pm (Today)")
        let actionLabel = subLabel(text: "Reservation, Questions?")

        let actionButton : UIButton = {
            let button = UIButton()
            button.backgroundColor = UIColor.Main(alpha: 1.0)
            button.titleLabel?.font = .systemFont(ofSize: 15)
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitle("Call Us".capitalized, for: .normal)
            button.layer.cornerRadius = 25/4
            return button
        }()
        
        cell.addSubview(nameLabel)
        cell.addSubview(shortDescLabel)
        cell.addSubview(locationLabel)
        cell.addSubview(ratingLabel)
        cell.addSubview(ratingTotalLabel)
        cell.addSubview(isOpenLabel)
        cell.addSubview(scheduleLabel)
        cell.addSubview(actionLabel)
        cell.addSubview(actionButton)
        
        cell.addSubview(indicatorPane)
        cell.addSubview(indicator)
        
        nameLabel.frame = CGRect(x: 10, y: 10, width: cell.frame.width-(20+50), height: 40)
        shortDescLabel.frame = CGRect(x: 10, y: nameLabel.frame.maxY, width: cell.frame.width-(20), height: 20)
        locationLabel.frame = CGRect(x: 10, y: shortDescLabel.frame.maxY, width: cell.frame.width-(20), height: 20)
        
        ratingLabel.frame = CGRect(x: nameLabel.frame.maxX, y: 15, width: 50, height: 25)
//        ratingTotalLabel.frame = CGRect(x: nameLabel.frame.maxX, y: ratingLabel.frame.maxY, width: ratingLabel.frame.width, height: 20)
        
        isOpenLabel.frame = CGRect(x: 5, y: locationLabel.frame.maxY+20, width: 100, height: 25)
        scheduleLabel.frame = CGRect(x: 10, y: isOpenLabel.frame.maxY, width: (cell.frame.width/2)-(10), height: 20)
        
        actionLabel.frame = CGRect(x: scheduleLabel.frame.maxX, y: locationLabel.frame.maxY+20, width: scheduleLabel.frame.width, height: 20)
        actionButton.frame = CGRect(x: scheduleLabel.frame.maxX, y: actionLabel.frame.maxY, width: scheduleLabel.frame.width-50, height: isOpenLabel.frame.height+20)
        
        //add tab
        var x = CGFloat(0)
        let width = ((cell.frame.width+1)/CGFloat(tabs.count))
        
        indicatorPane.frame = CGRect(x: 0, y: cell.frame.height-10, width: cell.frame.width, height: 10)
        indicator.frame = CGRect(x: indicator.frame.minX, y: indicatorPane.frame.minY, width: width, height: indicatorPane.frame.height)
        
        for (index, tab) in tabs.enumerated() {
            let indexLabel : UILabel = {
                let label = UILabel()
                label.text = "\(index)"
                return label
            }()
            let tabButton : UIButton = {
                let button = UIButton()
                button.backgroundColor = UIColor.clear
                button.titleLabel?.font = UIFont(name: "SFProDisplay-Medium", size: 15)!
                button.setTitleColor(UIColor.gray, for: .normal)
                button.setTitle(tab.capitalized, for: .normal)
                return button
            }()
            tabButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabSelect)))
            cell.addSubview(tabButton)
            tabButton.addSubview(indexLabel)
            tabButton.frame = CGRect(x: x, y: cell.frame.height-50, width: width, height: CGFloat(40))
            x = x + tabButton.frame.width + 1
        }
    }
    
    private func report(cell : UICollectionViewCell, indexPath: IndexPath) {
        let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
        let goToImage : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.Main(alpha: 1.0)
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi+(CGFloat.pi/2))
            return imageView
        }()
        let reportLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.Main(alpha: 1.0)
            label.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 1))
            label.adjustsFontSizeToFitWidth = true
            label.text = "Report an error with your listing".capitalized
            return label
        }()
        let subReportLabel = subLabel(text: "suggestions to help Kisap be more udpdated")
        
        cell.addSubview(border)
        cell.addSubview(reportLabel)
        cell.addSubview(subReportLabel)
        cell.addSubview(goToImage)
        
        border.frame = CGRect(x: 10, y: 0, width: cell.frame.width-20, height: 0.5)
        reportLabel.frame = CGRect(x: 10, y: border.frame.maxY+(10+border.frame.height), width: cell.frame.width-(20+20), height: 20)
        subReportLabel.frame = CGRect(x: 10, y: reportLabel.frame.maxY, width: cell.frame.width-(20+20), height: cell.frame.height-(reportLabel.frame.height+20))
        goToImage.frame = CGRect(x: reportLabel.frame.maxX, y: (cell.frame.height/2)-(7.5), width: 15, height: 15)
    }
    
    private func alsoTry(cell : UICollectionViewCell, indexPath: IndexPath) {
        let border = UIView.border(color: UIColor.Base(alpha: 1.0))
        cell.addSubview(border)
        
        border.frame = CGRect(x: 10, y: 0, width: cell.frame.width-20, height: 0.5)
    }
    
    private func headerFollowers(cell : UICollectionViewCell, indexPath: IndexPath) {
        let headers = ["Lorem", "ipsum", "followers"]
        let values = ["140", "140", "24k"]
        //
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear, UIColor(white: 0, alpha: 0.7).cgColor]
        cell.addSubview(headerMainView)
        //reset gradiant
        self.resetGradiant()
        headerMainView.layer.addSublayer(gradientLayer)
        headerMainView.frame = CGRect(x: 0, y: 50, width: cell.frame.width, height: cell.frame.height-50)
        gradientLayer.frame = CGRect(x: 0, y: 0, width: headerMainView.frame.width, height: headerMainView.frame.height)
        //
        let width = (cell.frame.width-40)/CGFloat(headers.count)
        var x = CGFloat(20)
        for (index, header) in headers.enumerated() {
            let headerView : UIView = {
                let view = UIView()
                view.backgroundColor = .clear
                return view
            }()
            let borderView : UIView = {
                let view = UIView()
                view.backgroundColor = .white
                view.layer.cornerRadius = 5
                return view
            }()
            let valueLabel : UILabel = {
                let label = UILabel()
                label.backgroundColor = UIColor.clear
                label.font = UIFont(name: "SFProDisplay-Heavy", size: 20)!
                label.textColor = .white
                label.textAlignment = .center
                label.text = "\(values[index])".uppercased()
                return label
            }()
            
            let headerLabel : UILabel = {
                let label = UILabel()
                label.backgroundColor = UIColor.clear
                label.font = UIFont(name: "SFProDisplay-Medium", size: 11)!
                label.textColor = .white
                label.textAlignment = .center
                label.text = header.uppercased()
                return label
            }()
            
            headerMainView.addSubview(headerView)
            headerView.addSubview(valueLabel)
            headerView.addSubview(headerLabel)
            headerView.addSubview(borderView)
            
            headerView.frame = CGRect(x: x, y: 0, width: width, height: headerMainView.frame.height)
            valueLabel.frame = CGRect(x: 0, y: 0, width: width, height: headerView.frame.height-25)
            headerLabel.frame = CGRect(x: 0, y: valueLabel.frame.maxY, width: width, height: 20)
            if index < headers.count-1 {
                borderView.frame = CGRect(x: headerView.frame.width-2, y: 5, width: 2, height: headerView.frame.height-10)
            }
            x = x + headerView.frame.width
        }
    }
    
    private func reset(cell : UICollectionViewCell) {
        for views in cell.subviews {
            views.removeFromSuperview()
        }
    }
    
    private func resetGradiant() {
        if headerMainView.layer.sublayers != nil {
            for layer in headerMainView.layer.sublayers! {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == GalleryView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: galleryId, for: indexPath) as! DashAdsCell
            let imageUrl = BusinessDetail.business.config.gallery[indexPath.item]
            var ad = Ads()
            ad.name = ""
            ad.icon = imageUrl
            //
            cell.ad = ad
            cell.backgroundColor = UIColor.Base(alpha: 1.0)
            return cell
            
        } else if collectionView == MoreView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: moreId, for: indexPath)
            cell.backgroundColor = .clear
            reset(cell : cell)
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: overViewId, for: indexPath) as! BusinessOverview
                return cell
                
            } else if indexPath.item == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuId, for: indexPath) as! BusinessMenu
                BusinessMenu.catalogs = BusinessDetail.business.catalog
                return cell
                
            } else if indexPath.item == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reviewId, for: indexPath) as! BusinessReview
                cell.backgroundColor = .white
                BusinessReview.feedbacks = BusinessDetail.business.feedback
                return cell
            }
            return cell
        }
        
        // Main
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainId, for: indexPath)
        cell.backgroundColor = .white
        self.reset(cell : cell)
        if indexPath.item == 0 {
            cell.backgroundColor = .clear
            self.headerFollowers(cell: cell, indexPath: indexPath)
        } else if indexPath.item == 1 {
            basicInfo(cell : cell, indexPath: indexPath)
            
        } else if indexPath.item == 2 {
            //add more collection view to this cell
            cell.backgroundColor = .clear
            cell.addSubview(MoreView)
            
        } else if indexPath.item == 3 {
            report(cell : cell, indexPath: indexPath)
            
        } else if indexPath.item == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: alsoTryId, for: indexPath) as! DashMustTries
            cell.backgroundColor = UIColor.BgLight(alpha : 1.0)
            cell.titleLabel.text = "You May Also Like"
            cell.seeAllButton.setTitle("+134 more", for: .normal)
            
            cell.titleLabel.frame = CGRect(x: 10, y: cell.border.frame.maxY+(15-cell.border.frame.height), width: cell.frame.width-150, height: 25)
            cell.seeAllButton.frame = CGRect(x: cell.titleLabel.frame.maxX, y: 15, width: 130, height: cell.titleLabel.frame.height)
            cell.arrowImage.frame = CGRect(x: cell.seeAllButton.frame.width-20, y: cell.seeAllButton.frame.height/2-10, width: 20, height: 20)
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == GalleryView {
            return CGSize(width: GalleryView.frame.width, height: GalleryView.frame.height)
            
        } else if collectionView == MoreView {
            return CGSize(width: MoreView.frame.width, height: MoreView.frame.height)
        }
        
        //Main
        if indexPath.item == 1 {
            return CGSize(width: view.frame.width, height: 240)
            
        } else if indexPath.item == 2 {
            return CGSize(width: MoreView.frame.width, height: MoreView.frame.height)
            
        } else if indexPath.item == 3 {
            return CGSize(width: view.frame.width, height: 60)
            
        } else if indexPath.item == 4 {
            return CGSize(width: view.frame.width, height: 200)
            
        }
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == MainView {
            let scroll = scrollView.contentOffset.y
            let maxY = self.GalleryView.frame.maxY-100
            let height =  (view.frame.height+navBarHeight+navbarStatusHeight+120)-(GalleryView.frame.height)

            if scroll < 10 {
                setAlpa(alpha : 1.0)
            } else if scroll > 11 && scroll < 20 {
                setAlpa(alpha : 0.8)
            } else if scroll > 21 && scroll < 30 {
                setAlpa(alpha : 0.5)
            } else if scroll > 31 && scroll < 40 {
                setAlpa(alpha : 0.3)
            } else if scroll > 51 {
                setAlpa(alpha : 0)
            }
            
            if maxY-scroll >= self.navBarHeight+self.navbarStatusHeight {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.GalleryView.frame = CGRect(x: 0, y: self.GalleryView.frame.minY, width: self.GalleryView.frame.width, height: height-scroll)
                    self.MainView.frame = CGRect(x: 0, y: maxY-scroll, width: self.view.frame.width, height: height-scroll)

                }, completion:  { (Bool) in })
            } else {
               
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == GalleryView {
            let index = Int(GalleryView.contentOffset.x/GalleryView.frame.width)
            self.currentIndex = index
            self.showNextPrev()
        }
    }
    
    private func setAlpa(alpha : CGFloat) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.headerMainView.alpha = alpha
        }, completion:  { (Bool) in })
    }
    
    private func showNextPrev() {
        if currentIndex > 0 {
            previousView.alpha = 1.0
        } else {
            previousView.alpha = 0
        }
        
        if currentIndex < BusinessDetail.business.config.gallery.count-1 {
            nextView.alpha = 1.0
        } else {
            nextView.alpha = 0
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        setupView()
        setupConstraints()
        setupGestures()
        watch()
        setupNavigationbar()
        mainViewNavigation()
    }
}
