//
//  BusinessMenu.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 17/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class BusinessMenu: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let mainId = "mainId"
    
    static var catalogs = [Catalog()]
    
    let MainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    private func setupView() {
        MainView.backgroundColor = UIColor.white
        MainView.delegate = self
        MainView.dataSource = self
        MainView.isScrollEnabled = false
        MainView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        MainView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: mainId)
        
        addSubview(MainView)
    }
    
    private func setupConstraints() {
        MainView.frame = self.bounds
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return BusinessMenu.catalogs.count
    }
    
    private func reset(cell : UICollectionViewCell){
        for views in cell.subviews {
            views.removeFromSuperview()
        }
    }
    
    private func catalog(cell : UICollectionViewCell, indexPath : IndexPath) {
        let catalog = BusinessMenu.catalogs[indexPath.item]
        let menuLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "SFProDisplay-Heavy", size: 20)
            label.textAlignment = .left
            label.adjustsFontSizeToFitWidth = true
            label.layer.shadowColor = UIColor.darkGray.cgColor
            label.layer.shadowOpacity = 0.5
            label.layer.shadowOffset = CGSize(width: 0, height: 0)
            label.layer.shadowRadius = 3
            label.text = "\((catalog.name)!)"
            return label
        }()
        let catalogImage : UIImageView = {
            let imageView = UIImageView()
            imageView.backgroundColor = UIColor.clear
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.kf.setImage(with: URL(string: "\((catalog.img_url)!)"))
            return imageView
        }()
        
        cell.addSubview(catalogImage)
        if "\((catalog.name)!)" != "menu" {
            cell.addSubview(menuLabel)
        }
        catalogImage.frame = cell.bounds
        menuLabel.frame = CGRect(x: 20, y: 0, width: cell.frame.width-40, height: cell.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainId, for: indexPath)
        cell.backgroundColor = UIColor.BgLight(alpha : 1.0)
        self.reset(cell : cell)
        self.catalog(cell : cell,  indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/2-5, height: frame.width/2-5)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
