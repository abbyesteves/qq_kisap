//
//  BusinessOverview.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 16/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class BusinessOverview: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let ratings = 5
    let mainId = "mainId"
    let categoriesId = "categoriesId"
    
    let MainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0.3
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let ratingView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    // @objc funcs
    @objc func selectRating(sender : UITapGestureRecognizer) {
        let button = sender.view as! UIButton
        let indexLabel = sender.view?.subviews[1] as! UILabel
        let index = Int("\((indexLabel.text)!)")
        button.pulse()
        
        for (i, buttons) in ratingView.subviews.enumerated() {
            let indexView = i+1
            let button = buttons as! UIButton
            if index! >= indexView {
                button.backgroundColor = Service().setColor(rating: Double(index!))
                button.setTitleColor(UIColor.white, for: .normal)
                button.layer.borderWidth = 0
            } else {
                button.backgroundColor = UIColor.clear
                button.setTitleColor(UIColor.darkGray, for: .normal)
                button.layer.borderWidth = 1
            }
        }
    }
    
    // private / public funcs
    
    private func setupView() {
        
        MainView.backgroundColor = UIColor.Base(alpha: 0.5)
        MainView.delegate = self
        MainView.dataSource = self
        MainView.isScrollEnabled = false
        MainView.register(DashCategories.self, forCellWithReuseIdentifier: categoriesId)
        MainView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: mainId)
        
        addSubview(MainView)
    }
    
    private func setupConstraints() {
        MainView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
    
    private func rating(cell: UICollectionViewCell, indexPath : IndexPath) {
        let titleLabel = UILabel.titleLabel(text : "Rate this store!", alignment : .center, font : UIFont(name: "SFProDisplay-Bold", size: 17)!, color : UIColor.darkGray)
        let subTitleLabel = UILabel.subTitleLabel(text: "Tell us your experience", alignment: .center)
        
        cell.addSubview(titleLabel)
        cell.addSubview(subTitleLabel)
        cell.addSubview(ratingView)
        
        titleLabel.frame = CGRect(x: 10, y: 20, width: frame.width-20, height: 20)
        subTitleLabel.frame = CGRect(x: 10, y: titleLabel.frame.maxY, width: frame.width-20, height: 20)
        ratingView.frame = CGRect(x: 0, y: subTitleLabel.frame.maxY+10, width: frame.width, height: 30)
        
        //add rating buttons
        let total = (50+10)*ratings
        var x = CGFloat(cell.frame.width/2)-CGFloat(total/2)
        for rating in 1...ratings {
            let index : UILabel = {
                let label = UILabel()
                label.text = "\(rating)"
                return label
            }()
            let ratingButton : UIButton = {
                let button = UIButton()
                button.backgroundColor = UIColor.clear
                button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.5))
                button.setTitleColor(UIColor.gray, for: .normal)
                button.setTitle("\(rating) ⭑".capitalized, for: .normal)
                button.layer.cornerRadius = 15
                button.layer.borderWidth = 1
                button.layer.borderColor = UIColor.lightGray.cgColor
                return button
            }()
            
            ratingView.addSubview(ratingButton)
            ratingButton.addSubview(index)
            
            ratingButton.frame = CGRect(x: x, y: 0, width: 50, height: ratingView.frame.height)
            ratingButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectRating)))
            
            x = x + ratingButton.frame.width + 10
        }
    }
    
    private func address(cell: UICollectionViewCell, indexPath : IndexPath) {
        let addressLabel = UILabel.titleLabel(text : "Address", alignment : .left, font : UIFont(name: "SFProDisplay-Bold", size: 17)!, color : UIColor.darkGray)
        let address = UITextView.subTextView(text: "101 Calle Madrid, Bocaue, Bulacan, 3006", textColor: UIColor.darkGray)
        
        cell.addSubview(addressLabel)
        cell.addSubview(address)
        
        addressLabel.frame = CGRect(x: 10, y: 10, width: cell.frame.width-20, height: 20)
        address.frame = CGRect(x: 10, y: addressLabel.frame.maxY, width: cell.frame.width-20, height: cell.frame.height-(addressLabel.frame.height+20))
    }
    
    private func actions(cell: UICollectionViewCell, indexPath : IndexPath) {
        let actions = ["Call", "Directions", "Copy Link"]
        let width = (cell.frame.width-19)/CGFloat(actions.count)
        var x = CGFloat(10)
        for (index, action) in actions.enumerated() {
            let indexLabel : UILabel = {
                let label = UILabel()
                label.text = "\(index)"
                return label
            }()
            
            let actionButton : UIButton = {
                let button = UIButton()
                button.backgroundColor = UIColor.clear
                button.titleLabel?.font = .systemFont(ofSize: 15)
                button.setTitleColor(UIColor.Main(alpha: 1.0), for: .normal)
                button.setTitle(action.capitalized, for: .normal)
                return button
            }()
            var subtext = UILabel.subTitleLabel(text: "", alignment: .left)
            
            if action == actions[0] {
                let image = UIImage(named: "ic_text_call")!.overlayImage(color: UIColor.white)
                subtext = UILabel.subTitleLabel(text: "Have any questions?", alignment: .left)
                actionButton.setImage(image.scaleImage(newSize: CGSize(width: 7, height: 7)), for: .normal)
                actionButton.backgroundColor = UIColor.Main(alpha: 1.0)
                actionButton.setTitleColor(UIColor.white, for: .normal)
                actionButton.layer.cornerRadius = 5
            } else if action == actions[1] {
                let image = UIImage(named: "ic_marker")!.overlayImage(color: UIColor.Main(alpha: 1.0))
                actionButton.setImage(image.scaleImage(newSize: CGSize(width: 7, height: 7)), for: .normal)
            } else {
                let image = UIImage(named: "ic_text_copy")!.overlayImage(color: UIColor.Main(alpha: 1.0))
                actionButton.setImage(image.scaleImage(newSize: CGSize(width: 7, height: 7)), for: .normal)
            }
            
            cell.addSubview(subtext)
            cell.addSubview(actionButton)
            actionButton.addSubview(indexLabel)
            
            subtext.frame = CGRect(x: x, y: 10, width: cell.frame.width-20, height: 20)
            actionButton.frame = CGRect(x: x, y: subtext.frame.maxY, width: width, height: cell.frame.height-(subtext.frame.height+20))
            x = x + actionButton.frame.width + 1
        }
    }
    
    private func outlets(cell: UICollectionViewCell, indexPath : IndexPath) {
        let outletsLabel = UILabel.titleLabel(text : "See all 4 outlets in Bocaue, Bulacan", alignment : .left, font : UIFont(name: "SFProDisplay-Bold", size: 15)!, color : UIColor.darkGray)
        let goToImage : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.Main(alpha: 1.0)
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi+(CGFloat.pi/2))
            return imageView
        }()
        cell.addSubview(outletsLabel)
        cell.addSubview(goToImage)
        outletsLabel.frame = CGRect(x: 10, y: 0, width: cell.frame.width-(20+20), height: cell.frame.height)
        goToImage.frame = CGRect(x: outletsLabel.frame.maxX, y: (cell.frame.height/2)-(7.5), width: 15, height: 15)
    }
    
    private func offers(cell: UICollectionViewCell, indexPath : IndexPath) {
        let icon : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_text_offer")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.gray
            return imageView
        }()
        let titleLabel = UILabel.titleLabel(text : "Offers", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.gray)
        cell.addSubview(titleLabel)
        cell.addSubview(icon)
        
        icon.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        titleLabel.frame = CGRect(x: icon.frame.maxX+5, y: 10, width: cell.frame.width-(20+icon.frame.width), height: 20)
    }
    
    private func types(cell: UICollectionViewCell, indexPath : IndexPath) {
        let icon : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_text_types")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.gray
            return imageView
        }()
        let titleLabel = UILabel.titleLabel(text : "Types", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.gray)
        let subText = UITextView.subTextView(text: "Burger Joint, Snacks", textColor: UIColor.darkGray)
        
        cell.addSubview(titleLabel)
        cell.addSubview(icon)
        cell.addSubview(subText)
        
        icon.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        titleLabel.frame = CGRect(x: icon.frame.maxX+5, y: 10, width: cell.frame.width-(20+icon.frame.width), height: 20)
        subText.frame = CGRect(x: 10, y: titleLabel.frame.maxY, width: frame.width-20, height: cell.frame.height-(titleLabel.frame.height+20))
    }
    
    private func cost(cell: UICollectionViewCell, indexPath : IndexPath) {
        let icon : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_text_cost")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.gray
            return imageView
        }()
        let titleLabel = UILabel.titleLabel(text : "Average Cost", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.gray)
        let subText = UITextView.subTextView(text: "PHP200 for two people (approx). Cash only.", textColor: UIColor.darkGray)
        
        cell.addSubview(titleLabel)
        cell.addSubview(icon)
        cell.addSubview(subText)
        
        icon.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        titleLabel.frame = CGRect(x: icon.frame.maxX+5, y: 10, width: cell.frame.width-(20+icon.frame.width), height: 20)
        subText.frame = CGRect(x: 10, y: titleLabel.frame.maxY, width: frame.width-20, height: cell.frame.height-(titleLabel.frame.height+20))
    }
    
    private func info(cell: UICollectionViewCell, indexPath : IndexPath) {
        let icon : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_text_info")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.gray
            return imageView
        }()
        let titleLabel = UILabel.titleLabel(text : "More Info", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.gray)
        let subText = UITextView.subTextView(text: "No alcohol available", textColor: UIColor.darkGray)
        
        cell.addSubview(titleLabel)
        cell.addSubview(icon)
        cell.addSubview(subText)
        
        icon.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        titleLabel.frame = CGRect(x: icon.frame.maxX+5, y: 10, width: cell.frame.width-(20+icon.frame.width), height: 20)
        subText.frame = CGRect(x: 10, y: titleLabel.frame.maxY+10, width: frame.width-20, height: cell.frame.height-(titleLabel.frame.height+20))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainId, for: indexPath)
        cell.backgroundColor = .white
        
        if indexPath.item == 0 {
            rating(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 1 {
            address(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 2 {
            actions(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 3 {
            outlets(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesId, for: indexPath) as! DashCategories
            cell.backgroundColor = .white
            cell.CategoriesView.frame = CGRect(x: 0, y: 40, width: cell.frame.width, height: cell.CategoriesView.frame.height-50)
            offers(cell : cell, indexPath: indexPath)
            return cell
        } else if indexPath.item == 5 {
            types(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 6 {
            cost(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 7 {
            info(cell : cell, indexPath: indexPath)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 || indexPath.item == 4 {
            return CGSize(width: frame.width, height: 150)
        } else if indexPath.item == 1 {
            return CGSize(width: frame.width, height: 100)
        } else if indexPath.item == 2 {
            return CGSize(width: frame.width, height: 80)
        } else if indexPath.item == 3 {
            return CGSize(width: frame.width, height: 50)
        }
//        else indexPath.item == 3 {
//            return CGSize(width: frame.width, height: 50)
//        }
        return CGSize(width: frame.width, height: 100)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
