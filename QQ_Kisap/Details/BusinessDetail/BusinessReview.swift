//
//  BusinessReview.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 17/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class BusinessReview: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let mainId = "mainId"
    let categoriesId = "categoriesId"
    let reviewId = "reviewId"
    
    static var feedbacks = [Feedbacks()]
    
    let MainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let ReviewsView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    private func setupView() {
        
        MainView.backgroundColor = UIColor.clear
        MainView.delegate = self
        MainView.dataSource = self
        MainView.isScrollEnabled = false
        MainView.register(DashCategories.self, forCellWithReuseIdentifier: categoriesId)
        MainView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: mainId)
        
        ReviewsView.backgroundColor = UIColor.clear
        ReviewsView.delegate = self
        ReviewsView.dataSource = self
        ReviewsView.isScrollEnabled = false
        ReviewsView.showsHorizontalScrollIndicator = false
        ReviewsView.register(BusinessReviewCell.self, forCellWithReuseIdentifier: reviewId)
        
        addSubview(MainView)
    }
    
    private func setupConstraints() {
        MainView.frame = self.bounds
    }
    
    private func header(cell: UICollectionViewCell, indexPath : IndexPath) {
        let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
        let titleLabel = UILabel.titleLabel(text : "Trustworthy Reviews", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 17)!, color : UIColor.darkGray)
        let subText = UITextView.subTextView(text: "We weed out fake reviews and bring 100% genuine reviews to you.", textColor: UIColor.darkGray)
        
        cell.addSubview(titleLabel)
        cell.addSubview(subText)
        cell.addSubview(border)
        
        titleLabel.frame = CGRect(x: 10, y: 10, width: frame.width-20, height: 20)
        subText.frame = CGRect(x: 5, y: titleLabel.frame.maxY, width: frame.width-10, height: cell.frame.height-(20+10+0.5))
        border.frame = CGRect(x: 10, y: cell.frame.height-(0.5), width: frame.width-20, height: 0.5)
    }
    
    private func allReviews(cell: UICollectionViewCell, indexPath : IndexPath) {
        let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
        let outletsLabel = UILabel.titleLabel(text : "Read all 32 reviews", alignment : .left, font : UIFont(name: "SFProDisplay-Heavy", size: 15)!, color : UIColor.darkGray)
        let goToImage : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.Main(alpha: 1.0)
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi+(CGFloat.pi/2))
            return imageView
        }()
        cell.addSubview(outletsLabel)
        cell.addSubview(goToImage)
        cell.addSubview(border)
        outletsLabel.frame = CGRect(x: 10, y: 0, width: cell.frame.width-(20+20), height: cell.frame.height)
        goToImage.frame = CGRect(x: outletsLabel.frame.maxX, y: (cell.frame.height/2)-(7.5), width: 15, height: 15)
        border.frame = CGRect(x: 10, y: cell.frame.height-(0.5), width: frame.width-20, height: 0.5)
    }
    
    private func offers(cell: UICollectionViewCell, indexPath : IndexPath) {
        let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
        let icon : UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_text_offer")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.gray
            return imageView
        }()
        let titleLabel = UILabel.titleLabel(text : "Offers", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 15)!, color : UIColor.darkGray)
        cell.addSubview(titleLabel)
        cell.addSubview(border)
        cell.addSubview(icon)
        icon.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        titleLabel.frame = CGRect(x: icon.frame.maxX+5, y: 10, width: cell.frame.width-(20+icon.frame.width), height: 20)
        border.frame = CGRect(x: 10, y: cell.frame.height-(0.5), width: frame.width-20, height: 0.5)
    }
    
    private func review(cell: UICollectionViewCell, indexPath : IndexPath) {
        let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
        cell.addSubview(ReviewsView)
        cell.addSubview(border)
        ReviewsView.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height-0.5)
        border.frame = CGRect(x: 10, y: cell.frame.height-(0.5), width: frame.width-20, height: 0.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ReviewsView {
            return BusinessReview.feedbacks.count
        }
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == ReviewsView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reviewId, for: indexPath) as! BusinessReviewCell
            cell.backgroundColor = .white
            cell.review = BusinessReview.feedbacks[indexPath.item]
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainId, for: indexPath)
        cell.backgroundColor = .white
        
        if indexPath.item == 0 {
            header(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 1 {
            //review collection view
            review(cell : cell, indexPath: indexPath)
            
        } else if indexPath.item == 2 {
            allReviews(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesId, for: indexPath) as! DashCategories
            cell.backgroundColor = .white
            cell.CategoriesView.frame = CGRect(x: 0, y: 40, width: cell.frame.width, height: cell.CategoriesView.frame.height-50)
            offers(cell : cell, indexPath: indexPath)
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = frame.height-(100+150+50)
        if collectionView == ReviewsView {
            return CGSize(width: frame.width, height: height/2)
        }
        
        if indexPath.item == 0 {
            return CGSize(width: frame.width, height: 100)
        } else if indexPath.item == 1 {
            return CGSize(width: frame.width, height: height)
        } else if indexPath.item == 2 {
            return CGSize(width: frame.width, height: 50)
        }
        return CGSize(width: frame.width, height: 150)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
