//
//  ProfileDetail.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 25/10/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class ProfileDetail: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let navBarHeight = UINavigationController.init().navigationBar.frame.height
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    let town = CouchService().town()
    var currentIndex = Int(0)
    
    let mainId = "mainId"
    let videoId = "videoId"
    let projectId = "projectId"
    let categoriesId = "categoriesId"
    let galleryId = "galleryId"
    
    let halfBlue = UIView.navigationBar(color : UIColor.Main(alpha: 1.0))
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    let MainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let GalleryView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let nextView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.alpha = 0.5
        return view
    }()
    
    let nextImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi+(CGFloat.pi/2))
        imageView.layer.shadowColor = UIColor.darkGray.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    let previousView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.alpha = 0.5
        return view
    }()
    
    let previousImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        imageView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi+(CGFloat.pi/2)))
        imageView.layer.shadowColor = UIColor.darkGray.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageView.layer.shadowRadius = 3
        return imageView
    }()
    
    
    //@objc funcs
    @objc func openArticle() {
        let urlString = town.officials.mayor.news[0]
        webDetail.open(url : urlString, sourceName : "centralluzon.politics.com.ph")
    }
    
    @objc func like() {
        
    }
    
    @objc func previousSlide() {
        currentIndex = currentIndex - 1
        self.showNextPrev()
        
        let indexPath = NSIndexPath(item: currentIndex, section: 0)
        GalleryView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    @objc func nextSlide() {
        currentIndex = currentIndex + 1
        self.showNextPrev()
        
        let indexPath = NSIndexPath(item: currentIndex, section: 0)
        GalleryView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    //private funcs
    private func showNextPrev() {
        if currentIndex > 0 {
            previousView.alpha = 1.0
        } else {
            previousView.alpha = 0
        }
        
        if currentIndex < town.officials.mayor.social_media.count-1 {
            nextView.alpha = 1.0
        } else {
            nextView.alpha = 0
        }
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.white
        
        MainView.backgroundColor = UIColor.clear
        MainView.delegate = self
        MainView.dataSource = self
        MainView.register(VideoCell.self, forCellWithReuseIdentifier: videoId)
        MainView.register(DashMustTries.self, forCellWithReuseIdentifier: projectId)
        MainView.register(DashCategories.self, forCellWithReuseIdentifier: categoriesId)
        MainView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: mainId)
        
        GalleryView.backgroundColor = UIColor.clear
        GalleryView.delegate = self
        GalleryView.dataSource = self
        GalleryView.isPagingEnabled = true
        GalleryView.register(DashAdsCell.self, forCellWithReuseIdentifier: galleryId)
        
        view.addSubview(halfBlue)
        view.addSubview(MainView)
        
        previousView.alpha = 0
    }
    
    private func setupConstraints() {
        halfBlue.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/2)
        MainView.frame = CGRect(x: 0, y: navbarStatusHeight+navBarHeight, width: view.frame.width, height: view.frame.height-(navbarStatusHeight+navBarHeight))
    }
    
    private func setupGestures() {
        previousView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(previousSlide)))
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextSlide)))
    }
    
    private func setupNavigationbar() {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.Main(alpha: 1.0)
        let likeBarItem = UIBarButtonItem(image: UIImage(named: "ic_heart"), style: .plain, target: self, action: #selector(like))
        let name = town.officials.mayor.name != nil ? town.officials.mayor.name : ""
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "\((name)!)".uppercased()
            label.font = UIFont(name: "SFProDisplay-Medium", size: 15)!
            label.textColor = UIColor.white
            return label
        }()
        
        self.navigationItem.titleView = titleLabel
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.rightBarButtonItems = [likeBarItem]
    }
    
    private func headerFollowers(cell : UICollectionViewCell, indexPath: IndexPath) {
        let headers = ["photos", "followers", "following"]
        let values = ["140", "24k", "1,890"]
        let width = (cell.frame.width-60)/CGFloat(headers.count)
        var x = CGFloat(20)
        for (index, header) in headers.enumerated() {
            let headerView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.BgLight(alpha: 1.0)
                return view
            }()
            
            let valueLabel : UILabel = {
                let label = UILabel()
                label.backgroundColor = UIColor.clear
                label.font = UIFont(name: "SFProDisplay-Heavy", size: 20)!
                label.textColor = UIColor.darkGray
                label.textAlignment = .center
                label.text = "\(values[index])".uppercased()
                return label
            }()
            
            let headerLabel : UILabel = {
                let label = UILabel()
                label.backgroundColor = UIColor.clear
                label.font = UIFont(name: "SFProDisplay-Medium", size: 11)!
                label.textColor = UIColor.darkGray
                label.textAlignment = .center
                label.text = header.uppercased()
                return label
            }()
            
            cell.addSubview(headerView)
            headerView.addSubview(valueLabel)
            headerView.addSubview(headerLabel)
            
            headerView.frame = CGRect(x: x, y: 10, width: width, height: cell.frame.height-20)
            valueLabel.frame = CGRect(x: 0, y: 0, width: width, height: headerView.frame.height-25)
            headerLabel.frame = CGRect(x: 0, y: valueLabel.frame.maxY, width: width, height: 20)
            x = x + headerView.frame.width + 10
        }
    }
    
    private func header(cell : UICollectionViewCell, indexPath: IndexPath) {
        let navigationBar = UIView.navigationBar(color : UIColor.Main(alpha: 1.0))
//        let headerView : UIImageView = {
//            let imageView = UIImageView()
//            imageView.backgroundColor = UIColor.Base(alpha: 1.0)
//            return imageView
//        }()
        cell.addSubview(navigationBar)
        cell.addSubview(GalleryView)
        cell.addSubview(nextView)
        nextView.addSubview(nextImage)
        cell.addSubview(previousView)
        previousView.addSubview(previousImage)
        
        GalleryView.frame = CGRect(x: 20, y: 0, width: cell.frame.width-40, height: cell.frame.height)
        navigationBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 20)
        
        nextView.frame = CGRect(x: GalleryView.frame.width-30, y: GalleryView.frame.height/2-25, width: 50, height: 50)
        previousView.frame = CGRect(x: 20, y: nextView.frame.minY, width: 50, height: 50)
        
        nextImage.frame = CGRect(x: 25, y: 12.5, width: nextView.frame.height/2, height: nextView.frame.height/2)
        previousImage.frame = CGRect(x: 0, y: 12.5, width: previousView.frame.height/2, height: previousView.frame.height/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == GalleryView {
            return town.officials.mayor.social_media.count
        }
        return 7
    }
    
    private func resetCell(cell: UICollectionViewCell, indexPath : IndexPath) {
        for views in cell.subviews {
            views.removeFromSuperview()
        }
    }
    
    private func shots() -> [ThumbnailCells] {
        let shots = town.officials.mayor.gallery
        var shotsArr = [ThumbnailCells]()
        for url in shots! {
            var shot = ThumbnailCells()
            shot.name = ""
            shot.img_url = "\(url)"
            shot.icon = ""
            shotsArr.append(shot)
        }
        return shotsArr
    }
    
    private func teams() -> [ThumbnailCells] {
        let teams = town.officials.mayor.teams
        var shotsArr = [ThumbnailCells]()
        for url in teams! {
            var shot = ThumbnailCells()
            shot.name = "\((url.name)!)"
            shot.img_url = "\((url.img)!)"
            shot.icon = "\((url.position)!)"
            shotsArr.append(shot)
        }
        return shotsArr
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == GalleryView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: galleryId, for: indexPath) as! DashAdsCell
            let imageUrl = town.officials.mayor.social_media[indexPath.item]
            var ad = Ads()
            ad.name = ""
            ad.icon = imageUrl
            //
            cell.ad = ad
            cell.backgroundColor = UIColor.Base(alpha: 1.0)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainId, for: indexPath)
        cell.backgroundColor = UIColor.white//BgLight(alpha : 1.0)
        
        if indexPath.item == 0 {
            self.header(cell : cell, indexPath: indexPath)
        } else if indexPath.item == 1 {
            self.headerFollowers(cell: cell, indexPath: indexPath)
        } else if indexPath.item == 2 || indexPath.item == 3 {
            let bgView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.BgLight(alpha: 1.0)
                return view
            }()
            var desc = town.officials.mayor.short_desc != nil ? town.officials.mayor.short_desc : ""
            if  indexPath.item == 3  {
                desc = town.officials.mayor.summary != nil ? town.officials.mayor.summary : ""
            }
            let subText = UITextView.subTextView(text: "\((desc)!)", textColor: UIColor.darkGray)

            cell.addSubview(bgView)
            bgView.addSubview(subText)
            bgView.frame = CGRect(x: 20, y: 0, width: cell.frame.width-40, height: cell.frame.height-10)
            subText.frame = CGRect(x: 10, y: 10, width: bgView.frame.width-20, height: bgView.frame.height-20)
            
        } else if indexPath.item == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: videoId, for: indexPath) as! VideoCell
            cell.backgroundColor = UIColor.BgLight(alpha : 1.0)
            VideoCell.videos = town.officials.mayor.videos
            cell.titleLabel.text = "Videos"
            cell.seeAllButton.setTitle("+246 more", for: .normal)
            
            cell.titleLabel.frame = CGRect(x: 10, y: cell.border.frame.maxY+(15-cell.border.frame.height), width: cell.frame.width-150, height: 25)
            cell.seeAllButton.frame = CGRect(x: cell.titleLabel.frame.maxX, y: 15, width: 130, height: cell.titleLabel.frame.height)
            cell.arrowImage.frame = CGRect(x: cell.seeAllButton.frame.width-20, y: cell.seeAllButton.frame.height/2-10, width: 20, height: 20)
            return cell
            
        } else if indexPath.item == 5 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: projectId, for: indexPath) as! DashMustTries
            cell.backgroundColor = UIColor.BgLight(alpha : 1.0)
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openArticle)))
            cell.tries = self.shots()
            cell.titleLabel.text = "Project"
            cell.seeAllButton.setTitle("+134 more", for: .normal)
            
            cell.titleLabel.frame = CGRect(x: 10, y: cell.border.frame.maxY+(15-cell.border.frame.height), width: cell.frame.width-150, height: 25)
            cell.seeAllButton.frame = CGRect(x: cell.titleLabel.frame.maxX, y: 15, width: 130, height: cell.titleLabel.frame.height)
            cell.arrowImage.frame = CGRect(x: cell.seeAllButton.frame.width-20, y: cell.seeAllButton.frame.height/2-10, width: 20, height: 20)
            return cell
            
        } else if indexPath.item == 6 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesId, for: indexPath) as! DashCategories
//            self.resetCell(cell: cell, indexPath: indexPath)
            cell.categories = self.teams()
            cell.CategoriesView.frame = CGRect(x: 0, y: 40, width: cell.frame.width, height: cell.CategoriesView.frame.height-50)
            cell.backgroundColor = UIColor.BgLight(alpha : 1.0)
            
            let border = UIView.border(color: UIColor.Boarder(alpha: 1.0))
            let titleLabel = UILabel.titleLabel(text : "Teams", alignment : .left, font : UIFont(name: "SFProDisplay-Medium", size: 17)!, color : UIColor.darkGray)
            cell.addSubview(border)
            cell.addSubview(titleLabel)
            border.frame = CGRect(x: 10, y: 0, width: view.frame.width-20, height: 0.5)
            titleLabel.frame = CGRect(x: 10, y: border.frame.height+10, width: cell.frame.width-20, height: 20)
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == GalleryView {
            return CGSize(width: GalleryView.frame.width, height: GalleryView.frame.height)
        }
        if indexPath.item == 0 {
            return CGSize(width: view.frame.width, height: view.frame.height/4)
        } else if indexPath.item == 1 {
            return CGSize(width: view.frame.width, height: 80)
        } else if indexPath.item == 2 || indexPath.item == 3 {
            var desc = town.officials.mayor.short_desc != nil ? town.officials.mayor.short_desc : ""
            if  indexPath.item == 3  {
                desc = town.officials.mayor.summary != nil ? town.officials.mayor.summary : ""
            }
            let sizeCaption = CGSize(width: view.frame.width-40, height: view.frame.height-60)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let captionSize = NSString(string: desc!).boundingRect(with: sizeCaption, options: options, attributes: [NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Regular", size: 18.1)!], context: nil)
            
            return CGSize(width: view.frame.width, height: captionSize.height)
        } else if indexPath.item == 4 {
            return CGSize(width: view.frame.width, height: 250)
        } else if indexPath.item == 5 {
            return CGSize(width: view.frame.width, height: 200)
        } else if indexPath.item == 6 {
            return CGSize(width: view.frame.width, height: 150)
        }
        return CGSize(width: view.frame.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == GalleryView {
            let index = Int(GalleryView.contentOffset.x/GalleryView.frame.width)
            self.currentIndex = index
            self.showNextPrev()
        }
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        setupView()
        setupConstraints()
        setupGestures()
        setupNavigationbar()
    }
}
