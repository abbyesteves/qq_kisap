//
//  WebDetail.swift
//  QQ_Kisap
//
//  Created by Abby Esteves on 15/11/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import WebKit

class WebDetail: UIViewController, UIWebViewDelegate {
    
    var timeBool = Bool()
    var timer = Timer()
    var safariURL = String()
    let mainView = UIView()
    let backView = UIView()
    let safariView = UIView()
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    
    let webView : UIWebView = {
        let web = UIWebView()
        return web
    }()
    
    let progressBar : UIProgressView = {
        let bar = UIProgressView()
        bar.progressTintColor = UIColor.Main(alpha: 1.0)
        bar.trackTintColor = UIColor.rgba(red: 237, green: 237, blue: 237, alpha: 1.0)
        return bar
    }()
    
    let sourceNameText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        //        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Name"
        return label
    }()
    
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.Main(alpha: 1.0)
        return imageView
    }()
    
    let safariImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_safari")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    @objc func timerCallBack() {
        if progressBar.progress == 0.8 {
            progressBar.progress += 0.05
        } else {
            progressBar.progress += 0.01
        }
        //progressBar.isHidden = true
        //timer.invalidate()
    }
    
    @objc func close(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        if let window = UIApplication.shared.keyWindow {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.removeCache()
                self.webView.removeFromSuperview()
                self.mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            }, completion:  { (Bool) in
                self.mainView.removeFromSuperview()
            })
        }
    }
    
    @objc func openSafari(){
        let alertController = UIAlertController(title: "Re-open Article", message: "Read artcile on web browser Safari?", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Default action"), style: UIAlertAction.Style.default, handler: { _ in
            UIApplication.shared.open(URL(string : self.safariURL)! as URL, options: [:], completionHandler: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.cancel, handler: { _ in
            
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    private func removeCache(){
        URLCache.shared.removeAllCachedResponses()
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    func open(url : String, sourceName : String) {
//        self.removeCache()
        progressBar.progress = 0.0
        sourceNameText.text = sourceName
        mainView.backgroundColor = UIColor.white
        webView.delegate = self
        
        safariURL = url
        guard let url = URL(string: url) else { return }
        let urlRequest : URLRequest = URLRequest(url: url)
        self.webView.loadRequest(urlRequest)
        
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
        safariView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSafari)))
        
        if let window = UIApplication.shared.keyWindow {
            
            safariImage.alpha = 0.5
            
            window.addSubview(mainView)
            mainView.addSubview(progressBar)
            mainView.addSubview(sourceNameText)
            mainView.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(safariView)
            safariView.addSubview(safariImage)
            mainView.addSubview(webView)
            
            sourceNameText.frame = CGRect(x: 0, y: 15, width: window.frame.width, height:  20)
            backImage.frame = CGRect(x: 0, y: 15, width: 20, height:  20)
            backView.frame = CGRect(x: 10, y: 0, width: 60, height:  60)
            safariImage.frame = CGRect(x: 0, y: 10, width: 30, height:  30)
            safariView.frame = CGRect(x: window.frame.width-40, y: 0, width: 60, height:  60)
            progressBar.frame = CGRect(x: 0, y: 48, width: window.frame.width, height: 2)
            webView.frame = CGRect(x: 0, y: 50, width: window.frame.width, height: window.frame.height-60)
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: self.navbarStatusHeight, width: window.frame.width, height: window.frame.height-self.navbarStatusHeight)
                
            }, completion:  { (Bool) in })
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        progressBar.progress = 0.0
        timeBool = false
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(timerCallBack), userInfo: nil, repeats: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        timeBool = true
        progressBar.progress = 1.0
    }
}

