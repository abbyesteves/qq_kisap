# BubblePicker

[![CI Status](http://img.shields.io/travis/ronnel_davis@yahoo.com/BubblePicker.svg?style=flat)](https://travis-ci.org/ronnel_davis@yahoo.com/BubblePicker)
[![Version](https://img.shields.io/cocoapods/v/BubblePicker.svg?style=flat)](http://cocoapods.org/pods/BubblePicker)
[![License](https://img.shields.io/cocoapods/l/BubblePicker.svg?style=flat)](http://cocoapods.org/pods/BubblePicker)
[![Platform](https://img.shields.io/cocoapods/p/BubblePicker.svg?style=flat)](http://cocoapods.org/pods/BubblePicker)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BubblePicker is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "BubblePicker"
```

## Author

ronnel_davis@yahoo.com, ronnel_davis@yahoo.com

## License

BubblePicker is available under the MIT license. See the LICENSE file for more info.
